<?php
App::uses('AppModel', 'Model');
/**
 * Push Model
 *
 */
class Push extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'push';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'idPush';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'text' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
