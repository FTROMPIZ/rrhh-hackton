<?php
App::uses('AppModel', 'Model');
/**
 * Branch Model
 *
 */
class Branch extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'branch';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'idBranch';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
		),

		'address' => array(
			'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
		),

		'city' => array(
			'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
		),
	);
}
