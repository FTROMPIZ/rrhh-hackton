<?php
App::uses('AppModel', 'Model');
/**
 * Diningroom Model
 *
 */
class Diningroom extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'diningroom';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'idDiningRoom';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'imgPath' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
                'date' => array(
			'isUnique' => array(
				'rule' => array('isUnique'),
				//'message' => 'Ya existe un menú para la fecha seleccionada',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'Branch_idBranch' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
