<?php
App::uses('AppModel', 'Model');
/**
 * News Model
 *
 */
class News extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
    public $useTable = 'news';
    public $primaryKey = 'idNews';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'name' => array(
            'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
        ),

        'description' => array(
            'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
        ),

        'imgPath' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo requerido.'
            ),
        )
    );
}
