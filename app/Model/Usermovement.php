<?php
App::uses('AppModel', 'Model');
/**
 * Usermovement Model
 *
 */
class Usermovement extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'usermovement';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'idUserMovement';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'User_idUser' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
