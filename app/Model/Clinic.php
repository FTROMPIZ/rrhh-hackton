<?php
App::uses('AppModel', 'Model');
/**
 * Clinic Model
 *
 */
class Clinic extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'clinic';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'idClinic';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
            'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
        ),

        'place' => array(
            'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
        ),

		'address' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Campo requerido'
			),
		),

		'city' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Campo requerido'
			)
		),

		'state' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Campo requerido'
			)
		),
	);
}
