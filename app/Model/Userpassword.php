<?php

    class Userpassword extends AppModel {
    /**
     * Use table
     *
     * @var mixed False or table name
     */
        public $useTable = 'userpassword';
        public $primaryKey = 'idUserPassword';

        public function beforeSave($options = array()) 
        {
            if (isset($this->data[$this->alias]['password'])) 
            {
                $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
            }
            return true;
        }
    }
?>