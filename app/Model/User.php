<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'user';
    public $primaryKey = 'idUser';
    public $belongsTo = array('State');
    public $hasMany = array('Person');

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'username' => array(
            'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            ),
            'isUnique' => array (
                    'rule' => 'isUnique',
                    'message' => 'El usuario ya Existe'
            )
        ),
 
        'password' => array(        
            'minLength' => array(
                'rule' => array('minLength', 8),
                'message' => 'El password debe contener al menos 8 caracteres.',
                'on' => 'create'
            ),
            'password-1' => array(
                'rule' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%]).{8,}$/',
                'message' => 'Debe contener al menos una letra mayúscula, un numero y alguno de estos caracteres "!@#$%"',
                'required' => true, 
                'allowEmpty' => false,
                'on' => 'create'
            )
        ),

        'email' => array(
            'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            ),
            'isUnique' => array (
                'rule' => 'isUnique',
                'message' => 'El email ya Existe'
            )
        ),

        'role' => array(
            'valid' => array(
                'rule' => array('inList', array('Admin', 'Super-Admin','MobileMedia-Admin')),
                'message' => 'Introduce un rol de usuario valido.',
                'allowEmpty' => false
            )
        ),
    );

    public function beforeSave($options = array()) 
    {
        if (isset($this->data[$this->alias]['password'])) 
        {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }
}
