<?php
App::uses('AppModel', 'Model');
/**
 * Insurance Model
 *
 */
class Insurance extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'insurance';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'idInsurance';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
