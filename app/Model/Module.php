<?php
App::uses('AppModel', 'Model');
/**
 * Module Model
 *
 */
class Module extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'module';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'idModule';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'moduleName' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
	);
        public function beforeSave($options = array()) 
    {
        if (isset($this->data[$this->alias]['moduleName'])) 
        {
            
            for($i=0; $i<strlen($this->data[$this->alias]['moduleName']); $i++) {
                    $char = substr($this->data[$this->alias]['moduleName'], $i, 1);
                    $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt"))-1, 1);
                    $char2 = chr(ord($char)+ord($keychar));
                    $result.=$char2;
                 }
                $encriptado=base64_encode($result);
            $this->data[$this->alias]['moduleName'] = $encriptado;
        }
        return true;
    }
}
