<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 */
class Event extends AppModel {


public $belongsTo = array('EventType');


/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'event';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'idEvent';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
            'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
        ),

        'description' => array(
            'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
        ),

        'place' => array(
            'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
        ),

		'imgPath' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty')
			)
		),

		'Branch_idBranch' => array(
			'numeric' => array(
				'rule' => array('numeric')
			)
		)
	);
}
