<?php
App::uses('AppModel', 'Model');
/**
 * Clinic Model
 *
 */
class State extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'state';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'idState';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'state' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

}
