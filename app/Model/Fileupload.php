<?php
App::uses('AppModel', 'Model');
/**
 * Fileupload Model
 *
 */
class Fileupload extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'fileupload';

/**
 * Validation rules
 *
 * @var array
 */
    public $actsAs = array('Upload.Upload' => array('archivo' => array('fields' => array('dir' => 'archivo'))));
	public $validate = array(
		'fileName' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty')
			),
		),
		'name' => array(
			'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
		),

		'description' => array(
            'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
        ),

        'place' => array(
            'ruleRequired' => array(
              'rule' => 'notEmpty',
              'message' => 'Campo requerido'
            )
        ),
	);
        
}
