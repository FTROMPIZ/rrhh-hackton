<?php
App::uses('AppModel', 'Model');
/**
 * Menutype Model
 *
 */
class Menutype extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'menutype';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'idMenuType';

}
