<?php
App::uses('AppModel', 'Model');
/**
 * Push Model
 *
 */
class Ipwhitelist extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'ipwhitelist';

/**
 * Primary key field
 *
 * @var string
 */
    public $primaryKey = 'idIpwhitelist';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'ip' => array(
            'rule' => array('ip', 'both'), // or 'IPv6' or 'both' (default)
            'message' => 'Pot favor ingrese una dirección IP válida.'
        )
    );
}
