<?php
App::uses('AppModel', 'Model');
/**
 * Appuser Model
 *
 */
class Appuser extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'appuser';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'idAppUser';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'username' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
                        'isUnique' => array (
                                'rule' => 'isUnique',
                                'message' => 'El usuario ya Existe',
                        )
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
                        'isUnique' => array (
                                'rule' => 'isUnique',
                                'message' => 'El correo ya Existe',
                        )
		),
		'password' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
    public function beforeSave($options = array()) 
    {
        if (isset($this->data[$this->alias]['password'])) 
        {
            
            for($i=0; $i<strlen($this->data[$this->alias]['password']); $i++) {
                    $char = substr($this->data[$this->alias]['password'], $i, 1);
                    $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt"))-1, 1);
                    $char2 = chr(ord($char)+ord($keychar));
                    $result.=$char2;
                 }
                $encriptado=base64_encode($result);
            $this->data[$this->alias]['password'] = $encriptado;
        }
        return true;
    }
}
