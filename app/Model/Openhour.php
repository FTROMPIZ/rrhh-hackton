<?php
App::uses('AppModel', 'Model');
/**
 * Push Model
 *
 */
class Openhour extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'openhour';

/**
 * Primary key field
 *
 * @var string
 */
    public $primaryKey = 'idOpenHour';

/**
 * Validation rules
 *
 * @var array
 */
    // public $validate = array(
    //     'ip' => array(
    //         'rule' => array('ip', 'both'), // or 'IPv6' or 'both' (default)
    //         'message' => 'Pot favor ingrese una dirección IP válida.'
    //     )
    // );
}
