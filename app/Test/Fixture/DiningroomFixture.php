<?php
/**
 * DiningroomFixture
 *
 */
class DiningroomFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'diningroom';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'idDiningRoom' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'date' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP(6)', 'length' => 6),
		'imgPath' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 200, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Branch_idBranch' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'idDiningRoom', 'unique' => 1),
			'idDiningRoom_UNIQUE' => array('column' => 'idDiningRoom', 'unique' => 1),
			'fk_DiningRoom_Branch1_idx' => array('column' => 'Branch_idBranch', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'idDiningRoom' => 1,
			'date' => 1423863305,
			'imgPath' => 'Lorem ipsum dolor sit amet',
			'Branch_idBranch' => 1
		),
	);

}
