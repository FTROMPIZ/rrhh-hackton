<?php
/**
 * UsermovementFixture
 *
 */
class UsermovementFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'usermovement';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'idUserMovement' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'action' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'field' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'value' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'User_idUser' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'idUserMovement', 'unique' => 1),
			'idUserMovement_UNIQUE' => array('column' => 'idUserMovement', 'unique' => 1),
			'fk_UserMovement_User_idx' => array('column' => 'User_idUser', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'idUserMovement' => 1,
			'action' => 'Lorem ipsum dolor sit amet',
			'field' => 'Lorem ipsum dolor sit amet',
			'value' => 'Lorem ipsum dolor sit amet',
			'User_idUser' => 1
		),
	);

}
