<?php
/**
 * EventFixture
 *
 */
class EventFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'event';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'idEvent' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'description' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'date' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP(6)', 'length' => 6),
		'place' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'imgPath' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 200, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Branch_idBranch' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'idEvent', 'unique' => 1),
			'idEvent_UNIQUE' => array('column' => 'idEvent', 'unique' => 1),
			'fk_Event_Branch1_idx' => array('column' => 'Branch_idBranch', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'idEvent' => 1,
			'description' => 'Lorem ipsum dolor sit amet',
			'date' => 1423863318,
			'place' => 'Lorem ipsum dolor sit amet',
			'imgPath' => 'Lorem ipsum dolor sit amet',
			'Branch_idBranch' => 1
		),
	);

}
