<?php
/**
 * DepartmentPersonFixture
 *
 */
class DepartmentPersonFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'idDP' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'idPeople' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'idDepartment' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'idPosition' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'start_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'end_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'idDP', 'unique' => 1),
			'department_people_pk' => array('column' => 'idDP', 'unique' => 1),
			'fk_department_people_fk' => array('column' => 'idPeople', 'unique' => 0),
			'fk_department_position_fk' => array('column' => 'idPosition', 'unique' => 0),
			'FK_department_people_FK_department' => array('column' => 'idDepartment', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'idDP' => '',
			'idPeople' => '',
			'idDepartment' => '',
			'idPosition' => '',
			'start_date' => '2015-12-10 21:16:57',
			'end_date' => '2015-12-10 21:16:57'
		),
	);

}
