<?php
/**
 * EventTypeFixture
 *
 */
class EventTypeFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'event_type';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'idEventType' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 50, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'idEventType', 'unique' => 1),
			'idEventType' => array('column' => 'idEventType', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'idEventType' => '',
			'name' => 'Lorem ipsum dolor sit amet'
		),
	);

}
