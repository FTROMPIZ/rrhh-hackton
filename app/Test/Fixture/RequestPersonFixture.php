<?php
/**
 * RequestPersonFixture
 *
 */
class RequestPersonFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'idRequestPeople' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'idRequestType' => array('type' => 'biginteger', 'null' => false, 'default' => '0', 'unsigned' => false, 'key' => 'index'),
		'idPeople' => array('type' => 'biginteger', 'null' => false, 'default' => '0', 'unsigned' => false, 'key' => 'index'),
		'date_request' => array('type' => 'datetime', 'null' => false, 'default' => '0000-00-00 00:00:00'),
		'reffer_to' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 50, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'start_date' => array('type' => 'datetime', 'null' => false, 'default' => '0000-00-00 00:00:00'),
		'end_date' => array('type' => 'datetime', 'null' => false, 'default' => '0000-00-00 00:00:00'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'idRequestPeople', 'unique' => 1),
			'idRequestPeople' => array('column' => 'idRequestPeople', 'unique' => 1),
			'FK_request_people_FK_requestType' => array('column' => 'idRequestType', 'unique' => 0),
			'FK_request_people_FK_people' => array('column' => 'idPeople', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'idRequestPeople' => '',
			'idRequestType' => '',
			'idPeople' => '',
			'date_request' => '2015-12-10 21:27:15',
			'reffer_to' => 'Lorem ipsum dolor sit amet',
			'start_date' => '2015-12-10 21:27:15',
			'end_date' => '2015-12-10 21:27:15'
		),
	);

}
