<?php
/**
 * InsuranceFixture
 *
 */
class InsuranceFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'insurance';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'idInsurance' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'rif' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'number' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'telephone_1' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 15, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'telephone_2' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 15, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'name_broker' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'lastname_broker' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'telephone_broker_1' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 15, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'telephone_broker_2' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 15, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'email_broker' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'hcm_insurance' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'gmm_insurance' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'idInsurance', 'unique' => 1),
			'insurance_pk' => array('column' => 'idInsurance', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'idInsurance' => '',
			'rif' => 'Lorem ipsum dolor ',
			'number' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'telephone_1' => 'Lorem ipsum d',
			'telephone_2' => 'Lorem ipsum d',
			'name_broker' => 'Lorem ipsum dolor sit amet',
			'lastname_broker' => 'Lorem ipsum dolor sit amet',
			'telephone_broker_1' => 'Lorem ipsum d',
			'telephone_broker_2' => 'Lorem ipsum d',
			'email_broker' => 'Lorem ipsum dolor sit amet',
			'hcm_insurance' => 1,
			'gmm_insurance' => 1
		),
	);

}
