<?php
/**
 * RequestTypeFixture
 *
 */
class RequestTypeFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'request_type';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'idRequestType' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 50, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'idRequestTypeUnique' => array('column' => 'idRequestType', 'unique' => 1),
			'idRequestTypeKey' => array('column' => 'idRequestType', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'idRequestType' => '',
			'name' => 'Lorem ipsum dolor sit amet'
		),
	);

}
