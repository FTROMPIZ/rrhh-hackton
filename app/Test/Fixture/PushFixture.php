<?php
/**
 * PushFixture
 *
 */
class PushFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'push';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'idPush' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'date' => array('type' => 'timestamp', 'null' => false, 'default' => null),
		'text' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1000, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'json' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1000, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'idPush', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'idPush' => 1,
			'date' => 1426027715,
			'text' => 'Lorem ipsum dolor sit amet',
			'json' => 'Lorem ipsum dolor sit amet'
		),
	);

}
