<?php
App::uses('Appuser', 'Model');

/**
 * Appuser Test Case
 *
 */
class AppuserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.appuser'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Appuser = ClassRegistry::init('Appuser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Appuser);

		parent::tearDown();
	}

}
