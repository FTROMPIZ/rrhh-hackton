<?php
App::uses('Push', 'Model');

/**
 * Push Test Case
 *
 */
class PushTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.push'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Push = ClassRegistry::init('Push');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Push);

		parent::tearDown();
	}

}
