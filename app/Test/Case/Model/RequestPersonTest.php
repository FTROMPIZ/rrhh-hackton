<?php
App::uses('RequestPerson', 'Model');

/**
 * RequestPerson Test Case
 *
 */
class RequestPersonTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.request_person'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RequestPerson = ClassRegistry::init('RequestPerson');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RequestPerson);

		parent::tearDown();
	}

}
