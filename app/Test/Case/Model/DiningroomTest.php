<?php
App::uses('Diningroom', 'Model');

/**
 * Diningroom Test Case
 *
 */
class DiningroomTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.diningroom'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Diningroom = ClassRegistry::init('Diningroom');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Diningroom);

		parent::tearDown();
	}

}
