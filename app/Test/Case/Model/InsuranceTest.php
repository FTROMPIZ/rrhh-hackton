<?php
App::uses('Insurance', 'Model');

/**
 * Insurance Test Case
 *
 */
class InsuranceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.insurance'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Insurance = ClassRegistry::init('Insurance');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Insurance);

		parent::tearDown();
	}

}
