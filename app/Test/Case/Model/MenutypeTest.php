<?php
App::uses('Menutype', 'Model');

/**
 * Menutype Test Case
 *
 */
class MenutypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.menutype'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Menutype = ClassRegistry::init('Menutype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Menutype);

		parent::tearDown();
	}

}
