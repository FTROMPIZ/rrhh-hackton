<?php
App::uses('Usermovement', 'Model');

/**
 * Usermovement Test Case
 *
 */
class UsermovementTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.usermovement'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Usermovement = ClassRegistry::init('Usermovement');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Usermovement);

		parent::tearDown();
	}

}
