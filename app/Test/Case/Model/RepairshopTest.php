<?php
App::uses('Repairshop', 'Model');

/**
 * Repairshop Test Case
 *
 */
class RepairshopTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.repairshop'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Repairshop = ClassRegistry::init('Repairshop');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Repairshop);

		parent::tearDown();
	}

}
