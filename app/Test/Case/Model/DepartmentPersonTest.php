<?php
App::uses('DepartmentPerson', 'Model');

/**
 * DepartmentPerson Test Case
 *
 */
class DepartmentPersonTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.department_person'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DepartmentPerson = ClassRegistry::init('DepartmentPerson');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DepartmentPerson);

		parent::tearDown();
	}

}
