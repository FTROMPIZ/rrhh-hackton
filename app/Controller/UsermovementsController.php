<?php
App::uses('CustomsController', 'Controller');
/**
 * Usermovements Controller
 *
 * @property Usermovement $Usermovement
 * @property PaginatorComponent $Paginator
 */
class UsermovementsController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
        public $uses = array('Usermovement','User', 'Userip');
	public $components = array('Paginator');

        function beforeFilter(){ 
            parent::beforeFilter();   
            $user = $this->Session->read('Auth.User');
            if ($user['username']){
                $this->set('username', $user['username']);
            }
            if ($user['role']=="Super-Admin"){
                $this->set('userRole', $user['role']);
            }
            if ($user['role']=="MobileMedia-Admin"){
                $this->set('userRoleMM', $user['role']);
            }
        } 
/**
 * index method
 *
 * @return void
 */
	public function index($userFilter = null,$date = null) {
                $branches = $this->User->find('all', array('order' => array('User.name' => 'asc')));
                $combo = array();
                $combo[0] =  "Seleccione un Usuario...";
                foreach($branches as $branch2){
                    $combo[$branch2['User']['idUser']] = $branch2['User']['name'];
                }	
                $this->set('userList',$combo);
                if(!($userFilter!='0')||(!$userFilter)){
                    if(!$date){
                        $this->set('usermovements', $this->Usermovement->find('all'));
                    }else{
                        $this->set('usermovements', $this->Usermovement->find('all',array('conditions' => array("DATE_FORMAT(Usermovement.date, '%Y-%m-%d')"=> $date))));
                    }
                }else{
                    if(!$date){
                        $this->set('usermovements', $this->Usermovement->find('all',array('conditions' => array('User_idUser'=> $userFilter))));
                    }else{
                        $this->set('usermovements', $this->Usermovement->find('all',array('conditions' => array('User_idUser'=> $userFilter,"DATE_FORMAT(Usermovement.date, '%Y-%m-%d')"=> $date))));
                    }
                }
                
                $this->set('thisUsermovement', $this);
                if ($this->request->is(array('post', 'put'))) {
                    if(isset($this->request->data['date'])){
                        $this->request->data['Usermovement']['date']=date('Y-m-d',strtotime($this->request->data['date']));
                        if($this->request->data['Usermovement']['date']=='1970-01-01'){
                            $this->request->data['Usermovement']['date']="";
                        }
                        if(!isset($this->request->data['Usermovement']['user'])){
                            return $this->redirect(array('controller'=>'usermovements','action' => 'index','null',$this->request->data['Usermovement']['date']));
                        }
                        else{
                            return $this->redirect(array('controller'=>'usermovements','action' => 'index',$this->request->data['Usermovement']['user'],$this->request->data['Usermovement']['date']));
                        }
                    }
                    else{
                        if(!isset($this->request->data['Usermovement']['user'])){
                            return $this->redirect(array('controller'=>'usermovements','action' => 'index'));
                        }
                        else{
                            return $this->redirect(array('controller'=>'usermovements','action' => 'index',$this->request->data['Usermovement']['user']));
                        }
                    }
                }
	}
}