<?php
App::uses('CustomsController', 'Controller');
/**
 * Appusers Controller
 *
 * @property Appuser $Appuser
 * @property PaginatorComponent $Paginator
 */
class OpenhoursController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
    public $uses = array('Openhour', 'Usermovement');
    public $components = array('Paginator');

    function beforeFilter(){   
        $action = $this->params['action'];
        parent::beforeFilter();
        $user = $this->Session->read('Auth.User');
  
        if ($user['username']){
            $this->set('username', $user['username']);
        }
        if ($user['role']=="Super-Admin"){
            $this->set('userRole', $user['role']);
        }
        if ($user['role']=="MobileMedia-Admin"){
            $this->set('userRoleMM', $user['role']);
        }
    } 
/**
 * index method
 *
 * @return void
 */
    public function index() {
        $this->Openhour->recursive = 0;
        $this->set('openhours', $this->Paginator->paginate());
    }

/**
 * add method
 *
 * @return void
 */
    public function add() {
        if ($this->request->is('post')) {
            $this->Openhour->create();

            $this->request->data['Openhour']['schedule_start'] = date("H:i:s", strtotime($this->request->data['Openhour']['schedule_start']));
            $this->request->data['Openhour']['schedule_finish'] = date("H:i:s", strtotime($this->request->data['Openhour']['schedule_finish']));
            if ($this->Openhour->save($this->request->data)) {
                $user = $this->Session->read('Auth.User');
                $this->request->data['Openhour']['idOpenHour']=$this->Openhour->getLastInsertID();
                $array_move =array('User_idUser'=>$user['idUser'],
                                   'action'=>'add',
                                   'table'=>'openhours',
                                   'value'=> json_encode($this->request->data));
                $this->Usermovement->create();
                $this->Usermovement->save($array_move);
                $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El horario se a <a href="#" class="alert-link">Agregado con Éxito</a>. </div>');
                return $this->redirect(array('action' => 'index'));
            } 
            else
            {
                $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Ip no pudo ser Agregada, Intenta nuevamente </div>');
            }
        }
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {
        $this->Openhour->id = $id;
        if (!$this->Openhour->exists($id)) {
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Clínica es Inválida </div>');
        }

        $consulta = $this->Openhour->findByIdopenhour($id);
        $s = date("h:i A", strtotime($consulta['Openhour']['schedule_start']));
        $f = date("h:i A", strtotime($consulta['Openhour']['schedule_finish']));
        
        $this->set('start', $s);
        $this->set('finish', $f);

        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Openhour']['schedule_start'] = date("H:i:s", strtotime($this->request->data['Openhour']['schedule_start']));
            $this->request->data['Openhour']['schedule_finish'] = date("H:i:s", strtotime($this->request->data['Openhour']['schedule_finish']));

            if ($this->Openhour->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'edit',
                                                   'table'=>'openhours',
                                                   'value'=> json_encode($this->request->data));
                                $this->Usermovement->create();
                                $this->Usermovement->save($array_move);
                $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Clínica se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
                return $this->redirect(array('action' => 'index', $id));
            } else {
                $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Clínica no pudo ser Editada, Intenta nuevamente </div>');
            }
        } else {
            $options = array('conditions' => array('Openhour.' . $this->Openhour->primaryKey => $id));
            $this->request->data = $this->Openhour->find('first', $options);
        }
    }
        

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 
    public function delete($id = null) {
        $this->Openhour->id = $id;
        if (!$this->Openhour->exists()) {
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El horario es Inválido </div>');
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Openhour->delete()) {
            $user = $this->Session->read('Auth.User');
            $array_move =array('User_idUser'=>$user['idUser'],
                               'action'=>'delete',
                               'table'=>'openhour',
                               'value'=> $id);
            $this->Usermovement->create();
            $this->Usermovement->save($array_move);
            $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El horario se a <a href="#" class="alert-link">Eliminado con Éxito</a>. </div>');
        } else {
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El horario no pudo ser Eliminado, Intenta nuevamente </div>');
        }
        return $this->redirect(array('action' => 'index'));
    }
}
