<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CustomsController', 'Controller');
//App::import('Vendor', 'Uploader.Uploader');
/**
 * Fileuploads Controller
 *
 * @property Fileupload $Fileupload
 * @property PaginatorComponent $Paginator
 */
class FileuploadsController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
  public $uses = array('Fileupload','Event','News','Branch','Clinic','Repairshop','Diningroom','Usermovement');
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Fileupload->recursive = 0;
		$this->set('fileuploads', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
	}

	public function viewRepairshop($id = null) {
	}

/**
 * add method
 *
 * @return void
 */
	public function addEvent() {
    $branches = $this->Branch->find('all', array('order' => array('Branch.name' => 'asc')));
    $combo = array();
    foreach($branches as $branch){
      $combo[$branch['Branch']['idBranch']] = $branch['Branch']['name'];
    }	
    $this->set('branches',$combo);
		if ($this->request->is('post')) {
			$this->Fileupload->create();
      if($this->request->data['Fileupload']['archivo']['name']!=''){
        $fileName=$this->request->data['Fileupload']['archivo']['name'];
        $this->request->data['Fileupload']['fileName'] = $fileName;
        if ($this->Fileupload->save($this->request->data)) {
          $id = $this->Fileupload->query("SELECT max(id) FROM fileupload");
          $directorio=Router::url('/', true).'/app/webroot/files/fileupload/archivo/'.$id[0][0]['max(id)'].'/'.$fileName;
          $array_event=array('imgPath'=>$directorio,
                             'name'=>$this->request->data['Fileupload']['name'],
                             'description'=>$this->request->data['Fileupload']['description'],
                             'place'=>$this->request->data['Fileupload']['place'],
                             'date'=>date('Y-m-d H:i:s',strtotime($this->request->data['date'].' '.$this->request->data['time'])),
                             'Branch_idBranch'=>$this->request->data['Fileupload']['Branch_idBranch'],
                             'outstanding'=>$this->request->data['Fileupload']['outstanding']);
          $this->Event->create();
          if($this->Event->save($array_event)){
            $user = $this->Session->read('Auth.User');
            $this->request->data['Event']['idEvent']=$this->Event->getLastInsertID();
            $array_move =array('User_idUser'=>$user['idUser'],
                               'action'=>'add',
                               'table'=>'event',
                               'value'=> json_encode($this->request->data));
            $this->Usermovement->create();
            $this->Usermovement->save($array_move);
            $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Evento se a <a href="#" class="alert-link">Salvado con Éxito</a>. </div>');
            return $this->redirect(array('controller'=>'events','action' => 'view',$this->Event->getLastInsertID()));
          }else{
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El evento no puedo ser creado </div>');
          }
        } else {
          $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Archivo no pudo subirse, Reintentelo </div>');
        }
      }
      else{
        $array_event=array('name'=>$this->request->data['Fileupload']['name'],
                           'description'=>$this->request->data['Fileupload']['description'],
                           'place'=>$this->request->data['Fileupload']['place'],
                           'date'=>date('Y-m-d H:i:s',strtotime($this->request->data['date'].' '.$this->request->data['time'])),
                           'Branch_idBranch'=>$this->request->data['Fileupload']['Branch_idBranch'],
                           'outstanding'=>$this->request->data['Fileupload']['outstanding']);
        $this->Event->create();
        if($this->Event->save($array_event)){
          $user = $this->Session->read('Auth.User');
          $this->request->data['Event']['idEvent']=$this->Event->getLastInsertID();
          $array_move =array('User_idUser'=>$user['idUser'],
                             'action'=>'add',
                             'table'=>'event',
                             'value'=> json_encode($this->request->data));
          $this->Usermovement->create();
          $this->Usermovement->save($array_move);
          $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Noticia se a <a href="#" class="alert-link">Salvado con Éxito</a>. </div>');
          return $this->redirect(array('controller'=>'events','action' => 'view',$this->Event->getLastInsertID()));
        }else{
          $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El evento no puedo ser creado </div>');
        }
      }
		}
	}
        
  public function editEvent($idEvent = null) {
		if (!$this->Event->exists($idEvent)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>el Evento es Inválido </div>');
      return $this->redirect(array('controller'=>'events','action' => 'index'));
		}
		if ($this->request->is('post')) {
			$this->Fileupload->create();
      $fileName=$this->request->data['Fileupload']['archivo']['name'];
      $this->request->data['Fileupload']['fileName'] = $fileName;
			if ($this->Fileupload->save($this->request->data)) {
        $id = $this->Fileupload->query("SELECT max(id) FROM fileupload");
        $directorio=Router::url('/', true).'/app/webroot/files/fileupload/archivo/'.$id[0][0]['max(id)'].'/'.$fileName;

        $event = $quer=$this->Event->query("SELECT date FROM event WHERE idEvent=".$idEvent);

        $array_event=array('idEvent'=>$idEvent,
                           'imgPath'=>$directorio,
                           'date' => $event[0]['event']['date']);
        //$this->Event->create();
        if($this->Event->save($array_event)){
          $user = $this->Session->read('Auth.User');
          $array_move =array('User_idUser'=>$user['idUser'],
                             'action'=>'editImage',
                             'table'=>'event',
                             'value'=> json_encode($this->request->data));
          $this->Usermovement->create();
          $this->Usermovement->save($array_move);
          $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Evento se a <a href="#" class="alert-link">Salvado con Éxito</a>. </div>');
          return $this->redirect(array('controller'=>'events','action' => 'view',$idEvent));
        }else{
          $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El evento no puedo ser creado </div>');
        }
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Archivo no pudo subirse, Reintentelo </div>');
			}
		}
	}
              
	public function addNews() {
		if ($this->request->is('post')) {
			$this->Fileupload->create();
      if($this->request->data['Fileupload']['archivo']['name']!=''){
        $fileName=$this->request->data['Fileupload']['archivo']['name'];
        $this->request->data['Fileupload']['fileName'] = $fileName;
        if ($this->Fileupload->save($this->request->data)) {
          $id = $this->Fileupload->query("SELECT max(id) FROM fileupload");
          $directorio=Router::url('/', true).'/app/webroot/files/fileupload/archivo/'.$id[0][0]['max(id)'].'/'.$fileName;
          $array_event=array('imgPath'=>$directorio,
                             'name'=>$this->request->data['Fileupload']['name'],
                             'description'=>$this->request->data['Fileupload']['description'],
                             'date'=>date('Y-m-d H:i:s',strtotime($this->request->data['date'].' '.$this->request->data['time'])),
                             );
          $this->News->create();
          if($this->News->save($array_event)){
            $user = $this->Session->read('Auth.User');
            $this->request->data['News']['idNews']=$this->News->getLastInsertID();
            $array_move =array('User_idUser'=>$user['idUser'],
                               'action'=>'add',
                               'table'=>'news',
                               'value'=> json_encode($this->request->data));
            $this->Usermovement->create();
            $this->Usermovement->save($array_move);
            $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Noticia se a <a href="#" class="alert-link">Salvado con Éxito</a>. </div>');
            return $this->redirect(array('controller'=>'news','action' => 'view',$this->News->getLastInsertID()));
          }else{
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Noticia no puedo ser creada </div>');
          }
        } else {
          $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Archivo no pudo subirse, Reintentelo </div>');
        }
      }
      else{
        $array_event=array('name'=>$this->request->data['Fileupload']['name'],
                           'description'=>$this->request->data['Fileupload']['description'],
                           'date'=>date('Y-m-d H:i:s',strtotime($this->request->data['date'].' '.$this->request->data['time'])),
                           );
        $this->News->create();
        if($this->News->save($array_event)){
          $user = $this->Session->read('Auth.User');
          $this->request->data['News']['idNews']=$this->News->getLastInsertID();
          $array_move =array('User_idUser'=>$user['idUser'],
                             'action'=>'add',
                             'table'=>'news',
                             'value'=> json_encode($this->request->data));
          $this->Usermovement->create();
          $this->Usermovement->save($array_move);
          $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Noticia se a <a href="#" class="alert-link">Salvado con Éxito</a>. </div>');
          return $this->redirect(array('controller'=>'news','action' => 'view',$this->News->getLastInsertID()));
        }else{
          $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El evento no puedo ser creado </div>');
        }
      }
		}
	}
        
  public function editNews($idEvent = null) {
    $branches = $this->Branch->find('all', array('order' => array('Branch.name' => 'asc')));
    $combo = array();
    foreach($branches as $branch){
        $combo[$branch['Branch']['idBranch']] = $branch['Branch']['name'];
    }	
    $this->set('branches',$combo);
		if (!$this->News->exists($idEvent)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>el Evento es Inválido </div>');
      return $this->redirect(array('controller'=>'news','action' => 'index'));
		}
		if ($this->request->is('post')) {
			$this->Fileupload->create();
      $fileName=$this->request->data['Fileupload']['archivo']['name'];
      $this->request->data['Fileupload']['fileName'] = $fileName;
			if ($this->Fileupload->save($this->request->data)) {
        $id = $this->Fileupload->query("SELECT max(id) FROM fileupload");
        $directorio=Router::url('/', true).'/app/webroot/files/fileupload/archivo/'.$id[0][0]['max(id)'].'/'.$fileName;
        $newAux = $quer=$this->News->query(
                                            "SELECT date FROM news WHERE idNews=".$idEvent);
        
        $array_event=array('idNews'=>$idEvent,
                           'imgPath'=>$directorio,
                           'date' => $newAux[0]['news']['date']);
        //$this->News->create();
        if($this->News->save($array_event)){
            $user = $this->Session->read('Auth.User');
            $array_move =array('User_idUser'=>$user['idUser'],
                               'action'=>'editImage',
                               'table'=>'news',
                               'value'=> json_encode($this->request->data));
            $this->Usermovement->create();
            $this->Usermovement->save($array_move);
            $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Noticia se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
            return $this->redirect(array('controller'=>'news','action' => 'view',$idEvent));
        }else{
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Noticia no puedo ser creada </div>');
        }
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Archivo no pudo subirse, Reintentelo </div>');
			}
		}
	}
  
  private function thereIsAMenuOnDate($date)
  {
    $array_menu = $this->Diningroom->query(
                                            'SELECT * '
                                            . 'FROM diningroom d ' 
                                            . 'WHERE d.date like "%'.$date.'%"'
                                            );
    return (count($array_menu) > 0);
  }

	public function addDiningroom() 
  {
    $branches = $this->Branch->find('all', array('order' => array('Branch.name' => 'asc')));
    $combo = array();
    foreach($branches as $branch){
        $combo[$branch['Branch']['idBranch']] = $branch['Branch']['name'];
    }	
    $this->set('branches',$combo);
    if ($this->request->is('post')) 
    {
      if (!$this->thereIsAMenuOnDate(date('Y-m-d',strtotime($this->request->data['date']))))
      {
        $this->Fileupload->create();
        if($this->request->data['Fileupload']['archivo']['name']!='')
        {
          $fileName=$this->request->data['Fileupload']['archivo']['name'];
          $this->request->data['Fileupload']['fileName'] = $fileName;
          if ($this->Fileupload->save($this->request->data)) 
          {
            $id = $this->Fileupload->query("SELECT max(id) FROM fileupload");
            $directorio=Router::url('/', true).'/app/webroot/files/fileupload/archivo/'.$id[0][0]['max(id)'].'/'.$fileName;
            $array_event=array('imgPath'=>$directorio,
                               'date'=>date('Y-m-d',strtotime($this->request->data['date'])),
                               'Branch_idBranch'=>$this->request->data['Fileupload']['Branch_idBranch'],
                               );
            $this->Diningroom->create();
            if($this->Diningroom->save($array_event))
            {
              $user = $this->Session->read('Auth.User');
              $this->request->data['Diningroom']['idDiningRoom']=$this->News->getLastInsertID();
              $array_move =array('User_idUser'=>$user['idUser'],
                                 'action'=>'add',
                                 'table'=>'diningroom',
                                 'value'=> json_encode($this->request->data));
              $this->Usermovement->create();
              $this->Usermovement->save($array_move);
              $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Comedor se a <a href="#" class="alert-link">Salvado con Éxito</a>. </div>');
              return $this->redirect(array('controller'=>'diningrooms','action' => 'view',$this->Diningroom->getLastInsertID()));
            }
            else
            {
              $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Comedor no puedo ser creado </div>');
            }
          } 
          else
          {
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Archivo no pudo subirse, Reintentelo </div>');
          }
        }
        else
        {
          $array_event=array(
                              'date'=>date('Y-m-d',strtotime($this->request->data['date'])),
                              'Branch_idBranch'=>$this->request->data['Fileupload']['Branch_idBranch'],
                             );
          $this->Diningroom->create();
          if($this->Diningroom->save($array_event))
          {
            $user = $this->Session->read('Auth.User');
            $this->request->data['Diningroom']['idDiningRoom']=$this->News->getLastInsertID();
            $array_move =array('User_idUser'=>$user['idUser'],
                               'action'=>'add',
                               'table'=>'diningroom',
                               'value'=> json_encode($this->request->data));
            $this->Usermovement->create();
            $this->Usermovement->save($array_move);
            $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Menú diario se a <a href="#" class="alert-link">Salvado con Éxito</a>. </div>');
            return $this->redirect(array('controller'=>'diningrooms','action' => 'view',$this->Diningroom->getLastInsertID()));
          }
          else
          {
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Menú diario no puedo ser creado </div>');
          }
        }
      }
      else
      {
        $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>Ya existe un Menú diario para la fecha seleccionada</div>');
      }
       
    }
	}

  public function editDiningroom($idEvent = null) {
		if (!$this->Diningroom->exists($idEvent)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>el Evento es Inválido </div>');
      return $this->redirect(array('controller'=>'diningrooms','action' => 'index'));
		}
		if ($this->request->is('post')) {
			$this->Fileupload->create();
      $fileName=$this->request->data['Fileupload']['archivo']['name'];
      $this->request->data['Fileupload']['fileName'] = $fileName;
			if ($this->Fileupload->save($this->request->data)) {
        $id = $this->Fileupload->query("SELECT max(id) FROM fileupload");
        $directorio=Router::url('/', true).'/app/webroot/files/fileupload/archivo/'.$id[0][0]['max(id)'].'/'.$fileName;
        $dinigAux = $quer=$this->Diningroom->query(
                                            "SELECT date FROM diningroom WHERE idDiningRoom=".$idEvent);
        $array_event=array('idDiningRoom'=>$idEvent,
                           'imgPath'=>$directorio,
                           'date' => $dinigAux[0]['diningroom']['date']);
        //$this->Diningroom->create();
        if($this->Diningroom->save($array_event)){
            $user = $this->Session->read('Auth.User');
            $array_move =array('User_idUser'=>$user['idUser'],
                               'action'=>'editImage',
                               'table'=>'diningroom',
                               'value'=> json_encode($this->request->data));
                $this->Usermovement->create();
                $this->Usermovement->save($array_move);
            $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Comedor se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
            return $this->redirect(array('controller'=>'diningrooms','action' => 'view',$idEvent));
        }else{
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Comedor no puedo ser creado </div>');
        }
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Archivo no pudo subirse, Reintentelo </div>');
			}
		}
	}   
        
	public function bulkloaderClinics() 
  {
    if ($this->request->is('post')) 
    {
      $this->Fileupload->create();
      if($this->request->data['Fileupload']['archivo']['name']!='')
      {
        $fileName=$this->request->data['Fileupload']['archivo']['name'];
        $this->request->data['Fileupload']['fileName'] = $fileName;
        if ($this->Fileupload->save($this->request->data)) 
        {
          $id = $this->Fileupload->query("SELECT max(id) FROM fileupload");
          $directorio='../webroot/files/fileupload/archivo/'.$id[0][0]['max(id)'];
          $dir = new Folder($directorio);

          //$array_lines = explode( "\n", file_get_contents($dir->pwd().'/'.$fileName));

          $lineNumber = 0;
          $fieldNumber = 0;
          $array_body = array();
          $errorLog="";
          
          $file_handle = fopen($dir->pwd().'/'.$fileName, "rb");
          $line_of_text = fgetcsv($file_handle, 10240, ';');
          $lineNumber = $lineNumber+1;
          do  
          {
            $line_of_text = fgetcsv($file_handle, 10240, ';');
            if($line_of_text!=false){
              foreach ($line_of_text as $field)
              {
                $array_body[$fieldNumber]=$field;
                $fieldNumber=$fieldNumber+1;
              }
              
              $array_save=array(
                                  'state'=>utf8_encode($array_body[0]),
                                  'city'=>utf8_encode($array_body[1]),
                                  'name'=>utf8_encode($array_body[2]),
                                  'address'=>utf8_encode($array_body[3]),
                                  'description'=>utf8_encode($array_body[4]),
                                  'phone'=>($array_body[5] != "" ? $array_body[5].'/' : "" ).
                                           ($array_body[6] != "" ? $array_body[6].'/' : "" ).
                                           ($array_body[7] != "" ? $array_body[7] : "" )
                           );
              $this->Clinic->create();
              if($this->Clinic->save($array_save))
              {
              }
              else
              {
                $err=$this->Clinic->validationErrors;
                foreach ($err as $theErr)
                {
                  $errorLog=$array_body[0].": Error, ".key($err)." should be ".$theErr[0]."\n";
                }
              }

              $fieldNumber = 0;
              $lineNumber = $lineNumber+1;
            }
          }
          while(!feof($file_handle));

          fclose($file_handle);

          $dir2 = new Folder("");
          $file = new File($dir2->pwd().'/LogClinicas.txt');
          $contents = $file->read();
          $file->write($errorLog);
          $file->close();
          $user = $this->Session->read('Auth.User');
          $array_move =array('User_idUser'=>$user['idUser'],
                             'action'=>'BulkloaderClinics',
                             'table'=>'clinic',
                             'value'=> '');
          $this->Usermovement->create();
          $this->Usermovement->save($array_move);

          return $this->redirect(array('action' => 'view'));
        } 
        else 
        {
          $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Archivo no pudo subirse, Reintentelo </div>');
        }
      }
    }
	}     
        
	public function bulkloaderRepairshop() {
		if ($this->request->is('post')) {
			$this->Fileupload->create();
      if($this->request->data['Fileupload']['archivo']['name']!=''){
        $fileName=$this->request->data['Fileupload']['archivo']['name'];
        $this->request->data['Fileupload']['fileName'] = $fileName;
        if ($this->Fileupload->save($this->request->data)) {
          $id = $this->Fileupload->query("SELECT max(id) FROM fileupload");
          $directorio='../webroot/files/fileupload/archivo/'.$id[0][0]['max(id)'];
          $dir = new Folder($directorio);
          
          //$array_lines = explode("\n", file_get_contents($dir->pwd().'/'.$fileName));
          
          $lineNumber = 0;
          $fieldNumber = 0;
          $array_body = array();
          $errorLog="";
          
          $file_handle = fopen($dir->pwd().'/'.$fileName, "rb");
  
          while (!feof($file_handle) ) 
          {
              $line_of_text = fgetcsv($file_handle, 10240, ';');

            if($line_of_text!=false){
              foreach ($line_of_text as $field)
              {
                  if($lineNumber!=0)
                  {
                      $array_body[$fieldNumber]=$field;
                      $fieldNumber=$fieldNumber+1;
                  }
              }
              if($lineNumber!=0)
              {
                  $array_save=array(
                                      'state'=>utf8_encode($array_body[0]),
                                      'city'=>utf8_encode($array_body[1]),
                                      'name'=>utf8_encode($array_body[2]),
                                      'address'=>utf8_encode($array_body[3]),
                                      'description'=>utf8_encode($array_body[4]),
                                      'phone'=>($array_body[5] != "" ? $array_body[5].'/' : "" ).
                                                  ($array_body[6] != "" ? $array_body[6].'/' : "" ).
                                                  ($array_body[7] != "" ? $array_body[7] : "" )
                               );
                  $this->Repairshop->create();
                  if($this->Repairshop->save($array_save))
                  {
                  }
                  else
                  {
                      $err=$this->Repairshop->validationErrors;
                      foreach ($err as $theErr)
                      {
                          $errorLog=$array_body[0].": Error, ".key($err)." should be ".$theErr[0]."\n";
                      }
                  }
              }
              $fieldNumber = 0;
              $lineNumber = $lineNumber+1;
            }
          }

          fclose($file_handle);

          $dir2 = new Folder("");
          $file = new File($dir2->pwd().'/LogTalleres.txt');
          $contents = $file->read();
          $file->write($errorLog);
          $file->close();
          $user = $this->Session->read('Auth.User');
          $array_move =array('User_idUser'=>$user['idUser'],
                             'action'=>'BulkloaderRepairshop',
                             'table'=>'repairshop',
                             'value'=> '');
          $this->Usermovement->create();
          $this->Usermovement->save($array_move);
              
          return $this->redirect(array('action' => 'viewRepairshop'));
        } 
        else 
        {
          $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Archivo no pudo subirse, Reintentelo </div>');
        }
      }
		}
	}
  
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Fileupload->exists($id)) {
			throw new NotFoundException(__('Invalid fileupload'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Fileupload->save($this->request->data)) {
				$this->Session->setFlash(__('The fileupload has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fileupload could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Fileupload.' . $this->Fileupload->primaryKey => $id));
			$this->request->data = $this->Fileupload->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Fileupload->id = $id;
		if (!$this->Fileupload->exists()) {
			throw new NotFoundException(__('Invalid fileupload'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Fileupload->delete()) {
			$this->Session->setFlash(__('The fileupload has been deleted.'));
		} else {
			$this->Session->setFlash(__('The fileupload could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
