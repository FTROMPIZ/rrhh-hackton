<?php
App::uses('CustomsController', 'Controller');
/**
 * News Controller
 *
 * @property News $News
 * @property PaginatorComponent $Paginator
 */
class NewsController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
    public $uses = array('News','Event','Usermovement','User', 'Userip');
	public $components = array('Paginator');

    function beforeFilter(){  
        $action = $this->params['action'];
  
        if(($action != "findNewsByIdWebService") && ($action != "outstandingNewsAndEventsWebService"))
        {
            parent::beforeFilter();
        }  
        $this->Auth->allow('findNewsByIdWebService','outstandingNewsAndEventsWebService');
        $user = $this->Session->read('Auth.User');
        if ($user['username']){
            $this->set('username', $user['username']);
        }
        if ($user['role']=="Super-Admin"){
            $this->set('userRole', $user['role']);
        }
        if ($user['role']=="MobileMedia-Admin"){
            $this->set('userRoleMM', $user['role']);
        }
        $result = '';
            for($i=0; $i<strlen('Noticias'); $i++) {
                $char = substr('Noticias', $i, 1);
                $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt"))-1, 1);
                $char2 = chr(ord($char)+ord($keychar));
                $result.=$char2;
             }
            $encriptado=base64_encode($result);
        $theSQL=$this->News->query('SELECT * FROM module WHERE moduleName="'.$encriptado.'"');
        if($theSQL){
            $this->set('module', $theSQL);
        }
    }
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->News->recursive = 0;
		$this->set('news', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->News->exists($id)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Noticia es Inválida </div>');
                        return $this->redirect(array('action' => 'index'));
		}
		$options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
		$this->set('news', $this->News->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->News->exists($id)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Noticia es Inválida </div>');
            return $this->redirect(array('action' => 'index'));
		}

        $quer=$this->News->query("SELECT DATE_FORMAT(news.`date`, '%d %b %Y') as date, DATE_FORMAT(news.`date`, '%l:%i %p') as hour FROM news WHERE idNews=".$id);
        $this->set('dateEdit',$quer[0][0]['date']);
        $this->set('hourEdit',$quer[0][0]['hour']);
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['News']['date']=date('Y-m-d H:i:s',strtotime($this->request->data['date'].' '.$this->request->data['time']));
			if ($this->News->save($this->request->data)) {
                $user = $this->Session->read('Auth.User');
                $array_move =array('User_idUser'=>$user['idUser'],
                                   'action'=>'edit',
                                   'table'=>'news',
                                   'value'=> json_encode($this->request->data));
                $this->Usermovement->create();
                $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Noticia se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
				return $this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Noticia no pudo ser Editada, Intenta nuevamente </div>');
			}
		} else {
			$options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
			$this->request->data = $this->News->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->News->id = $id;
		if (!$this->News->exists()) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Noticia es Inválida </div>');
                        return $this->redirect(array('action' => 'index'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->News->delete()) {
            $user = $this->Session->read('Auth.User');
            $array_move =array('User_idUser'=>$user['idUser'],
                               'action'=>'delete',
                               'table'=>'news',
                               'value'=> $id);
            $this->Usermovement->create();
            $this->Usermovement->save($array_move);
			$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Noticia se a <a href="#" class="alert-link">Eliminado con Éxito</a>. </div>');
		} else {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Noticia no pudo ser Eliminada, Intenta nuevamente </div>');
		}
		return $this->redirect(array('action' => 'index'));
	}
        
        
    public function outstandingNewsAndEventsWebService(){
        $this->autoRender = false;
        $this->response->type('json');
        $array_news=$this->News->query('SELECT t.* FROM ((SELECT `name`,`description`,`date`,`imgPath`,`createdDate`,"news" as type FROM news) UNION (SELECT `name`,`description`,`date`,`imgPath`,`createdDate`,"event" as type FROM event WHERE outstanding=1)) AS t ORDER BY t.`createdDate` DESC limit 5');
        $array_json=array();
        foreach ($array_news as $news){
            array_push($array_json, array('name'=>$news['t']['name'],
                                          'description'=>$news['t']['description'],
                                          'date'=>$news['t']['date'],
                                         // 'place'=>$news['t']['place'],
                                          'imgPath'=>$news['t']['imgPath'],
                                          'type'=>$news['t']['type']
            ));
        }
        $array_json=array('outstanding'=>$array_json);
        $json = json_encode($array_json);
        $this->response->body($json);
    }

    public function findNewsByIdWebService($id = null){
        $this->autoRender = false;
        $this->response->type('json');
        if($id){
            $events = $this->News->find('all', array('conditions' => array('News.idNews' => $id)));
        }else{
            $events = array();
        }
        $array_json=array();
        foreach ($events as $event){
            array_push($array_json, array('name'=>$event['News']['name'],
                                          'description'=>$event['News']['description'],
                                          'date'=>date('Y-m-d H:i:s',strtotime($event['News']['date'])),
                                          'imgPath'=>$event['News']['imgPath']
                                         ));
        }
        $array_json=array('news'=>$array_json);
        $json = json_encode($array_json);
        $this->response->body($json);
    }
}
