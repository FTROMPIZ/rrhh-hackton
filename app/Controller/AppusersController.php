<?php
App::uses('CustomsController', 'Controller');
/**
 * Appusers Controller
 *
 * @property Appuser $Appuser
 * @property PaginatorComponent $Paginator
 */
class AppusersController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
    public $uses = array('Appuser','Usermovement','User', 'Userip');
	public $components = array('Paginator');

    function beforeFilter(){   
        $action = $this->params['action'];
      
        if($action != "loginWebService")
        {
            parent::beforeFilter();
        }

        $this->Auth->allow('loginWebService');
        $user = $this->Session->read('Auth.User');
        if ($user['username']){
            $this->set('username', $user['username']);
        }
        if ($user['role']=="Super-Admin"){
            $this->set('userRole', $user['role']);
        }
        if ($user['role']=="MobileMedia-Admin"){
            $this->set('userRoleMM', $user['role']);
        }
        $result = '';
            for($i=0; $i<strlen('Usuarios de Aplicación'); $i++) {
                $char = substr('Usuarios de Aplicación', $i, 1);
                $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt"))-1, 1);
                $char2 = chr(ord($char)+ord($keychar));
                $result.=$char2;
             }
            $encriptado=base64_encode($result);
        $theSQL=$this->Appuser->query('SELECT * FROM module WHERE moduleName="'.$encriptado.'"');
        if($theSQL){
            $this->set('module', $theSQL);
        }
    } 
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Appuser->recursive = 0;
		$this->set('appusers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		return $this->redirect(array('action' => 'index'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Appuser->create();
                        $this->request->data['Appuser']['password']=$this->encryptAES128($this->request->data['Appuser']['password'],'MoBiLeMediANETwK');;
			if ($this->Appuser->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $this->request->data['Appuser']['idAppUser']=$this->Appuser->getLastInsertID();
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'add',
                                                   'table'=>'appuser',
                                                   'value'=> json_encode($this->request->data));
                                $this->Usermovement->create();
                                $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Usuario se a <a href="#" class="alert-link">Salvado con Éxito</a>. </div>');
                                return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Usuario no pudo ser Creado, Intenta nuevamente </div>');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Appuser->exists($id)) {
                    $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Usuario es Inválido </div>');
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Appuser->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'edit',
                                                   'table'=>'appuser',
                                                   'value'=> json_encode($this->request->data));
                                $this->Usermovement->create();
                                $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Usuario se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Usuario no pudo ser Editado, Intenta nuevamente </div>');
			}
		} else {
			$options = array('conditions' => array('Appuser.' . $this->Appuser->primaryKey => $id));
			$this->request->data = $this->Appuser->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Appuser->id = $id;
		if (!$this->Appuser->exists()) {
                    $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Usuario es Inválido </div>');
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Appuser->delete()) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'delete',
                                                   'table'=>'appuser',
                                                   'value'=> $id);
                                $this->Usermovement->create();
                                $this->Usermovement->save($array_move);
			$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Usuario se a <a href="#" class="alert-link">Eliminado con Éxito</a>. </div>');
		} else {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Usuario no pudo ser Eliminado, Intenta nuevamente </div>');
		}
		return $this->redirect(array('action' => 'index'));
	}
        
        private function encryptAES128($str, $key){
            $block = mcrypt_get_block_size('rijndael-128', 'ecb');
            $pad = $block - (strlen($str) % $block);
            $str .= str_repeat(chr($pad), $pad);
            return base64_encode(mcrypt_encrypt('rijndael-128', $key, $str, 'ecb'));
       }
        
        private function decryptAES128($str, $key){ 
            $str = mcrypt_decrypt('rijndael-128', $key, base64_decode($str), 'ecb');
            $block = mcrypt_get_block_size('rijndael-128', 'ecb');
            $pad = ord($str[($len = strlen($str)) - 1]);
            return substr($str, 0, strlen($str) - $pad);
        }
        
        public function loginWebService(){
            $this->autoRender = false;
            $this->response->type('json');
            if ($this->request->is('post')){
                $response = $this->request->data;//json_decode($this->request->data['json'] , true);
                $result = '';
                for($i=0; $i<strlen($response['password']); $i++) {
                    $char = substr($response['password'], $i, 1);
                    $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt"))-1, 1);
                    $char2 = chr(ord($char)+ord($keychar));
                    $result.=$char2;
                 }
                $encriptado=base64_encode($result);
                $user=$this->Appuser->query('SELECT idAppUser FROM appuser WHERE (BINARY username LIKE "'.$response['username'].'" OR BINARY email LIKE "'.$response['username'].'") AND password="'.$encriptado.'"');
                if($user){
                    $array_json=array('response'=>'1');
                }else{
                    $array_json=array('response'=>'-1');
                }
            }
            else{
                $array_json=array('response'=>'0');
            }
            $json = json_encode($array_json);
            $this->response->body($json);
        }
}
