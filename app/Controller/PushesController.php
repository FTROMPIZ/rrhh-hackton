<?php
App::uses('Notification', 'Controller/Push');
App::uses('NotificationHub', 'Controller/Push');
App::uses('CustomsController', 'Controller');
/**
 * Pushes Controller
 *
 * @property Push $Push
 * @property PaginatorComponent $Paginator
 */
class PushesController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
    public $uses = array('Push','Usermovement','Event','News','User', 'Userip');
	public $components = array('Paginator');

        
    function beforeFilter(){
        parent::beforeFilter();
        $user = $this->Session->read('Auth.User');
        if ($user['username']){
            $this->set('username', $user['username']);
        }
        if ($user['role']=="Super-Admin"){
            $this->set('userRole', $user['role']);
        }
        if ($user['role']=="MobileMedia-Admin"){
            $this->set('userRoleMM', $user['role']);
        }
        $result = '';
        for($i=0; $i<strlen('Push'); $i++) {
            $char = substr('Push', $i, 1);
            $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt"))-1, 1);
            $char2 = chr(ord($char)+ord($keychar));
            $result.=$char2;
         }
        $encriptado=base64_encode($result);
        $theSQL=$this->Push->query('SELECT * FROM module WHERE moduleName="'.$encriptado.'"');
        if($theSQL){
            $this->set('module', $theSQL);
        }
    }
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Push->recursive = 0;
		$this->set('pushes', $this->Push->find('all',array('order' => array('Push.date' => 'asc'))));
	}

/**
 * add method
 *
 * @return void
 */
	/*public function add() {
                $events = $this->Event->find('all', array('order' => array('Event.name' => 'asc')));
                $combo = array();
                foreach($events as $event){
                    $combo[$event['Event']['idEvent']] = $event['Event']['name'];
                }	
                $this->set('event',$combo);
                $news = $this->News->find('all', array('order' => array('News.name' => 'asc')));
                $combo = array();
                foreach($news as $new){
                    $combo[$new['News']['idNews']] = $new['News']['name'];
                }	
                $this->set('news',$combo);
                
                
		if ($this->request->is('post')) {
                    if($this->request->data['pushType']=='Evento'){
                        $this->request->data['Push']['json']=json_encode(array('pushType'=>'event','idEvent'=>$this->request->data['Push']['events']));
                    }
                    if($this->request->data['pushType']=='Noticia'){
                        $this->request->data['Push']['json']=json_encode(array('pushType'=>'news','idNews'=>$this->request->data['Push']['news']));
                    }
			$this->Push->create();
			if ($this->Push->save($this->request->data)) {
				$hub = new NotificationHub("Endpoint=sb://rrhh-banesco.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=ONM2csQU5NtuT6yIIWEewlBcMcXbhBzp+4j87nLJfRk=", "rrhh-banesco");
				
				if(isset($this->request->data['Push']['json'])){
					$message = '{"data":{"message":"'.$this->request->data['Push']['text'].'","data":'.$this->request->data['Push']['json'].'}}';
					$alert = '{"aps":{"alert":"'.$this->request->data['Push']['text'].'","data":'.$this->request->data['Push']['json'].'}}';
				}
				else{
                                   $message = '{"data":{"message":"'.$this->request->data['Push']['text'].'"}}';
                                   $alert = '{"aps":{"alert":"'.$this->request->data['Push']['text'].'"}}';
                            }
				$this->log($message);
				$notification = new Notification("gcm", $message);
				$hub->sendNotification($notification,"");

				$notification2 = new Notification("apple", $alert);
				//Cuando se termine de configurar la parte de iOS par apush descomentar
				//$hub->sendNotification($notification2,"");

                                $user = $this->Session->read('Auth.User');
                                $this->request->data['Push']['idPush']=$this->Push->getLastInsertID();
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'add',
                                                   'table'=>'push',
                                                   'value'=> json_encode($this->request->data));
                                $this->Usermovement->create();
                                $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Push se a <a href="#" class="alert-link">Salvado con Ã‰xito</a>. </div>');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Push no pudo ser Creado, Intenta nuevamente </div>');
			}
		}
	}*/
    public function add() {
        $events = $this->Event->find('all', array('order' => array('Event.name' => 'asc')));
        $combo = array();
        foreach($events as $event){
            $combo[$event['Event']['idEvent']] = $event['Event']['name'];
        }	
        $this->set('event',$combo);

        $news = $this->News->find('all', array('order' => array('News.name' => 'asc')));
        $combo = array();
        foreach($news as $new){
            $combo[$new['News']['idNews']] = $new['News']['name'];
        }	
        $this->set('news',$combo);
                
		if ($this->request->is('post')) {
            $id_tipo_push = 0;
            $nombre_tipo_push = "";
            $json_push = "";
            
            if($this->request->data['pushType']=='Evento')
            { 
                //$this->request->data['Push']['json']=json_encode(array('pushType'=>'event','idEvent'=>$this->request->data['Push']['events']));
                $evento = $this->Event->find('all', array(
                                                            'conditions' => array(
                                                                                    'Event.idEvent' => $this->request->data['Push']['events']
                                                                                )
                                                        )
                                            );
                $json_push = json_encode(
                                            array(
                                                    'titulo' => $this->request->data['Push']['text'],
                                                    'pushType'=>'event',
                                                    'info' => $evento[0]['Event']
                                                )
                                        );
                $this->request->data['Push']['json'] = $json_push;
                //$id_tipo_push = $this->request->data['Push']['events'];
                //$nombre_tipo_push = "event";
            }
            if($this->request->data['pushType']=='Noticia')
            {
                //$this->request->data['Push']['json']=json_encode(array('pushType'=>'news','idNews'=>$this->request->data['Push']['news']));
                $news = $this->News->find('all', array(
                                                        'conditions' => array(
                                                                                'News.idNews' => $this->request->data['Push']['news']
                                                                            )
                                                    )
                                        );
                
                $json_push = json_encode(
                                        array(
                                                'titulo' => $this->request->data['Push']['text'],
                                                'pushType'=>'news',
                                                'info' => $news[0]['News']
                                            )
                                    );
                $this->request->data['Push']['json'] = $json_push;
                //$id_tipo_push = $this->request->data['Push']['news'];
                //$nombre_tipo_push = "news";
            }

			$this->Push->create();
			if ($this->Push->save($this->request->data)) {
				$hub = new NotificationHub("Endpoint=sb://rrhh-banesco.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=ONM2csQU5NtuT6yIIWEewlBcMcXbhBzp+4j87nLJfRk=", "rrhh-banesco");
				
				if(isset($this->request->data['Push']['json'])){
					//$message = '{"data":{"message":"'.$this->request->data['Push']['text'].'","data":'.$this->request->data['Push']['json'].'}}';
					//$alert = '{"aps":{"alert":"'.$this->request->data['Push']['text'].'","data":'.$this->request->data['Push']['json'].'}}';
                                        $message = '{"message": \''.$json_push.'\' }';
				}
				else{
                                    //$message = '{"data":{"message":"'.$this->request->data['Push']['text'].'"}}';
                                    //$alert = '{"aps":{"alert":"'.$this->request->data['Push']['text'].'"}}';
                                    $message = '{"message":\'{ "titulo": "'.$this->request->data['Push']['text'].'"}\' }';
                                }
				$this->log($message);
                                
                $notification = new Notification("template", $message);
				//$notification = new Notification("gcm", $message);
				$hub->sendNotification($notification,"");

				//$notification2 = new Notification("apple", $alert);
				//Cuando se termine de configurar la parte de iOS par apush descomentar
				//$hub->sendNotification($notification2,"");

                $user = $this->Session->read('Auth.User');
                $this->request->data['Push']['idPush']=$this->Push->getLastInsertID();
                $array_move =array('User_idUser'=>$user['idUser'],
                                   'action'=>'add',
                                   'table'=>'push',
                                   'value'=> json_encode($message));
                $this->Usermovement->create();
                $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Push se a <a href="#" class="alert-link">Salvado con Ã‰xito</a>. </div>');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Push no pudo ser Creado, Intenta nuevamente </div>');
			}
		}
	}
}