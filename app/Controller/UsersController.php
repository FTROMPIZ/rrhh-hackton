<?php
App::uses('CustomsController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'Spreadsheet_Excel_Reader', array('file' => 'php-excel-reader/excel_reader2.php'));
App::import('Vendor', 'PHPExcel/Classes/PHPExcel');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
    
    
    public $uses = array('User','Usermovement', 'Userip', 'State', 'Userpassword', 'Ipwhitelist', 'Openhour','Person','DepartmentPerson','RequestPerson');

    function beforeFilter(){
        $action = $this->params['action'];
      
        if(($action != "forgetPassword") && ($action != 'recoverPassword') && ($action != "login") && ($action != "logout"))
        {
            parent::beforeFilter();
        } 
        $this->Auth->allow('forgetPassword','recoverPassword', 'login', 'logout','login_ws');
        $user = $this->Session->read('Auth.User');
  
        if ($user['username']){
            $this->set('username', $user['username']);
        }
        if ($user['role']=="Super-Admin"){
            $this->set('userRole', $user['role']);
        }
        if ($user['role']=="MobileMedia-Admin"){
            $this->set('userRoleMM', $user['role']);
        }
    }
        
    public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
    public function index() {
        
        //$this->getWorkLetter('prueba','banco',date('Y-m-d H:i:s'));
        /*
        $date_S=date_create("2016-01-15");
        $date_S=date_format($date_S,"Y-m-d H:i:s");
        $date_E=date_create("2016-01-30");
        $date_E=date_format($date_E,"Y-m-d H:i:s");
        */

        //$this->getVacations('prueba',date('Y-m-d H:i:s'),$date_S,$date_E);        
        
        //$this->requestAction('/api/sendEmailVacations/'.'Evelyn/'.$date_S.'/'.$date_S.'/'.$date_E);        

        $users = $this->User->find('all', array(
                'conditions' => array(
                    'OR' => array('User.role' => 'Admin','User.role' => 'Super-Admin')
                    )
                )
            );
        $this->set('users', $users);
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {
            return $this->redirect(array('action' => 'index'));
    }

/**
 * add method
 *
 * @return void
 */
    public function add() {
        $data = $this->State->find('list', array('fields' => array('idState', 'state')));
        $this->set('states', $data);
        if ($this->request->is('post')) {
            $this->User->create();
            $this->request->data['User']['fecha_password'] = date('Y-m-d', time());
            if ($this->User->save($this->request->data)) {
                $user = $this->Session->read('Auth.User');
                $this->request->data['User']['idUser']=$this->User->getLastInsertID();
                $array_move =array('User_idUser'=>$user['idUser'],
                                   'action'=>'add',
                                   'table'=>'user',
                                   'value'=> json_encode($this->request->data));
                $this->Usermovement->create();
                $this->Usermovement->save($array_move);

                // Creo nueva entrada en UserPassword para control
                $array_save = array(
                                    "Userpassword" => array(
                                                            "User_idUser" => $this->request->data['User']['idUser'],
                                                            "password" => $this->request->data['User']['password']
                                                        )
                                    );
                $this->Userpassword->save($array_save);
                $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Usuario se a <a href="#" class="alert-link">Salvado con Éxito</a>. </div>');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Usuario no pudo ser Creado, Intenta nuevamente </div>');
            }
        }
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {
        $data = $this->State->find('list', array('fields' => array('idState', 'state')));
        $this->set('states', $data);
        if (!$this->User->exists($id)) {
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Usuario es Inválido </div>');
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'edit',
                                                   'table'=>'user',
                                                   'value'=> json_encode($this->request->data));
                                    $this->Usermovement->create();
                                    $this->Usermovement->save($array_move);
                $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Usuario se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Usuario no pudo ser Editado, Intenta nuevamente </div>');
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Usuario es Inválido </div>');
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $user = $this->Session->read('Auth.User');
            $array_move =array('User_idUser'=>$user['idUser'],
                               'action'=>'delete',
                               'table'=>'user',
                               'value'=> $id);
            $this->Usermovement->create();
            $this->Usermovement->save($array_move);
            $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Usuario se a <a href="#" class="alert-link">Eliminado con Éxito</a>. </div>');
        } else {
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Usuario no pudo ser Eliminado, Intenta nuevamente </div>');
        }
        return $this->redirect(array('action' => 'index'));
    }

    private function logueo($actual_ip)
    {
        if ($this->Auth->login())
        {
            $user = $this->Session->read('Auth.User');
            $fecha_expiracion = date('Y-m-d', strtotime('-180 day'));

            // Verifica fecha expiración contraseña
            if($user['fecha_password'] <= $fecha_expiracion)
            {
                return $this->redirect(array('controller' => 'users', 'action' => 'modificar_clave'));
            }

            $user_connected = $this->Userip->findAllByuserIduser($user['idUser']);

            if ($user_connected)
            {
                // Verifica si estoy accediendo desde la misma máquina
                if($user_connected[0]['Userip']['session_ip'] != $actual_ip)
                {
                    // Borro la sesion anterior y creo nueva entrada
                    $this->Userip->deleteAll(array('Userip.idUserIp' => $user_connected[0]['Userip']['idUserIp']), false);
                    $array_save = array(
                                        "Userip" => array(
                                                            "User_idUser" => $user['idUser'],
                                                            "session_ip" => $actual_ip
                                                        )
                                    );
                    $this->Userip->save($array_save);
                }
            }
            return $this->redirect(array('controller' => 'users', 'action' => 'index'));
        }
        else
        {
            if($this->Session->read('login.fail'))
            {
                $login_fail = $this->Session->read('login.fail');

                if($login_fail >= 3)
                {
                    $estado = $this->State->field('idState',array('state =' => 'Suspendido'));
                    $u = $this->User->find('first', array('conditions' => 
                                                                    array('User.username' => $this->data['User']['username'])
                                                            )
                                            );
                    $array_save = array(
                                        "User" => array(
                                                            "idUser" => $u['User']['idUser'],
                                                            "state_id" => $estado
                                                        )
                                    );
                    $this->User->save($array_save);
                    $login_fail = 1;
                }
                else
                {
                    $login_fail = $this->Session->read('login.fail') + 1;
                }
            }else{
                $login_fail = 1;
            }
            $user = $this->User->field('idUser',array('username =' => $this->request->data['User']['username']));
            $array_move =array('User_idUser'=>$user,
                               'action'=>'login',
                               'table'=>'user',
                               'value'=> "Intento de acceso fallido");
            $this->Usermovement->create();
            $this->Usermovement->save($array_move);
            $this->Session->write("login.fail",$login_fail);
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Usuario o la Contraseña son erroneos </div>');
        }
    }

    function set_time()
    {
        $today = new DateTime();
        $fecha_inicio = new DateTime();
        $fecha_fin = new DateTime();
        $openhour = $this->Openhour->find('first');
        
        if(count($openhour) != 0)
        {
            $start = explode(':', $openhour['Openhour']['schedule_start']);
            $finish = explode(':', $openhour['Openhour']['schedule_finish']);

            $fecha_inicio->setTime($start[0], $start[1], $start[2]);
            $fecha_fin->setTime($finish[0], $finish[1], $finish[2]);
            
            if(($fecha_inicio <= $today) && ($fecha_fin >= $today))
            {
                return true;
            }
        }
        return false;
    }
        
    function login() 
    {
        if($this->Session->read('Auth.User'))
        {
            return $this->redirect($this->Auth->redirectUrl());
        }
        else
        {
            if ($this->request->is('post')) 
            {
                $user = $this->User->field('idUser',array('username =' => $this->request->data['User']['username']));
                $actual_ip = parent::verIP();
                # Busco si la IP está en la ipwhitelist para acceso
                $ip_permit = $this->Ipwhitelist->findByIp($actual_ip);

                $usuario = $this->User->field('state_id',array('username =' => $this->request->data['User']['username']));
                $role = $this->User->field('role',array('username =' => $this->request->data['User']['username']));
                $estado = $this->State->field('idState',array('state =' => 'Suspendido'));

                if($role == "Super-Admin")
                {
                    $this->logueo($actual_ip);
                }
                elseif($role == "Admin")
                {
                    if($usuario != $estado)
                    {
                        if($ip_permit != null)
                        {
                            if($this->set_time())
                            {
                                $this->logueo($actual_ip);
                            }
                            else
                            {
                                    $array_move =array('User_idUser'=>$user,
                                                       'action'=>'login',
                                                       'table'=>'user',
                                                       'value'=> "Intento de acceso en horario no permitido");
                                    $this->Usermovement->create();
                                    $this->Usermovement->save($array_move);
                                    $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong></strong>El horario en el que usted está intentando acceder a la plataforma no es permitido para su nivel de usuario</div>');
                            }   
                        }
                        else
                        {
                            $array_move =array('User_idUser'=>$user,
                                               'action'=>'login',
                                               'table'=>'user',
                                               'value'=> "Intento de acceso desde IP no permitida");
                            $this->Usermovement->create();
                            $this->Usermovement->save($array_move);
                            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong></strong>No tiene acceso desde este equipo. Contacte al administrador</div>');
                        }
                    }
                    else
                    {
                        $array_move =array('User_idUser'=>$user,
                                           'action'=>'login',
                                           'table'=>'user',
                                           'value'=> "Intento de acceso de usuario suspendido");
                        $this->Usermovement->create();
                        $this->Usermovement->save($array_move);
                        $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong></strong>Usuario Suspendido. Contacte al administrador</div>');
                    } 
                }               
            }
        }  
    }

    function forgetPassword() {
        if($this->Session->read('Auth.User')){
            return $this->redirect(array('controller'=>'pages','action' => 'index'));
        }else{
            if ($this->request->is('post')) {
                $user=$this->User->query("SELECT * FROM user WHERE BINARY email= '".$this->request->data['User']['username']."'");
                if($user){
                    $result = '';
                    for($i=0; $i<strlen($user[0]['user']['idUser']); $i++) {
                        $char = substr($user[0]['user']['idUser'], $i, 1);
                        $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt"))-1, 1);
                        $char2 = chr(ord($char)+ord($keychar));
                        $result.=$char2;
                    }
                    $idEncriptado=base64_encode($result);
                    $Email = new CakeEmail();
                    $Email->config('default');
                    $Email->emailFormat('html','text'); 
                    $mailTo = $user[0]['user']['email'];
                    $Email->from(array('evaluappmsd@mobmedianet.com' => 'Administrador'));
                    $Email->to($user[0]['user']['email']);
                    $Email->subject('Solicitud de Recuperacion de Usuario');
                    $Email->send('Saludos, <br>Ingrese al siguiente link para recuperar su contraseña: <br><br>'.Router::url('/', true).'users/recoverPassword/'.$idEncriptado);
                    return $this->redirect(array('controller'=>'users','action' => 'login'));
                }
                else{
                    return $this->redirect(array('controller'=>'users','action' => 'login'));
                    //$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>EL Correo no Existe </div>');
                }
            }
        }
    }
    
    function recoverPassword($id = null) {
        ini_set('max_execution_time', 300);
        if($this->Session->read('Auth.User')){
            return $this->redirect(array('controller'=>'pages','action' => 'index'));
        }else{
            $result = '';
            $string = base64_decode($id);
            
            for ($i = 0; $i < strlen($string); $i++) {
                $char = substr($string, $i, 1);
                $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt")) - 1, 1);
                $char2 = chr(ord($char) - ord($keychar));
                $result.=$char2;
            }

            if (!$this->User->exists($result)) {
                $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Usuario es Inválido </div>');
            }

            if ($this->request->is(array('post', 'put'))) {
                $this->request->data['User']['idUser']=$result;

                // Obtengo los últimos 10 passwords del user
                $registers = $this->Userpassword->find('all', array(
                                                                    'limit' => 10, 
                                                                    'conditions' => array(
                                                                                        'Userpassword.User_idUser'=> $this->request->data['User']['idUser']
                                                                                    )
                                                                    )
                                                        );
                for ($i = 0; $i < count($registers); $i++) 
                {
                    // Si el password coincide redirecciono y muestro mensaje
                    if($registers[$i]['Userpassword']['password'] == AuthComponent::password($this->request->data['User']['password']))
                    {
                        $idEncriptado=base64_encode($result);
                        $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Clave no puede coincidir con las últimas 10 claves. Intente nuevamente</div>');
                        return $this->redirect(array('controller'=>'users','action' => 'recoverPassword', $id));
                    }
                }

                $this->request->data['User']['fecha_password'] = date('Y-m-d', time());
                if ($this->User->save($this->request->data)) {
                    // Creo nueva entrada de userpassword
                    $array_save = array(
                                    "Userpassword" => array(
                                                            "User_idUser" => $result,
                                                            "password" => $this->request->data['User']['password']
                                                        )
                                    );
                    $this->Userpassword->save($array_save);

                    $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>a Contraseña se a <a href="#" class="alert-link">Restablecido con Éxito</a>. </div>');
                    $this->log($this->request->data,"error");
                    return $this->redirect(array('controller'=>'users','action' => 'login'));
                } else {
                    $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Clave no pudo ser Restablecida, Intenta nuevamente </div>');
                }
            } else {
                $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
                $this->request->data = $this->User->find('first', $options);
            }
        }
    }

    function modificar_clave(){
        $this->layout = false;
        $user = $this->Session->read('Auth.User');
        $user_actual = $this->User->find('first', array('User.idUser' => $user['idUser']));
        //debug($user['idUser']);

        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['User']['idUser'] = $user['idUser'];
            $this->request->data['User']['fecha_password'] = date('Y-m-d', time());

            debug($this->request->data['User']['idUser']);
            debug($this->request->data['User']['fecha_password']);

            // Obtengo los últimos 10 passwords del user
            $registers = $this->Userpassword->find('all', array(
                                                                'limit' => 10, 
                                                                'conditions' => array(
                                                                                    'Userpassword.User_idUser'=> $this->request->data['User']['idUser']
                                                                                )
                                                                )
                                                    );
            for ($i = 0; $i < count($registers); $i++)
            {
                // Si el password coincide redirecciono y muestro mensaje
                if($registers[$i]['Userpassword']['password'] == AuthComponent::password($this->request->data['User']['password']))
                {
                    $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Clave no puede coincidir con las últimas 10 claves. Intente nuevamente</div>');
                    return $this->redirect(array('controller'=>'users','action' => 'modificar_clave'));
                }
            }

            if ($this->User->save($this->request->data)) {
                // Creo nueva entrada de userpassword
                $array_save = array(
                                "Userpassword" => array(
                                                        "User_idUser" => $this->request->data['User']['idUser'],
                                                        "password" => $this->request->data['User']['password']
                                                    )
                                );
                $this->Userpassword->save($array_save);

                $array_move =array('User_idUser'=>$user['idUser'],
                                   'action'=>'modificar_clave',
                                   'table'=>'user',
                                   'value'=> "Cambio de contraseña con éxito");
                $this->Usermovement->create();
                $this->Usermovement->save($array_move);

                $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>a Contraseña se a <a href="#" class="alert-link">modificado con Éxito</a>. </div>');
                $this->log($this->request->data,"error");
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            } else {
                $array_move =array('User_idUser'=>$user['idUser'],
                                   'action'=>'modificar_clave',
                                   'table'=>'user',
                                   'value'=> "Error al cambiar contraseña");
                $this->Usermovement->create();
                $this->Usermovement->save($array_move);
                $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Clave no pudo ser modificada, Intenta nuevamente </div>');
            }
        }
    }

    function logout() {
        $user = $this->Session->read('Auth.User');
        $today = date('Y-m-d', time());
        $array_save = array(
                                "User" => array(
                                                    "idUser" => $user['idUser'],
                                                    "ultima_conexion" => $today
                                                )
                            );
        $this->User->save($array_save);
        $this->redirect($this->Auth->logout());
    }

/**
 * WEBSERVICES
 **/

    public function getWorkLetter($username, $reffer_to, $date)
    {
        $user = $this->User->find('all', array(
                'conditions' => array(
                    'User.role' => 'Empleado',
                    'User.username' => $username
                    )
                )
            );

        $info_request = array(
                                'idRequestType' => 1,
                                'idPeople' => $user[0]['Person'][0]['idPeople'],
                                'date_request' => $date,
                                'reffer_to' => $reffer_to,
                                'start_date' => null,
                                'end_date' => null                             
                                );

        $this->RequestPerson->create();

        if($this->RequestPerson->save($info_request))
        {
            $res = array(
                        'codigo' => '100',
                        'mensaje' => 'Su solicitud fue enviada con éxito.',
                    );
        }else{
            $res = array(
                        'codigo' => '400',
                        'mensaje' => 'Su solicitud no pudo ser enviada, intente nuevamente.',
                    );
        }        

        return json_encode($res);
    }

    public function getVacations($username,$date,$date_start,$date_end)
    {
        $user = $this->User->find('all', array(
                'conditions' => array(
                    'User.role' => 'Empleado',
                    'User.username' => $username
                    )
                )
            );

        $info_request = array(
                                'idRequestType' => 2,
                                'idPeople' => $user[0]['Person'][0]['idPeople'],
                                'date_request' => $date,
                                'reffer_to' => null,
                                'start_date' => $date_start,
                                'end_date' => $date_end                             
                                );

        $this->RequestPerson->create();

        if($this->RequestPerson->save($info_request))
        {
            $this->requestAction('/api/sendEmailVacations/'.$user[0]['Person'][0]['name_1'].$date.'/'.$date_start.'/'.$date_end);

            $res = array(
                        'codigo' => '100',
                        'mensaje' => 'Su solicitud fue enviada con éxito.',
                    );
        }else{
            $res = array(
                        'codigo' => '400',
                        'mensaje' => 'Su solicitud no pudo ser enviada, intente nuevamente.',
                    );
        }        

        return json_encode($res);
    }

    public function login_ws($username,$password)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $array_params = array(
                                    'username' => $username,
                                    'password' => $password
                                );
        $params = json_encode($array_params);
        $result = $this->_callCurlPetitionWithParamsJson(parent::URL_AUTH,$params);
        
        $message_return = array();
        if (isset($result))
        {
            $array_result = json_decode($result,true);
            if (array_key_exists("errorMessages", $array_result) || array_key_exists("errors", $array_result))
            // si existe el key de errorMessages hay un problema con la autenticacion
            // sea por el password o contrasena
            {
                $message_return = array(
                                        "Status" => 400,
                                        "mensaje" => $array_result['errorMessages'][0],
                    
                                    );
            }
            elseif(array_key_exists("session", $array_result))
            {
                
                // si no tiene esos keys, el login es exitoso
                $message_return = array(
                                        "Status" => 100,
                                        "mensaje" => "Login Exitoso"
                                    );
            }
            else 
            {
                $message_return = array(
                                        "Status" => 400,
                                        "mensaje" => "Error en la respuesta del servidor. Intentelo mas tarde"
                                    );
            }
        }
        else
        {
            $message_return = array(
                                        "Status" => 400,
                                        "mensaje" => "No se pudo conectar al servidor, intente mas tarde"
                                    );
        }
        
        
        return json_encode($message_return);
    }
    public function _callCurlPetitionWithParamsJson($url,$params)
    {
        $ch = curl_init();
        $headers = array(
            "Content-Type: application/json", 
        );
        curl_setopt($ch, CURLOPT_URL,            $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_POST,           true ); 
        curl_setopt($ch, CURLOPT_COOKIEJAR, parent::PATH_COOKIE_FILE);
        curl_setopt($ch, CURLOPT_COOKIEFILE, parent::PATH_COOKIE_FILE );
        curl_setopt($ch, CURLOPT_HTTPHEADER,    $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS,    $params);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }
}
