<?php
App::uses('CustomsController', 'Controller');
/**
 * Repairshops Controller
 *
 * @property Repairshop $Repairshop
 * @property PaginatorComponent $Paginator
 */
class RepairshopsController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
    public $uses = array('Repairshop','Usermovement','User', 'Userip');
	public $components = array('Paginator');

        function beforeFilter(){   
            $action = $this->params['action'];
      
            if(($action != "repairshopListWebService"))
            {
                parent::beforeFilter();
            }
            $this->Auth->allow('repairshopListWebService');
            $user = $this->Session->read('Auth.User');
            if ($user['username']){
                $this->set('username', $user['username']);
            }
            if ($user['role']=="Super-Admin"){
                $this->set('userRole', $user['role']);
            }
            if ($user['role']=="MobileMedia-Admin"){
                $this->set('userRoleMM', $user['role']);
            }
            $result = '';
                for($i=0; $i<strlen('Talleres'); $i++) {
                    $char = substr('Talleres', $i, 1);
                    $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt"))-1, 1);
                    $char2 = chr(ord($char)+ord($keychar));
                    $result.=$char2;
                 }
                $encriptado=base64_encode($result);
            $theSQL=$this->Repairshop->query('SELECT * FROM module WHERE moduleName="'.$encriptado.'"');
            if($theSQL){
                $this->set('module', $theSQL);
            }
        } 
/**
 * index method
 *
 * @return void
 */
	public function index() 
    {
		$this->Repairshop->recursive = 0;
		$this->set('repairshops', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) 
    {
		if (!$this->Repairshop->exists($id)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Taller es Inválido </div>');
                        return $this->redirect(array('action' => 'index'));
		}
		$options = array('conditions' => array('Repairshop.' . $this->Repairshop->primaryKey => $id));
		$this->set('repairshop', $this->Repairshop->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() 
    {
		if ($this->request->is('post')) {
			$this->Repairshop->create();
			if ($this->Repairshop->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $this->request->data['Repairshop']['idRepairshop']=$this->Repairshop->getLastInsertID();
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'add',
                                                   'table'=>'repairshop',
                                                   'value'=> json_encode($this->request->data));
                                    $this->Usermovement->create();
                                    $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Taller se a <a href="#" class="alert-link">Agregado con Éxito</a>. </div>');
				return $this->redirect(array('action' => 'view',$this->Repairshop->getLastInsertID()));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Taller no pudo ser Agregado, Intenta nuevamente </div>');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) 
    {
		if (!$this->Repairshop->exists($id)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Taller es Inválido </div>');
                        return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Repairshop->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'edit',
                                                   'table'=>'repairshop',
                                                   'value'=> json_encode($this->request->data));
                                    $this->Usermovement->create();
                                    $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Taller se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
				return $this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Taller no pudo ser Editado, Intenta nuevamente </div>');
			}
		} else {
			$options = array('conditions' => array('Repairshop.' . $this->Repairshop->primaryKey => $id));
			$this->request->data = $this->Repairshop->find('first', $options);
            $repairshop = $this->request->data['Repairshop']['state'];
            $repairshopUp   = strtoupper($repairshop);  // Se coloca el estado en mayusculas
            $this->set('estado', $repairshopUp);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) 
    {
		$this->Repairshop->id = $id;
		if (!$this->Repairshop->exists()) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Taller es Inválido </div>');
                        return $this->redirect(array('action' => 'index'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Repairshop->delete()) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'delete',
                                                   'table'=>'repairshop',
                                                   'value'=> $id);
                                    $this->Usermovement->create();
                                    $this->Usermovement->save($array_move);
			$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Taller se a <a href="#" class="alert-link">Eliminada con Éxito</a>. </div>');
		} else {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Taller no pudo ser Eliminado, Intenta nuevamente </div>');
		}
		return $this->redirect(array('action' => 'index'));
	}
        
    public function repairshopListWebService($state = null)
    {
        $this->autoRender = false;
        $this->response->type('json');
        if(!$state){
            $repairshop=$this->Repairshop->find('all');
            if ($repairshop){
                $array_json=array();
                foreach ($repairshop as $array_repairshop){
                    array_push($array_json, array('name'=>$array_repairshop['Repairshop']['name'],
                                                                'address'=>$array_repairshop['Repairshop']['address'],
                                                                'city'=>$array_repairshop['Repairshop']['city'],
                                                                'state'=>$array_repairshop['Repairshop']['state'],
                                                                'description'=>$array_repairshop['Repairshop']['description'],
                                                                'phone'=>$array_repairshop['Repairshop']['phone'],
                            ));
                } 
                $array_json=array('response'=>'1','repairshop'=>$array_json);
            }
            else{
                $array_json=array('response'=>'0');
            }
        }else{
            $state=str_replace("_"," ",$state);
            $repairshop=$this->Repairshop->find('all', array('conditions' => array('state' => $state)));
            if ($repairshop){
                $array_json=array();
                foreach ($repairshop as $array_repairshop){
                    array_push($array_json, array('name'=>$array_repairshop['Repairshop']['name'],
                                                                'address'=>$array_repairshop['Repairshop']['address'],
                                                                'city'=>$array_repairshop['Repairshop']['city'],
                                                                'state'=>$array_repairshop['Repairshop']['state'],
                                                                'description'=>$array_repairshop['Repairshop']['description'],
                                                                'phone'=>$array_repairshop['Repairshop']['phone'],
                            ));
                } 
                $array_json=array('response'=>'1','repairshop'=>$array_json);
            }
            else{
                $array_json=array('response'=>'0');
            }
        }
        $json = json_encode($array_json);
        $this->response->body($json);
    }

    public function delete_data() 
    {
        $this->Repairshop->query('TRUNCATE repairshop;');
        $this->redirect(array('controller'=> 'repairshops', 'action'=>'index'));
    }
}
