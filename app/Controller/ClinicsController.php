<?php
header('Content-Type: text/html; charset=utf-8');
App::uses('CustomsController', 'Controller');

/**
 * Clinics Controller
 *
 * @property Clinic $Clinic
 * @property PaginatorComponent $Paginator
 */
class ClinicsController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
    public $uses = array('Clinic','Usermovement','User', 'Userip');
	public $components = array('Paginator');

        function beforeFilter(){    
            $action = $this->params['action'];
      
            if($action != "clinicListWebService")
            {
                parent::beforeFilter();
            }
            
            $this->Auth->allow('clinicListWebService');
            $user = $this->Session->read('Auth.User');
            if ($user['username']){
                $this->set('username', $user['username']);
            }
            if ($user['role']=="Super-Admin"){
                $this->set('userRole', $user['role']);
            }
            if ($user['role']=="MobileMedia-Admin"){
                $this->set('userRoleMM', $user['role']);
            }
            $result = '';
                for($i=0; $i<strlen('Clinicas'); $i++) {
                    $char = substr('Clinicas', $i, 1);
                    $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt"))-1, 1);
                    $char2 = chr(ord($char)+ord($keychar));
                    $result.=$char2;
                 }
                $encriptado=base64_encode($result);
            $theSQL=$this->Clinic->query('SELECT * FROM module WHERE moduleName="'.$encriptado.'"');
            if($theSQL){
                $this->set('module', $theSQL);
            }
        } 
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->set('clinics', $this->Clinic->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Clinic->exists($id)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Clínica es Inválida </div>');
		}
		$options = array('conditions' => array('Clinic.' . $this->Clinic->primaryKey => $id));
		$this->set('clinic', $this->Clinic->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Clinic->create();
			if ($this->Clinic->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $this->request->data['Clinic']['idClinic']=$this->Clinic->getLastInsertID();
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'add',
                                                   'table'=>'clinics',
                                                   'value'=> json_encode($this->request->data));
                                $this->Usermovement->create();
                                $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Clínica se a <a href="#" class="alert-link">Agregado con Éxito</a>. </div>');
				return $this->redirect(array('action' => 'view',$this->Clinic->getLastInsertID()));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Clínica no pudo ser Agregada, Intenta nuevamente </div>');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Clinic->exists($id)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Clínica es Inválida </div>');
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Clinic->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'edit',
                                                   'table'=>'clinics',
                                                   'value'=> json_encode($this->request->data));
                                $this->Usermovement->create();
                                $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Clínica se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
				return $this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Clínica no pudo ser Editada, Intenta nuevamente </div>');
			}
		} else {
			$options = array('conditions' => array('Clinic.' . $this->Clinic->primaryKey => $id));
			$this->request->data = $this->Clinic->find('first', $options);
            $clinic     = $this->request->data['Clinic']['state'];
            $clinicUp   = strtoupper($clinic);  // Se coloca el estado en mayusculas
            $this->set('estado', $clinicUp);
            //$this->set('estado', 'Lara');
            //debug($this->request->data['Clinic']['state']);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Clinic->id = $id;
		if (!$this->Clinic->exists()) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Clínica es Inválida </div>');
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Clinic->delete()) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'delete',
                                                   'table'=>'clinics',
                                                   'value'=> $id);
                                $this->Usermovement->create();
                                $this->Usermovement->save($array_move);
			$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Clínica se a <a href="#" class="alert-link">Eliminada con Éxito</a>. </div>');
		} else {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Clínica no pudo ser Eliminada, Intenta nuevamente </div>');
		}
		return $this->redirect(array('action' => 'index'));
	}
        
        public function clinicListWebService($state = null){
            $this->autoRender = false;
            $this->response->type('json');
            if(!$state){
                $clinics=$this->Clinic->find('all');
                if ($clinics){
                    $array_json=array();
                    foreach ($clinics as $array_clinic){
                        array_push($array_json, array('name'=>$array_clinic['Clinic']['name'],
                                                                    'address'=>$array_clinic['Clinic']['address'],
                                                                    'city'=>$array_clinic['Clinic']['city'],
                                                                    'state'=>$array_clinic['Clinic']['state'],
                                                                    'description'=>$array_clinic['Clinic']['description'],
                                                                    'phone'=>$array_clinic['Clinic']['phone'],
                                ));
                    } 
                    $array_json=array('response'=>'1','clinics'=>$array_json);
                }
                else{
                    $array_json=array('response'=>'0');
                }
            }else{
                $state=str_replace("_"," ",$state);
                $clinics=$this->Clinic->find('all', array('conditions' => array('state' => $state)));
                if ($clinics){
                    $array_json=array();
                    foreach ($clinics as $array_clinic){
                        array_push($array_json, array('name'=>$array_clinic['Clinic']['name'],
                                                                    'address'=>$array_clinic['Clinic']['address'],
                                                                    'city'=>$array_clinic['Clinic']['city'],
                                                                    'state'=>$array_clinic['Clinic']['state'],
                                                                    'description'=>$array_clinic['Clinic']['description'],
                                                                    'phone'=>$array_clinic['Clinic']['phone'],
                                ));
                    } 
                    $array_json=array('response'=>'1','clinics'=>$array_json);
                }
                else{
                    $array_json=array('response'=>'0');
                }
            }
            $json = json_encode($array_json);
            $this->response->body($json);
        }

        public function delete_data() 
        {
            $this->Clinic->query('TRUNCATE clinic;');
            $this->redirect(array('controller'=> 'clinics', 'action'=>'index'));
        }
        
    }
