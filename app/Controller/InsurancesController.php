<?php
App::uses('AppController', 'Controller');
/**
 * Insurances Controller
 *
 * @property Insurance $Insurance
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class InsurancesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Insurance->recursive = 0;
		$this->set('insurances', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Insurance->exists($id)) {
			throw new NotFoundException(__('Invalid insurance'));
		}
		$options = array('conditions' => array('Insurance.' . $this->Insurance->primaryKey => $id));
		$this->set('insurance', $this->Insurance->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Insurance->create();
			if ($this->Insurance->save($this->request->data)) {
				$this->Session->setFlash(__('The insurance has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The insurance could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Insurance->exists($id)) {
			throw new NotFoundException(__('Invalid insurance'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Insurance->save($this->request->data)) {
				$this->Session->setFlash(__('The insurance has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The insurance could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Insurance.' . $this->Insurance->primaryKey => $id));
			$this->request->data = $this->Insurance->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Insurance->id = $id;
		if (!$this->Insurance->exists()) {
			throw new NotFoundException(__('Invalid insurance'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Insurance->delete()) {
			$this->Session->setFlash(__('The insurance has been deleted.'));
		} else {
			$this->Session->setFlash(__('The insurance could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * WEBSERVICES
 **/
	
	public function getInsurance()
	{
		$insurance = $this->Insurance->find('all');

		return json_encode($insurance);
	}

}
