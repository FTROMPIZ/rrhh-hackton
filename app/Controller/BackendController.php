<?php
class BackendController extends AppController
{
	public $uses = array();	  
	
	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('landingpage');	
	}
	public function landingpage()
	{
		$this->layout = false;
	}
}
?>