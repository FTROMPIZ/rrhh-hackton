<?php
App::uses('CustomsController', 'Controller');
/**
 * Branches Controller
 *
 * @property Branch $Branch
 * @property PaginatorComponent $Paginator
 */
class BranchesController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
        public $uses = array('Branch','Usermovement','User', 'Userip');
	public $components = array('Paginator');

        function beforeFilter(){    
            $action = $this->params['action'];
      
            if($action != "branchListWebService")
            {
                parent::beforeFilter();
            }

            $this->Auth->allow('branchListWebService');
            $user = $this->Session->read('Auth.User');
            if ($user['username']){
                $this->set('username', $user['username']);
            }
            if ($user['role']=="Super-Admin"){
                $this->set('userRole', $user['role']);
            }
            if ($user['role']=="MobileMedia-Admin"){
                $this->set('userRoleMM', $user['role']);
            }
            $result = '';
                for($i=0; $i<strlen('Sucursal'); $i++) {
                    $char = substr('Sucursal', $i, 1);
                    $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt"))-1, 1);
                    $char2 = chr(ord($char)+ord($keychar));
                    $result.=$char2;
                 }
                $encriptado=base64_encode($result);
            $theSQL=$this->Branch->query('SELECT * FROM module WHERE moduleName="'.$encriptado.'"');
            if($theSQL){
                $this->set('module', $theSQL);
            }
        } 
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Branch->recursive = 0;
		$this->set('branches', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		return $this->redirect(array('action' => 'index'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Branch->create();
			if ($this->Branch->save($this->request->data)) {
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Sucursal se a <a href="#" class="alert-link">Salvado con Éxito</a>. </div>');
                                $user = $this->Session->read('Auth.User');
                                $this->request->data['Branch']['idBranch']=$this->Branch->getLastInsertID();
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'add',
                                                   'table'=>'branches',
                                                   'value'=> json_encode($this->request->data));
                                $this->Usermovement->create();
                                $this->Usermovement->save($array_move);
                                return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Sucursal no pudo ser Creada, Intenta nuevamente </div>');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Branch->exists($id)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Sucursal es Inválida </div>');
                        return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Branch->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'edit',
                                                   'table'=>'branches',
                                                   'value'=> json_encode($this->request->data));
                                $this->Usermovement->create();
                                $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Sucursal se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La sucursal no pudo ser Editada, Intenta nuevamente </div>');
			}
		} else {
			$options = array('conditions' => array('Branch.' . $this->Branch->primaryKey => $id));
			$this->request->data = $this->Branch->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Branch->id = $id;
		if (!$this->Branch->exists()) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Sucursal es Inválida </div>');
                        return $this->redirect(array('action' => 'index'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Branch->delete()) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'delete',
                                                   'table'=>'branches',
                                                   'value'=> $id);
                                $this->Usermovement->create();
                                $this->Usermovement->save($array_move);
			$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Sucursal se a <a href="#" class="alert-link">Eliminado con Éxito</a>. </div>');
		} else {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La sucursal no pudo ser Eliminada, Intenta nuevamente </div>');
		}
		return $this->redirect(array('action' => 'index'));
	}
        
        public function branchListWebService(){
            $this->autoRender = false;
            $this->response->type('json');
            $branches=$this->Branch->find('all');
            if ($branches){
                $array_json=array();
                foreach ($branches as $array_branches){
                    array_push($array_json, array('name'=>$array_branches['Branch']['name'],
                                                                'address'=>$array_branches['Branch']['address'],
                                                                'city'=>$array_branches['Branch']['city'],
                                                                'state'=>$array_branches['Branch']['state']
                            ));
                } 
                $array_json=array('branches'=>$array_json);
            }
            else{
                $array_json=array('response'=>'0');
            }
            $json = json_encode($array_json);
            $this->response->body($json);
        }
}
