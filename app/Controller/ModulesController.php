<?php
App::uses('CustomsController', 'Controller');
/**
 * Modules Controller
 *
 * @property Module $Module
 * @property PaginatorComponent $Paginator
 */
class ModulesController extends CustomsController {

/**
 * Components
 *
 * @var array
 */public $uses = array('Module','Usermovement','User', 'Userip');
	public $components = array('Paginator');
        function beforeFilter(){ 
        	parent::beforeFilter();   
            $this->Auth->allow();
            $user = $this->Session->read('Auth.User');
            if ($user['username']){
                $this->set('username', $user['username']);
            }
            if ($user['role']=="Super-Admin"){
                $this->set('userRole', $user['role']);
            }
        if ($user['role']=="MobileMedia-Admin"){
            $this->set('userRoleMM', $user['role']);
        }
        }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Module->recursive = 0;
		$this->set('modules', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		return $this->redirect(array('action' => 'index'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Module->create();
			if ($this->Module->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $this->request->data['Module']['idModule']=$this->Module->getLastInsertID();
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'add',
                                                   'table'=>'module',
                                                   'value'=> json_encode($this->request->data));
                                    $this->Usermovement->create();
                                    $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Módulo se a <a href="#" class="alert-link">Agregado con Éxito</a>. </div>');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Módulo no pudo ser Agregado, Intenta nuevamente </div>');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Module->exists($id)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Módulo es Inválida </div>');
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Module->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'edit',
                                                   'table'=>'module',
                                                   'value'=> json_encode($this->request->data));
                                    $this->Usermovement->create();
                                    $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Módulo se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Módulo no pudo ser Editado, Intenta nuevamente </div>');
			}
		} else {
			$options = array('conditions' => array('Module.' . $this->Module->primaryKey => $id));
			$this->request->data = $this->Module->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Module->id = $id;
		if (!$this->Module->exists()) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Módulo es Inválida </div>');
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Module->delete()) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'delete',
                                                   'table'=>'module',
                                                   'value'=> $id);
                                    $this->Usermovement->create();
                                    $this->Usermovement->save($array_move);
			$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Módulo se a <a href="#" class="alert-link">Eliminado con Éxito</a>. </div>');
		} else {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Módulo no pudo ser Eliminado, Intenta nuevamente </div>');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
