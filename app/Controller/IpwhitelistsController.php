<?php
App::uses('CustomsController', 'Controller');
/**
 * Appusers Controller
 *
 * @property Appuser $Appuser
 * @property PaginatorComponent $Paginator
 */
class IpwhitelistsController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
    public $uses = array('Ipwhitelist', 'Usermovement');
    public $components = array('Paginator');

    function beforeFilter(){   
        $action = $this->params['action'];
        parent::beforeFilter();
        $user = $this->Session->read('Auth.User');
  
        if ($user['username']){
            $this->set('username', $user['username']);
        }
        if ($user['role']=="Super-Admin"){
            $this->set('userRole', $user['role']);
        }
        if ($user['role']=="MobileMedia-Admin"){
            $this->set('userRoleMM', $user['role']);
        }
    } 
/**
 * index method
 *
 * @return void
 */
    public function index() {
        $this->Ipwhitelist->recursive = 0;
        $this->set('ipwhitelists', $this->Paginator->paginate());
        // $this->set('users', $this->User->find('all'));
    }

/**
 * add method
 *
 * @return void
 */
    public function add() {
        if ($this->request->is('post')) {
            $this->Ipwhitelist->create();
            if ($this->Ipwhitelist->save($this->request->data)) {
                $user = $this->Session->read('Auth.User');
                $this->request->data['Ipwhitelist']['idIpWhitelist']=$this->Ipwhitelist->getLastInsertID();
                $array_move =array('User_idUser'=>$user['idUser'],
                                   'action'=>'add',
                                   'table'=>'ipwhitelists',
                                   'value'=> json_encode($this->request->data));
                $this->Usermovement->create();
                $this->Usermovement->save($array_move);
                $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Ip se a <a href="#" class="alert-link">Agregado con Éxito</a>. </div>');
                return $this->redirect(array('action' => 'index'));
            } 
            else
            {
                $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Ip no pudo ser Agregada, Intenta nuevamente </div>');
            }
        }
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {
        $this->Ipwhitelist->id = $id;
        if (!$this->Ipwhitelist->exists($id)) {
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Ip es Inválido </div>');
            return $this->redirect(array('action' => 'index'));
        }
        else
        {
            if( $this->request->is( 'post' ) || $this->request->is( 'put' ) ){
                if ($this->Ipwhitelist->save($this->request->data)) {
                    $user = $this->Session->read('Auth.User');
                    $array_move =array('User_idUser'=>$user['idUser'],
                                       'action'=>'edit',
                                       'table'=>'ipwhitelist',
                                       'value'=> json_encode($this->request->data));
                    $this->Usermovement->create();
                    $this->Usermovement->save($array_move);
                    $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La Ip se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La Ip no pudo ser Editado, Intenta nuevamente </div>');
                }
            } else {
                $options = array('conditions' => array('Ipwhitelist.' . $this->Ipwhitelist->primaryKey => $id));
                $this->request->data = $this->Ipwhitelist->find('first', $options);
            } 
        }
    }
        

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 
    public function delete($id = null) {
        $this->Ipwhitelist->id = $id;
        if (!$this->Ipwhitelist->exists()) {
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La IP es Inválida </div>');
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Ipwhitelist->delete()) {
            $user = $this->Session->read('Auth.User');
            $array_move =array('User_idUser'=>$user['idUser'],
                               'action'=>'delete',
                               'table'=>'ipwhitelist',
                               'value'=> $id);
            $this->Usermovement->create();
            $this->Usermovement->save($array_move);
            $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>La IP se a <a href="#" class="alert-link">Eliminado con Éxito</a>. </div>');
        } else {
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>La IP no pudo ser Eliminado, Intenta nuevamente </div>');
        }
        return $this->redirect(array('action' => 'index'));
    }
}
