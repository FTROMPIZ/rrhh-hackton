<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    const USERNAME_SYSTEM = "ftrompiz";
    const PASSWORD_SYSTEM = "ABCD19998023abcd";
    
    const EVENT_TYPE_PARENT_BIRTHDATE = 1;
    const EVENT_TYPE_PARENT_SPRINT = 2;
    const EVENT_TYPE_PARENT_PRODUCT = 3;
    const EVENT_TYPE_PARENT_MY_REUNIONS = 4;
    const EVENT_TYPE_PARENT_EVENTS_MMN = 5;
    const EVENT_TYPE_PARENT_HUMAN_TALENT = 6;
    
    const PATH_COOKIE_FILE = "app/webroot/cookie.txt";
    const PATH_ICS_FILE = "app/webroot/iCalendar.ics";
    
    const URL_AUTH = "https://mobmedianet.atlassian.net/rest/auth/1/session";
    const URL_PARENT_EVENT_ID = 'https://mobmedianet.atlassian.net/wiki/rest/calendar-services/1.0/calendar/subcalendars/';
    const URL_EVENTS_OF_PARENT_ID = 'https://mobmedianet.atlassian.net/wiki/rest/calendar-services/1.0/calendar/subcalendar/privateurl/';


    var $components = array('Session','Auth' => array(
        'authenticate' => array(
            'Form' => array(
                'fields' => array('username','email')
            )
        ),
        'loginRedirect' => array(
                'controller' => 'users',
                'action' => 'index'
        ),
        'logoutRedirect' => array(
            'controller' => 'users',
            'action' => 'login'
        ),
    ));
    function beforeFilter(){ 
        $user = $this->Session->read('Auth.User');
        if ($user['username']){
            $this->set('username', $user['username']);
        }
        if ($user['role']=="Super-Admin"){
            $this->set('userRole', $user['role']);
        }
        if ($user['role']=="MobileMedia-Admin"){
            $this->set('userRoleMM', $user['role']);
        }
    }
}