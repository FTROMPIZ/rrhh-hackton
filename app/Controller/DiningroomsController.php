<?php
App::uses('CustomsController', 'Controller');
/**
 * Diningrooms Controller
 *
 * @property Diningroom $Diningroom
 * @property PaginatorComponent $Paginator
 */
class DiningroomsController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
    public $uses = array('Diningroom','Branch','Usermovement','User', 'Userip');
	public $components = array('Paginator');

    function beforeFilter(){  
        $action = $this->params['action'];
      
        if($action != "diningroomListWebService")
        {
            parent::beforeFilter();
        }

        $this->Auth->allow('diningroomListWebService');
        $user = $this->Session->read('Auth.User');
        if ($user['username']){
            $this->set('username', $user['username']);
        }
        if ($user['role']=="Super-Admin"){
            $this->set('userRole', $user['role']);
        }
        if ($user['role']=="MobileMedia-Admin"){
            $this->set('userRoleMM', $user['role']);
        }
        $result = '';
            for($i=0; $i<strlen('Menú de Comida'); $i++) 
            {
                $char = substr('Menú de Comida', $i, 1);
                $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt"))-1, 1);
                $char2 = chr(ord($char)+ord($keychar));
                $result.=$char2;
            }
        $encriptado=base64_encode($result);
        $theSQL=$this->Diningroom->query('SELECT * FROM module WHERE moduleName="'.$encriptado.'"');
        if($theSQL){
            $this->set('module', $theSQL);
        }
    } 
/**
 * index method
 *
 * @return void
 */
	public function index($branch = null,$date = null) {
        $branches = $this->Branch->find('all', array('order' => array('Branch.name' => 'asc')));
        $combo = array();
        $combo[0] =  "Seleccione una Sucursal...";
        foreach($branches as $branch2){
            $combo[$branch2['Branch']['idBranch']] = $branch2['Branch']['name'];
        }	
        $this->set('branches',$combo);
        $this->Diningroom->recursive = 0;
        if(!($branch!='0')||(!$branch))
        {
            if(!$date){
                $this->set('diningrooms', $this->Diningroom->find('all'));
            }else{
                $this->set('diningrooms', $this->Diningroom->find('all',array('conditions' => array('date'=> $date))));
            }
        }else
        {
            if(!$date){
                $this->set('diningrooms', $this->Diningroom->find('all',array('conditions' => array('Branch_idBranch'=> $branch))));
            }else{
                $this->set('diningrooms', $this->Diningroom->find('all',array('conditions' => array('Branch_idBranch'=> $branch,'date'=> $date))));
            }
        }
		$this->set('thisDiningroom', $this);
        if ($this->request->is(array('post', 'put'))) {
            if(isset($this->request->data['date'])){
                $this->request->data['Diningroom']['date']=date('Y-m-d',strtotime($this->request->data['date']));
                if($this->request->data['Diningroom']['date']=='1970-01-01'){
                    $this->request->data['Diningroom']['date']="";
                }
                if(!isset($this->request->data['Diningroom']['Branch_idBranch'])){
                    return $this->redirect(array('controller'=>'diningrooms','action' => 'index','null',$this->request->data['Diningroom']['date']));
                }
                else{
                    return $this->redirect(array('controller'=>'diningrooms','action' => 'index',$this->request->data['Diningroom']['Branch_idBranch'],$this->request->data['Diningroom']['date']));
                }
            }
            else{
                if(!isset($this->request->data['Diningroom']['Branch_idBranch'])){
                    return $this->redirect(array('controller'=>'diningrooms','action' => 'index'));
                }
                else{
                    return $this->redirect(array('controller'=>'diningrooms','action' => 'index',$this->request->data['Diningroom']['Branch_idBranch']));
                }
            }
        }
	}
        
//        public function filtro() {
//                $branches = $this->Branch->find('all', array('order' => array('Branch.name' => 'asc')));
//                $combo = array();
//                foreach($branches as $branch){
//                    $combo[$branch['Branch']['idBranch']] = $branch['Branch']['name'];
//                }	
//                $this->set('branches',$combo);
//            if ($this->request->is(array('post', 'put'))) {
//                if(isset($this->request->data['date'])){
//                        $this->request->data['Diningroom']['date']=date('Y-m-d',strtotime($this->request->data['date']));
//                    if(!isset($this->request->data['Diningroom']['Branch_idBranch'])){
//                        return $this->redirect(array('controller'=>'diningrooms','action' => 'index','null',$this->request->data['Diningroom']['date']));
//                    }
//                    else{
//                        return $this->redirect(array('controller'=>'diningrooms','action' => 'index',$this->request->data['Diningroom']['Branch_idBranch'],$this->request->data['Diningroom']['date']));
//                    }
//                }
//                else{
//                    if(!isset($this->request->data['Diningroom']['Branch_idBranch'])){
//                        $this->request->data['Diningroom']['date']=date('Y-m-d',strtotime($this->request->data['date']));
//                        return $this->redirect(array('controller'=>'diningrooms','action' => 'index'));
//                    }
//                    else{
//                        return $this->redirect(array('controller'=>'diningrooms','action' => 'index',$this->request->data['Diningroom']['Branch_idBranch']));
//                    }
//                }
//            }
//        }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Diningroom->exists($id)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Menú diario es Inválida </div>');
            return $this->redirect(array('controller'=>'diningrooms','action' => 'index'));
		}
		$this->set('thisDiningroom', $this);
		$options = array('conditions' => array('Diningroom.' . $this->Diningroom->primaryKey => $id));
		$this->set('diningroom', $this->Diningroom->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    private function thereIsAMenuOnDate($date)
    {
        $array_menu = $this->Diningroom->query(
                                                'SELECT * '
                                                . 'FROM diningroom d ' 
                                                . 'WHERE d.date like "%'.$date.'%"'
                                                );
        return (count($array_menu) > 0);
    }

	public function edit($id = null) 
    {
        $branches = $this->Branch->find('all', array('order' => array('Branch.name' => 'asc')));
        $combo = array();
        foreach($branches as $branch)
        {
            $combo[$branch['Branch']['idBranch']] = $branch['Branch']['name'];
        }	
        $this->set('branches',$combo);
        $quer=$this->Diningroom->query("SELECT DATE_FORMAT(diningroom.`date`, '%d %b %Y') as date FROM diningroom WHERE idDiningRoom=".$id);
        $this->set('dateEdit',$quer[0][0]['date']);
        if (!$this->Diningroom->exists($id)) 
        {
            $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Menú diario es Inválida </div>');
        }
        if ($this->request->is(array('post', 'put'))) 
        {
            if (!$this->thereIsAMenuOnDate(date('Y-m-d',strtotime($this->request->data['date']))))
            {
                $this->request->data['Diningroom']['idDiningRoom']=$id;
                $this->request->data['Diningroom']['date']=date('Y-m-d',strtotime($this->request->data['date']));
                if ($this->Diningroom->save($this->request->data)) 
                {
                    $user = $this->Session->read('Auth.User');
                    $array_move =array('User_idUser'=>$user['idUser'],
                                       'action'=>'edit',
                                       'table'=>'diningroom',
                                       'value'=> json_encode($this->request->data));
                    $this->Usermovement->create();
                    $this->Usermovement->save($array_move);
                    $this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Menú diario se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
                    return $this->redirect(array('action' => 'index'));
                } 
                else
                {
                    $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Menú diario no pudo ser Editado, Intenta nuevamente </div>');
                }
            }
            else
            {
                $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>Ya existe un Menú diario para la fecha seleccionada</div>');
            }
        } 
        else 
        {
            $options = array('conditions' => array('Diningroom.' . $this->Diningroom->primaryKey => $id));
            $this->request->data = $this->Diningroom->find('first', $options);
        }
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) 
    {
		$this->Diningroom->id = $id;
		if (!$this->Diningroom->exists()) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Menú diario es Inválida </div>');
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Diningroom->delete()) {
            $user = $this->Session->read('Auth.User');
            $array_move =array('User_idUser'=>$user['idUser'],
                               'action'=>'delete',
                               'table'=>'diningroom',
                               'value'=> $id);
            $this->Usermovement->create();
            $this->Usermovement->save($array_move);
			$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Menú diario se a <a href="#" class="alert-link">Eliminado con Éxito</a>. </div>');
		} else {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Menú diario no pudo ser Eliminado, Intenta nuevamente </div>');
		}
		return $this->redirect(array('action' => 'index'));
	}
        
    public function diningroomListWebService($fecha = null)
    {
        $this->autoRender = false;
        $this->response->type('json');
        $array_json=array();
       
        if(!$fecha)
        {
            $diningrooms=$this->Diningroom->find('all');
        }
        else
        {
            $diningrooms=$this->Diningroom->find('all',array('conditions' => array('DATE(Diningroom.date)'=> date('Y-m-d',strtotime($fecha)))));
            $diningrooms2=$this->Diningroom->find('all',array('conditions' => array('DATE(Diningroom.date)'=> date('Y-m-d',strtotime( '+1 day' , strtotime( $fecha ))))));
        }
        if ($diningrooms)
        {
            // $array_json=array();
            foreach ($diningrooms as $array_diningrooms)
            {
                $array_menu = array();
                if (!empty($listado_menu_type = $this->Diningroom->query('SELECT distinct(menutype.description) '
                                                                        . 'FROM `menu`,menutype '
                                                                        . 'WHERE menu.`menuType`= menutype.idMenuType '
                                                                        . 'AND `menu`.DiningRoom_idDiningRoom='.$array_diningrooms['Diningroom']['idDiningRoom'])))
                {
                    foreach ($listado_menu_type as $menus)  
                    {
                        $array_food=array();
                        foreach ($this->Diningroom->query('SELECT * '
                                                                . 'FROM `menu`,menutype '
                                                                . 'WHERE menu.`menuType`= menutype.idMenuType '
                                                                . 'AND menutype.description="'.$menus['menutype']['description'].'" '
                                                                . 'AND `menu`.DiningRoom_idDiningRoom ='.$array_diningrooms['Diningroom']['idDiningRoom'].' '
                                                                . 'GROUP BY menu.idMenu '
                                                                . 'ORDER BY menu.idMenu DESC  ') as $theMenu)
                        {
                            array_push($array_food, $theMenu['menu']['description']);
                        }
                        array_push(
                                    $array_menu, array(
                                                        'menuType'=>$menus['menutype']['description'],
                                                        'menus'=>$array_food,
                                                        
                                                    )
                        );
                    }
                }
                $query=$this->Diningroom->query('SELECT * FROM branch WHERE idBranch='.$array_diningrooms['Diningroom']['Branch_idBranch']);
                array_push($array_json, array('date'=>date('Y-m-d',strtotime($array_diningrooms['Diningroom']['date'])),
                                              'branch'=>$query[0]['branch']['name'],
                                              'imgPath'=> $array_diningrooms['Diningroom']['imgPath'],
                                              'menu'=>$array_menu,
                ));
            }
            if ($diningrooms2)
            {
                array_push($array_json, array('openmenu'=>true));
            }
            else
            {
                array_push($array_json, array('openmenu'=>false));
            }

            $array_json=array('diningroom'=>$array_json);
        }
        else
        {
            // $array_json=array('response'=>'0');
            if ($diningrooms2)
            {
                array_push($array_json, array('response'=>'0', 'openmenu'=>true));
            }
            else
            {   
                array_push($array_json, array('response'=>'0', 'openmenu'=>false));
            }
        }
        $json = json_encode($array_json);
        $this->response->body($json);
    }
}
