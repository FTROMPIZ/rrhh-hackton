<?php 

App::uses('CakeEmail', 'Network/Email');

class ApiController extends AppController
{
	public $uses = array(
							'User',
							'Person',
						);	  
	
	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('sendEmailVacations');
	
	}

	public function sendEmailVacations ($nombre, $date, $date_start, $date_end)
	{/*
		try 
		{*/
			$Email = new CakeEmail();
			$Email->config('default');
			$Email->emailFormat('html','text');	
			$Email->template('formato_email_vacations',null);
							
			$Email->viewVars(
				array(
					'nombre' => $nombre,
					//'date' => $date,
					//'date_start' => $date_start,
					//'date_end' => $date_end,
					)
				);
			
			$Email->to('evelyn.deleon@gmail.com');
			$Email->subject('Solicitud de vacaciones');;
			$Email->attachments('/var/sentora/hostdata/admin/public_html/equipo3_mobilemediacms_com/rrhh-hackton/app/webroot/files/'.$nombre.'-solicitudVacaciones.xlsx');

			$Email->send('My message');
	/*	}
		catch (Exception $e)	
		{
			
		}*/
	}

}
?>
