<?php
App::uses('CustomsController', 'Controller');
/**
 * Menus Controller
 *
 * @property Menu $Menu
 * @property PaginatorComponent $Paginator
 */
class MenusController extends CustomsController {

/**
 * Components
 *
 * @var array
 */
        public $uses = array('Menu','Menutype','Usermovement','User', 'Userip');
	public $components = array('Paginator');

        function beforeFilter(){
            $action = $this->params['action'];
      
            if($action != "loginWebService")
            {
                parent::beforeFilter();
            }  
            $this->Auth->allow('loginWebService');
            $user = $this->Session->read('Auth.User');
            if ($user['username']){
                $this->set('username', $user['username']);
            }
            if ($user['role']=="Super-Admin"){
                $this->set('userRole', $user['role']);
            }
            if ($user['role']=="MobileMedia-Admin"){
                $this->set('userRoleMM', $user['role']);
            }
            $result = '';
                for($i=0; $i<strlen('Menú de Comida'); $i++) {
                    $char = substr('Menú de Comida', $i, 1);
                    $keychar = substr("MoBiLeMediaNeTENCRipt", ($i % strlen("MoBiLeMediaNeTENCRipt"))-1, 1);
                    $char2 = chr(ord($char)+ord($keychar));
                    $result.=$char2;
                 }
                $encriptado=base64_encode($result);
            $theSQL=$this->Menu->query('SELECT * FROM module WHERE moduleName="'.$encriptado.'"');
            if($theSQL){
                $this->set('module', $theSQL);
            }
        } 
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Menu->recursive = 0;
		$this->set('menus', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		return $this->redirect(array('controller'=>'diningrooms','action' => 'index'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($idDiningroom = null) {
                $menutypes = $this->Menutype->find('all', array('order' => array('Menutype.description' => 'asc')));
                $combo = array();
                foreach($menutypes as $menutype){
                    $combo[$menutype['Menutype']['idMenuType']] = $menutype['Menutype']['description'];
                }	
                $this->set('menutype',$combo);
                $this->set('idDiningroom',$idDiningroom);
                if ($idDiningroom){
                    $varQuer=$this->Menu->query('SELECT DATE_FORMAT(date, "%d/%m/%Y") as date,Branch_idBranch FROM diningroom WHERE idDiningRoom='.$idDiningroom);
                    $varQuer2=$this->Menu->query('SELECT name FROM branch WHERE idBranch='.$varQuer[0]['diningroom']['Branch_idBranch']);
                    $this->set('branch',$varQuer2[0]['branch']['name']);
                    $this->set('thisMenu',$varQuer[0][0]['date']);
                }
		if ($this->request->is('post')) {
                    if($idDiningroom){
			$this->Menu->create();
                        $this->request->data['Menu']['DiningRoom_idDiningRoom']=$idDiningroom;
			if ($this->Menu->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $this->request->data['Menu']['idMenu']=$this->Menu->getLastInsertID();
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'add',
                                                   'table'=>'menu',
                                                   'value'=> json_encode($this->request->data));
                                    $this->Usermovement->create();
                                    $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Menú se a <a href="#" class="alert-link">Salvado con Éxito</a>. </div>');
				return $this->redirect(array('controller'=>'menus','action' => 'add',$idDiningroom));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Menú no pudo ser Creado, Intenta nuevamente </div>');
			}
                    }else{
                        $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Menú no pudo ser Creado, Intenta nuevamente </div>');
                    }
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null,$idDiningroom = null) {
                $menutypes = $this->Menutype->find('all', array('order' => array('Menutype.description' => 'asc')));
                $combo = array();
                foreach($menutypes as $menutype){
                    $combo[$menutype['Menutype']['idMenuType']] = $menutype['Menutype']['description'];
                }	
                $this->set('menutype',$combo);
		if (!$this->Menu->exists($id)) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Menú es Inválida </div>');
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Menu->save($this->request->data)) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'edit',
                                                   'table'=>'menu',
                                                   'value'=> json_encode($this->request->data));
                                    $this->Usermovement->create();
                                    $this->Usermovement->save($array_move);
				$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Menú se a <a href="#" class="alert-link">Editado con Éxito</a>. </div>');
				return $this->redirect(array('controller'=>'diningrooms','action' => 'view',$idDiningroom));
			} else {
				$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Menú no pudo ser Editado, Intenta nuevamente </div>');
			}
		} else {
			$options = array('conditions' => array('Menu.' . $this->Menu->primaryKey => $id));
			$this->request->data = $this->Menu->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null,$idDiningroom = null) {
		$this->Menu->id = $id;
		if (!$this->Menu->exists()) {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Menú es Inválida </div>');
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Menu->delete()) {
                                $user = $this->Session->read('Auth.User');
                                $array_move =array('User_idUser'=>$user['idUser'],
                                                   'action'=>'delete',
                                                   'table'=>'menu',
                                                   'value'=> $id);
                                    $this->Usermovement->create();
                                    $this->Usermovement->save($array_move);
			$this->Session->setFlash('<div class="alert alert-success"> <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Exito! </strong>El Menú se a <a href="#" class="alert-link">Eliminado con Éxito</a>. </div>');
                        return $this->redirect(array('controller'=>'diningrooms','action' => 'view',$idDiningroom));
		} else {
			$this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>El Menú no pudo ser Eliminado, Intenta nuevamente </div>');
		}
	}
}
