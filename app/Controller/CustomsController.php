<?php
  App::uses('AppController', 'Controller');
  App::import('Controller', 'App');

  /**
  * Customs Controller
  *
  * @property Custom $Customs
  * @property PaginatorComponent $Paginator
  */
  class CustomsController extends AppController {

  /**
  * Components
  *
  * @var array
  */
    public $components = array('RequestHandler');
    public $uses = array('User', 'Userip');
    
    function beforeFilter(){
      parent::beforeFilter();
      
      $response_connected = $this->userValidation();
      if ($response_connected == false)
      {
        $this->redirect(array('controller'=>'users','action' => 'logout'));
      }
    }

    public function userValidation ()
    {
      $user = $this->Session->read('Auth.User');
      $this->loadModel("Userip");
      $user_connected = $this->Userip->findAllByuserIduser($user['idUser']);
      $actual_ip = $this->verIP();
      if ($user_connected)
      {
        // Verifica si estoy accediendo desde la misma máquina
        if($user_connected[0]['Userip']['session_ip'] != $actual_ip)
        {
          $this->Session->setFlash('<div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>Se ha iniciado sesión en otra computadora</div>');
          return false;
        }
      }
      return true;
    }

    public function verIP(){   
      if(!empty($_SERVER['HTTP_CLIENT_IP']))
      {
          $ip = $_SERVER['HTTP_CLIENT_IP'];
      }else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
      {
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      }else
      {
          $ip = $_SERVER['REMOTE_ADDR'];
      }
      return $ip;
    }
  }
?>