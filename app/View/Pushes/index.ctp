<div class="vd_head-section clearfix">
    <div class="vd_panel-header">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>">Home</a> </li>
            <li class="active">Push</li>
        </ul>
        <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
            <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
            <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
            <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>

        </div>

    </div>
</div>

<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1>Lista de Push</h1>

    </div>
</div>
<?php if(isset($module)){ ?> 
  <div class="vd_content-section clearfix">
      <div class="row">
        <div class="col-md-12">
          <div class="panel widget">
            <div class="panel-heading vd_bg-grey">
              <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>  </h3>
            </div>
            <div class="panel-body table-responsive">
              <table class="table table-striped" id="data-tables">
                <thead>
                  <tr>
                    <th>Texto</th>
                    <th>Fecha</th>
                    <th>Data</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($pushes as $push): ?>
                    <tr>
                      <td><?php echo $push['Push']['text']; ?>&nbsp;</td>
                      <td style="width: 95px;"><span style='display: none;'><?php echo date('Y/m/d H:i:s',strtotime($push['Push']['date'])); ?></span><?php echo date('d-m-Y g:i a',strtotime($push['Push']['date'])); ?></td>
                      <td><?php echo $push['Push']['json']; ?>&nbsp;</td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
             </table>
            </div>
          </div> 
        </div>
      </div>
      
      <a href="<?php echo $this->Html->url(array('controller' => 'pushes', 'action' => 'add')) ?>" class="btn btn-success start"> <i class="glyphicon glyphicon-plus"></i> <span>Enviar Push</span> </a>
  </div>
<?php }else{?>
  <div class="alert alert-danger">
    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>No tienes acceso a esta locación 
  </div>
<?php } ?>
