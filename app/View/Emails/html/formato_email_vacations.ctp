<!DOCTYPE>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=320, target-densitydpi=device-dpi">
</head>
<body>
	<p>Solicitud de vacaciones</p>
	<br />
	<p>El empleado <?php echo $nombre; ?> ha solicitado vacaciones.</p>
	<p>La información asociada a dicha solicitud se encuentra en el archivo adjunto.</p>
	<br />
	<br />
	<p>Mensaje envíado automáticamente por el sistema.</p>
	<a href="http://www.mobilemedia.net"><img src="<?php echo $this->webroot; ?>img/mm.png" width="150" ></a>
</body>