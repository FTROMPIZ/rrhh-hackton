<div class="menus view">
<h2><?php echo __('Menu'); ?></h2>
	<dl>
		<dt><?php echo __('IdMenu'); ?></dt>
		<dd>
			<?php echo $menu['Menu']['idMenu']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo $menu['Menu']['type']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo $menu['Menu']['description']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('DiningRoom IdDiningRoom'); ?></dt>
		<dd>
			<?php echo $menu['Menu']['DiningRoom_idDiningRoom']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Menu'), array('action' => 'edit', $menu['Menu']['idMenu'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Menu'), array('action' => 'delete', $menu['Menu']['idMenu']), array(), __('Are you sure you want to delete # %s?', $menu['Menu']['idMenu'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Menus'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Menu'), array('action' => 'add')); ?> </li>
	</ul>
</div>
