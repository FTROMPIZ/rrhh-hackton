<!-- app/View/Users/add.ctp -->
<div class="vd_head-section clearfix">
    <div class="vd_panel-header">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>">Home</a> </li>
            <li><a href="<?php echo $this->Html->url(array('controller' => 'diningrooms', 'action' => 'index')) ?>">Menú Diario</a> </li>
            <li class="active">Crear Comida</li>
        </ul>
        <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
            <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
            <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
            <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>

        </div>

    </div>
</div>

<div class="questionaires view">
<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1>Crear Comida para el menú: <?php echo $thisMenu;?> - <?php echo $branch;?></h1>

    </div>
</div>
 <?php if(isset($module)){ ?>   
              <div class="vd_content-section clearfix">
<div class="related">
<div class="users form">
<?php echo $this->Form->create('Menu'); ?>
    <fieldset>
        <?php 
		echo $this->Form->input('description',array('label'=>'Descripción'));
		echo $this->Form->input('menuType',array('label'=>'Tipo de Menú','options' => $menutype));
    ?>
    </fieldset><br>
<?php echo $this->Form->input("Agregar",array("label" => false,'div'=>'false', "class" => "btn btn-success start", "type" => "submit","style"=>"float:left;"));
      echo "<a href='". $this->Html->url(array('controller' => 'diningrooms', 'action' => 'view',$idDiningroom))."' class='btn btn-warning start' style='margin-left:5px;padding-left:5px;'> <span>Volver al Menú de comida</span> </a>";
	echo $this->Form->end(); ?>
</div>
          </div>
          
        </div>
    <?php }else{?>
    <div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>No tienes acceso a esta locación </div>
    <?php } ?>
