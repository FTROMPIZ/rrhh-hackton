<div class="vd_head-section clearfix">
    <div class="vd_panel-header">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>">Home</a> </li>
            <li><a href="<?php echo $this->Html->url(array('controller' => 'news', 'action' => 'index')) ?>">Noticias</a> </li>
            <li class="active">Ver Noticia</li>
        </ul>
        <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
            <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
            <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
            <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>

        </div>

    </div>
</div>

<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1>Noticia: <?php echo $news['News']['name']; ?></h1>

    </div>
</div>

<div class="clinics view">
    <div class="vd_content-section clearfix">
<div class="related">
    <div class="row" style="background: white">
        <table class="table table-bordered table-striped" style="margin-bottom: 0px;">
                        <tbody>
                          <tr>
                            <th>Nombre:</th>
                            <td><?php echo $news['News']['name']; ?></td>
                          </tr>
                          <tr>
                            <th>Descripción:</th>
                            <td><?php echo $news['News']['description']; ?></td>
                          </tr>
                          <tr>
                            <th>Fecha:</th>
                            <td><?php echo date('d-m-Y g:i a',strtotime($news['News']['date'])); ?></td>
                          </tr>
                          <tr>
                            <th>Imagen:</th>
                            <td><?php if($news['News']['imgPath']!='0'){ echo '<img src="'.$news['News']['imgPath'].'">'; }else{echo 'No posee imagen asociada';} ?></td>
                          </tr>
                        </tbody>
    </table>
          
          </div>
        </div>
    <a href="<?php echo $this->Html->url(array('controller' => 'news', 'action' => 'edit',$news['News']['idNews'])) ?>" class="btn btn-success start"> <i class="glyphicon glyphicon-upload"></i> <span>Editar Noticia</span> </a>
    <a href="<?php echo $this->Html->url(array('controller' => 'fileuploads', 'action' => 'editNews',$news['News']['idNews'])) ?>" class="btn btn-warning start"> <i class="glyphicon glyphicon-picture"></i> <span>Editar Imagen</span> </a>
    <form action="<?php echo $this->Html->url(array('controller' => 'news', 'action' => 'delete', $news['News']['idNews'])) ?>" name="post_delete" id="post_delete" style="display:none;" method="post">
        <input type="hidden" name="_method" value="POST">
    </form>
    <a href="#" onclick="if (confirm(&quot;¿Estás seguro que deseas borrar la clínica: <?php echo $news['News']['name']; ?> ?&quot;)) { document.post_delete.submit(); } event.returnValue = false; return false;" class="btn btn-danger start"><i class="glyphicon glyphicon-trash"></i>Borrar</a>