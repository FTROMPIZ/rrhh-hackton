<?php
  $u_agent = $_SERVER['HTTP_USER_AGENT']; 
  // Next get the name of the useragent yes seperately and for good reason

  if(!preg_match('/Chrome/i',$u_agent)) 
  { 
      header("Location: ".$this->webroot."backend/landingpage");
      exit;
  }

  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
?>

<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	

<html><!--<![endif]-->

    <!-- Specific Page Data -->

    <!-- End of Data -->
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>RRHH Banesco</title>
      <meta name="keywords" content="HTML5 Template, CSS3, All Purpose Admin Template, " />
      <meta name="description" content="Responsive Admin Template for multipurpose use">
      <meta name="author" content="Venmond">

      <!-- Set the viewport width to device width for mobile -->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">

      <!-- Fav and touch icons -->
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/pple-touch-icon-144-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
      <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
      <!--<link rel="shortcut icon" href="img/ico/favicon.png">-->
      <?php echo $this->Html->meta('img/favicon.ico','img/favicon.ico',array('type' => 'icon')); ?>
      <!-- CSS -->

      <!-- Custom CSS -->
      <link rel="stylesheet" type="text/css" href="../custom/custom.css" />
      
      <!-- Bootstrap & FontAwesome & Entypo CSS -->
      <?php echo $this->Html->css('bootstrap.min.css'); ?>
      <?php echo $this->Html->css('font-awesome.min.css'); ?>
      <!--[if IE 7]><link type="text/css" rel="stylesheet" href="css/font-awesome-ie7.min.css"><![endif]-->
      <?php echo $this->Html->css('font-entypo.css'); ?> 

      <!-- Fonts CSS -->
      <?php echo $this->Html->css('fonts.css'); ?>

      <?php echo $this->Html->css('stylesheets.css'); ?>

      <!-- Plugin CSS -->
      
      <?php echo $this->Html->css('../plugins/jquery-ui/jquery-ui.custom.min.css'); ?>
      <?php echo $this->Html->css('../plugins/prettyPhoto-plugin/css/prettyPhoto.css'); ?>
      <?php echo $this->Html->css('../plugins/isotope/css/isotope.css'); ?>
      <?php echo $this->Html->css('../plugins/pnotify/css/jquery.pnotify.css'); ?>   
      <?php echo $this->Html->css('../plugins/google-code-prettify/prettify.css'); ?>
      
      <?php echo $this->Html->css('../plugins/mCustomScrollbar/jquery.mCustomScrollbar.css'); ?>
      <?php echo $this->Html->css('../plugins/tagsInput/jquery.tagsinput.css'); ?>
      <?php echo $this->Html->css('../plugins/bootstrap-switch/bootstrap-switch.css'); ?>
      <?php echo $this->Html->css('../plugins/daterangepicker/daterangepicker-bs3.css'); ?>
      <?php echo $this->Html->css('../plugins/bootstrap-timepicker/bootstrap-timepicker.min.css'); ?>
      <?php echo $this->Html->css('../plugins/colorpicker/css/colorpicker.css'); ?>       
      
      <?php echo $this->Html->css('../plugins/jquery-file-upload/css/jquery.fileupload.css'); ?>     
      <?php echo $this->Html->css('../plugins/jquery-file-upload/css/jquery.fileupload-ui.css'); ?>     
      <?php echo $this->Html->css('../plugins/bootstrap-wysiwyg/css/bootstrap-wysihtml5-0.0.2.css'); ?>        
   
      <!-- Specific CSS -->
      <?php echo $this->Html->css('../plugins/fullcalendar/fullcalendar.css'); ?>
      <?php echo $this->Html->css('../plugins/fullcalendar/fullcalendar.print.css'); ?>
      <?php echo $this->Html->css('../plugins/introjs/css/introjs.min.css'); ?>
      <?php echo $this->Html->css("../plugins/dataTables/css/jquery.dataTables..css"); ?>
      <?php echo $this->Html->css("../plugins/dataTables/css/dataTables.bootstrap.css"); ?> 

      <!-- Theme CSS -->
      <?php echo $this->Html->css('theme.min.css'); ?>
      <!--[if IE]> <link href="css/ie.css" rel="stylesheet" > <![endif]-->
      <?php echo $this->Html->css('chrome.css'); ?><!-- chrome only css -->    

      <!-- Responsive CSS -->
      <?php echo $this->Html->css('theme-responsive.min.css'); ?>

      <!-- Head SCRIPTS -->
      <?php echo $this->Html->script('modernizr.js'); ?>
      <?php echo $this->Html->script('mobile-detect.min.js'); ?>
      <?php echo $this->Html->script('mobile-detect-modernizr.js'); ?>

      <!-- Javascript =============================================== --> 
      <!-- Placed at the end of the document so the pages load faster --> 
      <?php echo $this->Html->script('jquery.js'); ?> 
      
      <!--[if lt IE 9]>
        <script type="text/javascript" src="js/excanvas.js"></script>      
      <![endif]--> 
      <?php echo $this->Html->script('bootstrap.min.js'); ?>
      <?php echo $this->Html->script('../plugins/jquery-ui/jquery-ui.custom.min.js'); ?>
      <?php echo $this->Html->script('../plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js'); ?>

      <?php echo $this->Html->script('caroufredsel.js'); ?>
      <?php echo $this->Html->script('plugins.js'); ?>

      <?php echo $this->Html->script('../plugins/breakpoints/breakpoints.js'); ?>
      <?php echo $this->Html->script('../plugins/dataTables/jquery.dataTables.min.js'); ?>
      <?php echo $this->Html->script('../plugins/dataTables/dataTables.bootstrap.js'); ?>
      <?php echo $this->Html->script('../plugins/prettyPhoto-plugin/js/jquery.prettyPhoto.js'); ?>

      <?php echo $this->Html->script('../plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js'); ?>
      <?php echo $this->Html->script('../plugins/tagsInput/jquery.tagsinput.min.js'); ?>
      <?php echo $this->Html->script('../plugins/bootstrap-switch/bootstrap-switch.min.js'); ?>
      <?php echo $this->Html->script('../plugins/blockUI/jquery.blockUI.js'); ?>
      <?php echo $this->Html->script('../plugins/pnotify/js/jquery.pnotify.min.js'); ?>

      <?php echo $this->Html->script('theme.js'); ?>
      <?php echo $this->Html->script('../custom/custom.js'); ?>

      <!-- Specific Page Scripts Put Here -->
      <!-- Flot Chart  -->
      <?php echo $this->Html->script('../plugins/flot/jquery.flot.min.js'); ?>
      <?php echo $this->Html->script('../plugins/flot/jquery.flot.resize.min.js'); ?>
      <?php echo $this->Html->script('../plugins/flot/jquery.flot.pie.min.js'); ?>
      <?php echo $this->Html->script('../plugins/flot/jquery.flot.categories.min.js'); ?>
      <?php echo $this->Html->script('../plugins/flot/jquery.flot.time.min.js'); ?>
      <?php echo $this->Html->script('../plugins/flot/jquery.flot.animator.min.js'); ?>

      <!-- Vector Map -->
      <?php echo $this->Html->script('../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>
      <?php echo $this->Html->script('../plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?>

      <!-- Calendar -->
      <?php echo $this->Html->script('../plugins/moment/moment.min.js'); ?>
      <?php echo $this->Html->script('../plugins/jquery-ui/jquery-ui.custom.min.js'); ?>
      <?php echo $this->Html->script('../plugins/fullcalendar/fullcalendar.min.js'); ?>

      <!-- Intro JS (Tour) -->
      <?php echo $this->Html->script('../plugins/introjs/js/intro.min.js'); ?>

      <!-- Sky Icons -->
      <?php echo $this->Html->script('../plugins/skycons/skycons.js'); ?>
      <?php echo $this->Html->script('../plugins/jquery-ui/jquery-ui.custom.min.js'); ?>
      
      
      <?php echo $this->Html->script('../plugins/tagsInput/jquery.tagsinput.min.js'); ?>
      <?php echo $this->Html->script('../plugins/bootstrap-timepicker/bootstrap-timepicker.min.js'); ?>
      <?php echo $this->Html->script('../plugins/daterangepicker/moment.min.js'); ?>
      <?php echo $this->Html->script('../plugins/daterangepicker/daterangepicker.js'); ?>

      <?php echo $this->Html->script('../plugins/colorpicker/colorpicker.js'); ?>
      <?php echo $this->Html->script('../plugins/ckeditor/ckeditor.js'); ?>
      <?php echo $this->Html->script('../plugins/ckeditor/adapters/jquery.js'); ?>

      <?php echo $this->Html->script('../plugins/bootstrap-wysiwyg/js/wysihtml5-0.3.0.min.js'); ?>
      <?php echo $this->Html->script('../plugins/bootstrap-wysiwyg/js/bootstrap-wysihtml5-0.0.2.js'); ?>

      <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
      <?php echo $this->Html->script('../http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js') ?>
      <!-- The Canvas to Blob plugin is included for image resizing functionality -->
      <?php echo $this->Html->script('../http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js') ?>
      <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
      <?php echo $this->Html->script('../plugins/jquery-file-upload/js/jquery.iframe-transport.js') ?>
      <!-- The basic File Upload plugin -->
      <?php echo $this->Html->script('../plugins/jquery-file-upload/js/jquery.fileupload.js') ?>
      <!-- The File Upload processing plugin -->
      <?php echo $this->Html->script('../plugins/jquery-file-upload/js/jquery.fileupload-process.js') ?>
      <!-- The File Upload image preview & resize plugin -->
      <?php echo $this->Html->script('../plugins/jquery-file-upload/js/jquery.fileupload-image.js') ?>
      <!-- The File Upload audio preview plugin -->
      <?php echo $this->Html->script('../plugins/jquery-file-upload/js/jquery.fileupload-audio.js') ?>
      <!-- The File Upload video preview plugin -->
      <?php echo $this->Html->script('../plugins/jquery-file-upload/js/jquery.fileupload-video.js') ?>
      <!-- The File Upload validation plugin -->
      <?php echo $this->Html->script('../plugins/jquery-file-upload/js/jquery.fileupload-validate.js') ?>

      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
        <script type="text/javascript" src="js/html5shiv.js"></script>
        <script type="text/javascript" src="js/respond.min.js"></script>     
      <![endif]-->
  </head> 

<?php if(isset ($username)){ ?>
    <body id="dashboard" class="full-layout  nav-right-hide nav-right-start-hide       responsive    clearfix" data-active="dashboard "  data-smooth-scrolling="1"> 
      <div class="vd_body">
          <!-- Header Start -->
          <header class="header-1" id="header">
              <div class="vd_top-menu-wrapper">
                  <div class="container ">
                      <div class="vd_top-nav vd_nav-width  ">
                          <div class="vd_panel-header">
                              <div class="logo">
                                  <a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>"><?php echo $this->Html->image('logo.png'); ?></a>
                              </div>
                              <!-- logo -->
                              <div class="vd_panel-menu  hidden-sm hidden-xs" data-intro="<strong>Minimize Left Navigation</strong><br/>Toggle navigation size to medium or small size. You can set both button or one button only. See full option at documentation." data-step=1>
                                  <span class="nav-medium-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Medium Nav Toggle" data-action="nav-left-medium">
                                      <i class="fa fa-bars"></i>
                                  </span>

                                  <span class="nav-small-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Small Nav Toggle" data-action="nav-left-small">
                                      <i class="fa fa-ellipsis-v"></i>
                                  </span> 

                              </div>
                              <div class="vd_panel-menu left-pos visible-sm visible-xs">
                                  <span class="menu" data-action="toggle-navbar-left">
                                      <i class="fa fa-ellipsis-v"></i>
                                  </span>  
                              </div>
                              <div class="vd_panel-menu visible-sm visible-xs">
                                  <span class="menu visible-xs" data-action="submenu">
                                      <i class="fa fa-bars"></i>
                                  </span>        

                                  <span class="menu visible-sm visible-xs" data-action="toggle-navbar-right">
                                      <i class="fa fa-comments"></i>
                                  </span>                   

                              </div>                                     
                              <!-- vd_panel-menu -->
                          </div>
                          <!-- vd_panel-header -->

                      </div>    
                      <div class="vd_container">
                          <div class="row">
                              <div class="col-sm-5 col-xs-12"></div>
                              <div class="col-sm-7 col-xs-12">
                                  <div class="vd_mega-menu-wrapper">
                                      <div class="vd_mega-menu pull-right">
                                          <ul class="mega-ul">
                                              <li id="top-menu-profile" class="profile mega-li"> 
                                                  <a href="#" class="mega-link"  data-action="click-trigger"> 
                                                      <span  class="mega-image">
                                                          <?php echo $this->Html->image('avatar/avatar.jpg'); ?>              
                                                      </span>
                                                      <span class="mega-name">
                                                          <?php echo $username ?> <i class="fa fa-caret-down fa-fw"></i> 
                                                      </span>
                                                  </a> 
                                                  <div class="vd_mega-menu-content  width-xs-2  left-xs left-sm" data-action="click-target">
                                                      <div class="child-menu"> 
                                                          <div class="content-list content-menu">
                                                              <ul class="list-wrapper pd-lr-10">
                                                                  <li> <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'logout')) ?>"> <div class="menu-icon"><i class=" fa fa-sign-out"></i></div>  <div class="menu-text">Salir</div> </a> </li>             
                                                              </ul>
                                                          </div> 
                                                      </div> 
                                                  </div>     
                                              </li>               
                                          </ul>
                                          <!-- Head menu search form ends -->                         
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- container --> 
              </div>
              <!-- vd_primary-menu-wrapper --> 
          </header>
          <!-- Header Ends --> 
          <div class="content">
              <div class="container">
                  <div class="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_navbar-left  ">
                      <div class="navbar-menu clearfix">
                          <div class="vd_menu">
                              <ul>
                                  <!--<li>
                                      <a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index'));?>">
                                          <span class="menu-icon"><i class="fa fa-dashboard"></i></span> 
                                          <span class="menu-text">Tablero</span>  
                                      </a>
                                  </li>--> 
                                  
                                  <li>
                                      <a href="<?php echo $this->Html->url(array('controller' => 'diningrooms', 'action' => 'index')) ?>">
                                          <span class="menu-icon"><i class="fa fa-cutlery"></i></span> 
                                          <span class="menu-text">Comedor</span>  
                                      </a>
                                  </li>  
                                  <li>
                                      <a href="<?php echo $this->Html->url(array('controller' => 'events', 'action' => 'index')) ?>">
                                          <span class="menu-icon"><i class="fa fa-calendar"> </i></span> 
                                          <span class="menu-text">Eventos</span>  
                                      </a>
                                  </li> 
                                  <li>
                                      <a href="<?php echo $this->Html->url(array('controller' => 'news', 'action' => 'index')) ?>">
                                          <span class="menu-icon"><i class="fa fa-bullhorn"> </i></span> 
                                          <span class="menu-text">Noticias</span>  
                                      </a>
                                  </li>
                                  <li>
                                      <a href="<?php echo $this->Html->url(array('controller' => 'pushes', 'action' => 'index')) ?>">
                                          <span class="menu-icon"><i class="fa fa-caret-square-o-up"> </i></span> 
                                          <span class="menu-text">Push</span>  
                                      </a>
                                  </li> 
                                  <li>
                                      <a href="javascript:void(0);" data-action="click-trigger">
                                              <span class="menu-icon"><i class="fa fa-automobile"></i></span> 
                                          <span class="menu-text">Talleres</span>  
                                          <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                                      </a>
                                      <div class="child-menu"  data-action="click-target">
                                          <ul>
                                              <li>
                                                  <a href="<?php echo $this->Html->url(array('controller' => 'repairshops', 'action' => 'index')) ?>">
                                                      <span class="menu-text">Listado de Talleres</span>  
                                                  </a>
                                              </li>              
                                              <li>
                                                  <a href="<?php echo $this->Html->url(array('controller' => 'fileuploads', 'action' => 'bulkloaderRepairshop')) ?>">
                                                      <span class="menu-text">Carga Masiva de Talleres</span>  
                                                  </a>
                                              </li>                                                                                                  
                                          </ul>   
                                      </div>
                                  </li>
                                  <li>
                                      <a href="javascript:void(0);" data-action="click-trigger">
                                              <span class="menu-icon"><i class="fa fa-file"></i></span> 
                                          <span class="menu-text">Clínicas</span>  
                                          <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                                      </a>
                                      <div class="child-menu"  data-action="click-target">
                                          <ul>
                                              <li>
                                                  <a href="<?php echo $this->Html->url(array('controller' => 'clinics', 'action' => 'index')) ?>">
                                                      <span class="menu-text">Listado de Clínicas</span>  
                                                  </a>
                                              </li>              
                                              <li>
                                                  <a href="<?php echo $this->Html->url(array('controller' => 'fileuploads', 'action' => 'bulkloaderClinics')) ?>">
                                                      <span class="menu-text">Carga Masiva de Clínicas</span>  
                                                  </a>
                                              </li>                                                                                                  
                                          </ul>   
                                      </div>
                                  </li> 
                                  <!--<li>
                                      <a href="<?php echo $this->Html->url(array('controller' => 'appusers', 'action' => 'index')) ?>">
                                          <span class="menu-icon"><i class="fa fa-user"> </i></span> 
                                          <span class="menu-text">Usuarios de Aplicación</span>  
                                      </a>
                                  </li>-->
                                  <?php if(isset($userRole)){ ?>
                                  <li>
                                      <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'index')) ?>">
                                          <span class="menu-icon"><i class="fa fa-user"> </i></span> 
                                          <span class="menu-text">Usuarios</span>  
                                      </a>
                                  </li> 
                                  <li>
                                      <a href="<?php echo $this->Html->url(array('controller' => 'usermovements', 'action' => 'index')) ?>">
                                          <span class="menu-icon"><i class="fa fa-server"> </i></span> 
                                          <span class="menu-text">Movimientos de Usuario</span>  
                                      </a>
                                  </li> 
                                  <?php }?>
                                  <?php if(isset($userRoleMM)){ ?>
                                  <li>
                                      <a href="<?php echo $this->Html->url(array('controller' => 'modules', 'action' => 'index')) ?>">
                                          <span class="menu-icon"><i class="fa fa-puzzle-piece"> </i></span> 
                                          <span class="menu-text">Módulos</span>  
                                      </a>
                                  </li> 
                                  <?php }?>
                                  <li>
                                      <a href="<?php echo $this->Html->url(array('controller' => 'branches', 'action' => 'index')) ?>">
                                          <span class="menu-icon"><i class="fa fa-sitemap"></i></span> 
                                          <span class="menu-text">Sucursales</span>  
                                      </a>
                                      
                                  </li>

                                  <?php if(isset($userRole)){ ?>
                                  <li>
                                      <a href="javascript:void(0);" data-action="click-trigger">
                                          <span class="menu-icon"><i class="fa fa-cogs"></i></span> 
                                          <span class="menu-text">Propiedades</span>  
                                          <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                                      </a>
                                      <div class="child-menu"  data-action="click-target">
                                          <ul>
                                              <li>
                                                  <a href="<?php echo $this->Html->url(array('controller' => 'ipwhitelists', 'action' => 'index')) ?>">
                                                      <span class="menu-text">IPs Permitidas</span>  
                                                  </a>
                                              </li>              
                                              <li>
                                                  <a href="<?php echo $this->Html->url(array('controller' => 'openhours', 'action' => 'index')) ?>">
                                                      <span class="menu-text">Horarios Permitidos</span>  
                                                  </a>
                                              </li>                                                                                                  
                                          </ul>   
                                      </div>
                                  </li>
                                  <?php }?>
                              </ul>
                              <br>
                              <br>
                              <br>
                              <br>
                              <!-- Head menu search form ends -->
                          </div>             
                      </div>
                      <div class="navbar-spacing clearfix">
                      </div> 
                  </div>      
                  <!-- Middle Content Start -->

                  <div class="vd_content-wrapper">
                      <div class="vd_container">
                          <div class="vd_content clearfix">
                              <!-- vd_head-section -->
                              <!-- titulo del top -->
                              <div id="content">
                                      <?php echo $this->Session->flash(); ?>
                                      <?php echo $this->fetch('content'); ?>
                              </div>
                              <!-- .vd_content-section --> 
                          </div>
                          <!-- .vd_content --> 
                      </div>
                      <!-- .vd_container --> 
                  </div>
                  <!-- .vd_content-wrapper --> 

                  <!-- Middle Content End --> 
              </div>
              <!-- .container --> 
          </div>
          <!-- .content -->

          <!-- Footer Start -->
          <footer class="footer-1"  id="footer">      
              <div class="vd_bottom ">
                  <div class="container">
                      <div class="row">
                          <div class=" col-xs-12">
                              <div class="copyright">
                                  Copyright &copy;2014 Mobmedianet. All Rights Reserved 
                              </div>
                          </div>
                      </div><!-- row -->
                  </div><!-- container -->
              </div>
          </footer>
          <!-- Footer END -->
      </div>

      <!-- .vd_body END  -->
      <a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<script type="text/javascript">
  {
    if(history.forward(1))
    location.replace(history.forward(1))
  }
</script>

<script type="text/javascript">
$(window).load(function() 
{
	"use strict";   

	$('#goto-menu a').click(function(e) {
        e.preventDefault();
		scrollTo($(this).attr('href'),-80);
    });
	
	$('#input-autocomplete').tagsInput({
		width: 'auto',

		autocomplete_url:'templates/files/fake_json_endpoint.html', // jquery ui autocomplete requires a json endpoint
	});
	
	var availableTags = [
	"ActionScript",
	"AppleScript",
	"Asp",
	"BASIC",
	"C",
	"C++",
	"Clojure",
	"COBOL",
	"ColdFusion",
	"Erlang",
	"Fortran",
	"Groovy",
	"Haskell",
	"Java",
	"JavaScript",
	"Lisp",
	"Perl",
	"PHP",
	"Python",
	"Ruby",
	"Scala",
	"Scheme"
	];
	
	$( "#normal-autocomplete" ).autocomplete({
		source: availableTags
	});
	
	 $.widget( "custom.catcomplete", $.ui.autocomplete, {
		_renderMenu: function( ul, items ) {
			var that = this,
			currentCategory = "";
			$.each( items, function( index, item ) {
				if ( item.category != currentCategory ) {
					ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
					currentCategory = item.category;
				}
					that._renderItemData( ul, item );
			});
		}
	});

	var data = [
	{ label: "anders", category: "" },
	{ label: "andreas", category: "" },
	{ label: "antal", category: "" },
	{ label: "annhhx10", category: "Products" },
	{ label: "annk K12", category: "Products" },
	{ label: "annttop C13", category: "Products" },
	{ label: "anders andersson", category: "People" },
	{ label: "andreas andersson", category: "People" },
	{ label: "andreas johnson", category: "People" }
	];
	$( "#category-autocomplete" ).catcomplete({
		delay: 0,
		source: data
	});
		
	var data_image = [
		{ label: "anders", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar.jpg" },
		{ label: "andreas", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-2.jpg" },
		{ label: "antal", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-3.jpg" },
		{ label: "annhhx10", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-4.jpg" },
		{ label: "annk K12", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-5.jpg" },
		{ label: "annttop C13", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-6.jpg" },
		{ label: "anders andersson", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-7.jpg"},
		{ label: "andreas andersson", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-8.jpg" },
		{ label: "andreas johnson", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-9.jpg" }
	];	
	$( "#image-autocomplete" ).autocomplete({
		minLength: 0,
		source: data_image,
		focus: function( event, ui ) {
		$( "#image-autocomplete" ).val( ui.item.label );
			return false;
		}
	})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		return $( "<li>" )
		.append( "<a href='javascript:void(0)'><span class='menu-icon'><img src='" + item.icon + "' alt='"+ item.icon +"'></span><span class='menu-text'>" + item.label + "<span class='menu-info'>" + item.desc + "</span></span></a>" )
		.appendTo( ul );
	};

	/* Multiple Values */
	function split( val ) {
		return val.split( /,\s*/ );
	}
	function extractLast( term ) {
		return split( term ).pop();
	}
	$( "#multiple-autocomplete" )
	// don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).data( "ui-autocomplete" ).menu.active ) {
		event.preventDefault();
	}
	})
	.autocomplete({
		minLength: 0,
		source: function( request, response ) {
		// delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
		availableTags, extractLast( request.term ) ) );
		},
		focus: function() {
		// prevent value inserted on focus
		return false;
		},
		select: function( event, ui ) {
		var terms = split( this.value );
		// remove the current input
		terms.pop();
		// add the selected item
		terms.push( ui.item.value );
		// add placeholder to get the comma-and-space at the end
		terms.push( "" );
		this.value = terms.join( ", " );
		return false;
		}
	});
	
	$( "#datepicker-normal" ).datepicker({ dateFormat: 'dd M yy'});
	$( "#datepicker-multiple" ).datepicker({
		numberOfMonths: 3,
		showButtonPanel: true,
		dateFormat: 'dd M yy'
	});	
	$( "#datepicker-from" ).datepicker({
		defaultDate: "+1w",
		dateFormat: 'dd M yy',
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
		$( "#to" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#datepicker-to" ).datepicker({
		defaultDate: "+1w",
		dateFormat: 'dd M yy',
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
		$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		}
	});	
	$( "#datepicker-icon" ).datepicker({ dateFormat: 'dd M yy'});
	$( '[data-datepicker]' ).click(function(e){ 
		var data=$(this).data('datepicker');
		$(data).focus();
	});
	$( "#datepicker-restrict" ).datepicker({ minDate: -20, maxDate: "+1M +10D" });	
	$( "#datepicker-widget" ).datepicker();	
	
	$('#datepicker-daterangepicker').daterangepicker(
		{
		  ranges: {
			 'Today': [moment(), moment()],
			 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
			 'Last 7 Days': [moment().subtract('days', 6), moment()],
			 'Last 30 Days': [moment().subtract('days', 29), moment()],
			 'This Month': [moment().startOf('month'), moment().endOf('month')],
			 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
		  },
		  startDate: moment().subtract('days', 29),
		  endDate: moment()
		},
		function(start, end) {
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		}
	);	
	
	$('#datepicker-datetime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });	

    $('.timepicker-default').timepicker();
    /*$('#timepicker-default2').timepicker();*/
	$('#timepicker-full').timepicker({
		minuteStep: 1,
		template: false,
		showSeconds: true,
		showMeridian: false,
	});		

	$('#colorpicker-hex').ColorPicker({
		color: '#ff00ff',
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		},
		onChange: function (hsb, hex, rgb) {
			$('#colorpicker-hex').val('#' + hex);
			$('#colorpicker-hex').siblings().css({'color' : '#' + hex});
		}			
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	}).siblings().click(function(e){ 
		$(this).siblings().click();
	});	
	
	$('#colorpicker-rgba').ColorPicker({
		color: '#ff00ff',
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		},
		onChange: function (hsb, hex, rgb) {
			$('#colorpicker-rgba').val('rgb(' + rgb['r'] +',' + rgb['g']+',' + rgb['b'] + ')');
			$('#colorpicker-rgba').siblings().css({'color' : 'rgb(' + rgb['r'] +',' + rgb['g']+',' + rgb['b'] + ')'});
		}			
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	}).siblings().click(function(e){ 
		$(this).siblings().click();
	});	

	//CKEDITOR.replace( $('[data-rel^="ckeditor"]') );
	$( '[data-rel^="ckeditor"]' ).ckeditor();

})
</script>

<script>
/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'plugins/jquery-file-upload/server/php/',
        uploadButton = $('<button/>')
            .addClass('btn vd_btn vd_bg-blue')
            .prop('disabled', true)
            .text('Processing...')
            .on('click', function () {
                var $this = $(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Abort')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });

    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 5000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index, file) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

	$('#wysiwyghtml').wysihtml5();		
});
</script>
<!-- Specific Page Scripts END -->

<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script> 

        <script type="text/javascript">
              $(window).load(function ()
              {




                  $.fn.UseTooltip = function () {
                      var previousPoint = null;

                      $(this).bind("plothover", function (event, pos, item) {
                          if (item) {
                              if (previousPoint != item.dataIndex) {

                                  previousPoint = item.dataIndex;

                                  $("#tooltip").remove();
                                  var x = item.datapoint[0].toFixed(2),
                                          y = item.datapoint[1].toFixed(2);

                                  showTooltip(item.pageX, item.pageY,
                                          "<p class='vd_bg-green'><strong class='mgr-10 mgl-10'>" + Math.round(x) + " NOV 2013 </strong></p>" +
                                          "<div style='padding: 0 10px 10px;'>" +
                                          "<div>" + item.series.label + ": <strong>" + Math.round(y) + "</strong></div>" +
                                          "<div> Profit: <strong>$" + Math.round(y) * 7 + "</strong></div>" +
                                          "</div>"
                                          );
                              }
                          } else {
                              $("#tooltip").remove();
                              previousPoint = null;
                          }
                      });
                  };

                  function showTooltip(x, y, contents) {
                      $('<div id="tooltip">' + contents + '</div>').css({
                          position: 'absolute',
                          display: 'none',
                          top: y + 5,
                          left: x + 20,
                          size: '10',
        //				'border-top' : '3px solid #1FAE66',
                          'background-color': '#111111',
                          color: "#FFFFFF",
                          opacity: 0.85
                      }).appendTo("body").fadeIn(200);
                  }


                  /* REVENUE LINE CHART */

                  var d2 = [[1, 250],
                      [2, 150],
                      [3, 50],
                      [4, 200],
                      [5, 50],
                      [6, 150],
                      [7, 150],
                      [8, 200],
                      [9, 100],
                      [10, 250],
                      [11, 250],
                      [12, 200],
                      [13, 300]

                  ];
                  var d1 = [
                      [1, 650],
                      [2, 550],
                      [3, 450],
                      [4, 550],
                      [5, 350],
                      [6, 500],
                      [7, 600],
                      [8, 450],
                      [9, 300],
                      [10, 600],
                      [11, 400],
                      [12, 500],
                      [13, 700]

                  ];
                  var plot = $.plotAnimator($("#revenue-line-chart"), [
                      {label: "Revenue",
                          data: d2,
                          lines: {
                              fill: 0.4,
                              lineWidth: 0,
                          },
                          color: ['#f2be3e']
                      }, {
                          data: d1,
                          animator: {steps: 60, duration: 1000, start: 0},
                          lines: {lineWidth: 2},
                          shadowSize: 0,
                          color: '#F85D2C'
                      }, {
                          label: "Revenue",
                          data: d1,
                          points: {show: true, fill: true, radius: 6, fillColor: "#F85D2C", lineWidth: 3},
                          color: '#fff',
                          shadowSize: 0
                      },
                      {label: "Cost",
                          data: d2,
                          points: {show: true, fill: true, radius: 6, fillColor: "#f2be3e", lineWidth: 3},
                          color: '#fff',
                          shadowSize: 0
                      }
                  ], {xaxis: {
                          tickLength: 0,
                          tickDecimals: 0,
                          min: 2,
                          font: {
                              lineHeight: 13,
                              style: "normal",
                              weight: "bold",
                              family: "sans-serif",
                              variant: "small-caps",
                              color: "#6F7B8A"
                          }
                      },
                      yaxis: {
                          ticks: 3,
                          tickDecimals: 0,
                          tickColor: "#f0f0f0",
                          font: {
                              lineHeight: 13,
                              style: "normal",
                              weight: "bold",
                              family: "sans-serif",
                              variant: "small-caps",
                              color: "#6F7B8A"
                          }
                      },
                      grid: {
                          backgroundColor: {colors: ["#fff", "#fff"]},
                          borderWidth: 1, borderColor: "#f0f0f0",
                          margin: 0,
                          minBorderMargin: 0,
                          labelMargin: 20,
                          hoverable: true,
                          clickable: true,
                          mouseActiveRadius: 6
                      },
                      legend: {show: false}
                  });

                  $("#revenue-line-chart").UseTooltip();

                  $(window).on("resize", function () {
                      plot.resize();
                      plot.setupGrid();
                      plot.draw();
                  });

                  /* REVENUE DONUT CHART */

                  var data2 = [],
                          series = 3;
                  var data2 = [
                      {label: "Men", data: 35},
                      {label: "Women", data: 65}
                  ];
                  var revenue_donut_chart = $("#revenue-donut-chart");

                  $("#revenue-donut-chart").bind("plotclick", function (event, pos, item) {
                      if (item) {
                          $("#clickdata").text(" - click point " + item.dataIndex + " in " + item.series.label);
                          plot.highlight(item.series, item.datapoint);
                      }
                  });
                  $.plot(revenue_donut_chart, data2, {
                      series: {
                          pie: {
                              innerRadius: 0.4,
                              show: true
                          }
                      },
                      grid: {
                          hoverable: true,
                          clickable: true,
                      },
                      colors: ["#1FAE66", "#F85D2C "]
                  });

                  /* REVENUE BAR CHART */

                  var bar_chart_data = [["Jan", 10], ["Feb", 8], ["Mar", 4], ["Apr", 13], ["May", 17], ["Jun", 9], ["Jul", 10], ["Aug", 8], ["Sep", 4], ["Oct", 13], ["Nov", 17], ["Dec", 9]];

                  var bar_chart = $.plot(
                          $("#revenue-bar-chart"), [{
                          data: bar_chart_data,
                          //           color: "rgba(31,174,102, 0.8)",
                          color: "#F85D2C",
                          shadowSize: 0,
                          bars: {
                              show: true,
                              lineWidth: 0,
                              fill: true,
                              fillColor: {
                                  colors: [{
                                          opacity: 1
                                      }, {
                                          opacity: 1
                                      }]
                              }
                          }
                      }], {
                      series: {
                          bars: {
                              show: true,
                              barWidth: 0.9,
                              align: "center"
                          }
                      },
                      grid: {
                          show: true,
                          hoverable: true,
                          borderWidth: 0
                      },
                      yaxis: {
                          min: 0,
                          max: 20,
                          show: false
                      },
                      xaxis: {
                          mode: "categories",
                          tickLength: 0,
                          color: "#FFFFFF",
                      }
                  });

                  var previousPoint2 = null;
                  $("#revenue-bar-chart").bind("plothover", function (event, pos, item) {
                      $("#x").text(pos.x.toFixed(2));
                      $("#y").text(pos.y.toFixed(2));
                      if (item) {
                        if (previousPoint2 != item.dataIndex) {
                          previousPoint2 = item.dataIndex;
                          $("#tooltip").remove();
                          var x = item.datapoint[0] + 1,
                                  y = item.datapoint[1].toFixed(2);
                          showTooltip(item.pageX, item.pageY,
                                  "<p class='vd_bg-green'><strong class='mgr-10 mgl-10'>" + x + " - 2013 </strong></p>" +
                                  "<div style='padding: 0 10px 10px;'>" +
                                  "<div> Sales: <strong>" + Math.round(y) * 17 + "</strong></div>" +
                                  "<div> Profit: <strong>$" + Math.round(y) * 280 + "</strong></div>" +
                                  "</div>"
                                  );
                        }
                      }
                  });

                  $('#revenue-bar-chart').bind("mouseleave", function () {
                      $("#tooltip").remove();
                  });

                  /* PIE CHART */

                  var pie_placeholder = $("#pie-chart");

                  var pie_data = [];

                  pie_data[0] = {
                      label: "IE",
                      data: 10
                  }
                  pie_data[1] = {
                      label: "Safari",
                      data: 8
                  }
                  pie_data[2] = {
                      label: "Opera",
                      data: 8
                  }
                  pie_data[3] = {
                      label: "Chrome",
                      data: 13
                  }
                  pie_data[4] = {
                      label: "Firefox",
                      data: 17
                  }
                  pie_data[5] = {
                      label: "Other",
                      data: 3
                  }
                  $.plot(pie_placeholder, pie_data, {
                      series: {
                          pie: {
                              show: true,
                              label: {
                                  show: true,
                                  radius: .5,
                                  formatter: labelFormatter,
                                  background: {
                                      opacity: 0
                                  }
                              },
                          }
                      },
                      grid: {
                          hoverable: true,
                          clickable: true
                      },
                      colors: ["#FCB660", "#ce91db", "#56A2CF", "#52D793", "#FC8660", "#CCCCCC"]
                  });

                  pie_placeholder.bind("plothover", function (event, pos, obj) {
                      if (!obj) {
                          return;
                      }
                      var percent = parseFloat(obj.series.percent).toFixed(2);
                      $("#hover").html("<span style='font-weight:bold; color:" + obj.series.color + "'>" + obj.series.label + " (" + percent + "%)</span>");
                  });

                  pie_placeholder.bind("plotclick", function (event, pos, obj) {
                      if (!obj) {
                          return;
                      }
                      percent = parseFloat(obj.series.percent).toFixed(2);
                      alert("" + obj.series.label + ": " + percent + "%");
                  });

                  function labelFormatter(label, series) {
                      return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                  }

                  var cityAreaData = [
                      500.70,
                      410.16,
                      210.69,
                      120.17,
                      64.31,
                      150.35,
                      130.22,
                      120.71,
                      100.32
                  ]
                  $('#map1').vectorMap({
                      map: 'world_mill_en',
                      scaleColors: ['#C8EEFF', '#0071A4'],
                      normalizeFunction: 'polynomial',
                      focusOn: {
                          x: 5,
                          y: 0.56,
                          scale: 1.7
                      },
                      zoomOnScroll: false,
                      zoomMin: 0.85,
                      hoverColor: false,
                      regionStyle: {
                          initial: {
                              fill: '#abe7c8',
                              "fill-opacity": 1,
                              stroke: '#abe7c8',
                              "stroke-width": 0,
                              "stroke-opacity": 0
                          },
                          hover: {
                              "fill-opacity": 0.8
                          },
                          selected: {
                              fill: 'yellow'
                          },
                          selectedHover: {
                          }
                      },
                      markerStyle: {
                          initial: {
                              fill: '#F85D2C',
                              stroke: '#F85D2C',
                              "fill-opacity": 0.9,
                              "stroke-width": 10,
                              "stroke-opacity": 0.5,
                              r: 3
                          },
                          hover: {
                              stroke: '#F85D2C',
                              "stroke-width": 14
                          },
                          selected: {
                              fill: 'blue'
                          },
                          selectedHover: {
                          }
                      },
                      backgroundColor: '#ffffff',
                      markers: [
                          {latLng: [50, 0], name: 'France - 43145 views'},
                          {latLng: [0, 120], name: 'Indonesia - 145 views'},
                          {latLng: [-25, 130], name: 'Australia - 486 views'},
                          {latLng: [0, 20], name: 'Africa - 12 views'},
                          {latLng: [35, 100], name: 'China - 7890 views'},
                          {latLng: [46, 105], name: 'Mongolia - 2123 views'},
                          {latLng: [40, 70], name: 'Kyrgiztan - 87456 views'},
                          {latLng: [58, 50], name: 'Russia - 4905 views'},
                          {latLng: [35, 135], name: 'Japan - 87456 views'}
                      ],
                      series: {
                          markers: [{
                                  attribute: 'r',
                                  scale: [3, 7],
                                  values: cityAreaData
                              }]
                      },
                  });

                  /* REAL TIME CHART */

                  var data = [],
                          totalPoints = 300;

                  function getRandomData() {

                      if (data.length > 0)
                          data = data.slice(1);

                      // Do a random walk

                      while (data.length < totalPoints) {

                          var prev = data.length > 0 ? data[data.length - 1] : 50,
                                  y = prev + Math.random() * 10 - 5;

                          if (y < 0) {
                              y = 0;
                          } else if (y > 100) {
                              y = 100;
                          }

                          data.push(y);
                      }

                      // Zip the generated y values with the x values

                      var res = [];
                      for (var i = 0; i < data.length; ++i) {
                          res.push([i, data[i]])
                      }

                      return res;
                  }

                  // Set up the control widget

                  var updateInterval = 30;
                  $("#updateInterval").val(updateInterval).change(function () {
                      var v = $(this).val();
                      if (v && !isNaN(+v)) {
                          updateInterval = +v;
                          if (updateInterval < 1) {
                              updateInterval = 1;
                          } else if (updateInterval > 2000) {
                              updateInterval = 2000;
                          }
                          $(this).val("" + updateInterval);
                      }
                  });

                  var realtime_chart = $.plot("#realtime-chart", [getRandomData()], {
                      series: {
                          shadowSize: 0, // Drawing is faster without shadows
                          lines: {
                              fill: true,
                              fillColor: "#ffe29c",
                          },
                          color: "#ffe29c",
                      },
                      yaxis: {
                          min: 0,
                          max: 100
                      },
                      xaxis: {
                          show: false
                      },
                      grid: {
                          borderWidth: 0
                      },
                  });

                  function update() {
                      realtime_chart.setData([getRandomData()]);

                      // Since the axes don't change, we don't need to call plot.setupGrid()
                      realtime_chart.draw();
                      setTimeout(update, updateInterval);
                  }

                  update();

                  /* FULL CALENDAR  */

                  var date = new Date();
                  var d = date.getDate();
                  var m = date.getMonth();
                  var y = date.getFullYear();

                  $('#calendar').fullCalendar({
                      header: {
                          left: 'title',
                          center: '',
                          right: 'today prev,next'
                      },
                      editable: true,
                      events: [
                          {
                              title: 'All Day Event',
                              start: new Date(y, m, 1)
                          },
                          {
                              title: 'Long Event',
                              start: new Date(y, m, d - 5),
                              end: new Date(y, m, d - 2)
                          },
                          {
                              id: 999,
                              title: 'Repeating Event',
                              start: new Date(y, m, d - 3, 16, 0),
                              allDay: false
                          },
                          {
                              id: 999,
                              title: 'Repeating Event',
                              start: new Date(y, m, d + 4, 16, 0),
                              allDay: false
                          },
                          {
                              title: 'Meeting',
                              start: new Date(y, m, d, 10, 30),
                              allDay: false
                          },
                          {
                              title: 'Lunch',
                              start: new Date(y, m, d, 12, 0),
                              end: new Date(y, m, d, 14, 0),
                              allDay: false
                          },
                          {
                              title: 'Birthday Party',
                              start: new Date(y, m, d + 1, 19, 0),
                              end: new Date(y, m, d + 1, 22, 30),
                              allDay: false
                          },
                          {
                              title: 'Click for Google',
                              start: new Date(y, m, 28),
                              end: new Date(y, m, 29),
                              url: 'http://google.com/'
                          }
                      ]
                  });

        // Skycons
                  var icons = new Skycons({"color": "white", "resizeClear": true}),
                          icons_btm = new Skycons({"color": "#F89C2C", "resizeClear": true}),
                          list = "clear-day",
                          livd_btm = ["rain", "wind"
                          ];
                  icons.set(list, list)
                  for (var i = livd_btm.length; i--; )
                      icons_btm.set(livd_btm[i], livd_btm[i]);

                  icons.play();
                  icons_btm.play();

                  /* News Widget */
                  $(".vd_news-widget .vd_carousel").carouFredSel({
                      prev: {
                          button: function ()
                          {
                              return $(this).parent().parent().children('.vd_carousel-control').children('a:first-child')
                          }
                      },
                      next: {
                          button: function ()
                          {
                              return $(this).parent().parent().children('.vd_carousel-control').children('a:last-child')
                          }
                      },
                      scroll: {
                          fx: "crossfade",
                          onBefore: function () {
                              var target = "#front-1-clients";
                              $(target).css("transition", "all .5s ease-in-out 0s");
                              if ($(target).hasClass("vd_bg-soft-yellow")) {
                                  $(target).removeClass("vd_bg-soft-yellow");
                                  $(target).addClass("vd_bg-soft-red");
                              } else
                              if ($(target).hasClass("vd_bg-soft-red")) {
                                  $(target).removeClass("vd_bg-soft-red");
                                  $(target).addClass("vd_bg-soft-blue");
                              } else
                              if ($(target).hasClass("vd_bg-soft-blue")) {
                                  $(target).removeClass("vd_bg-soft-blue");
                                  $(target).addClass("vd_bg-soft-green");
                              } else
                              if ($(target).hasClass("vd_bg-soft-green")) {
                                  $(target).removeClass("vd_bg-soft-green");
                                  $(target).addClass("vd_bg-soft-yellow");
                              }
                          }
                      },
                      width: "auto",
                      height: "responsive",
                      responsive: true,
                      auto: 3000

                  });

                  // Notification
                  setTimeout(function () {
                      notification("topright", "notify", "fa fa-exclamation-triangle vd_yellow", "Welcome to Vendroid", "Click on <i class='fa fa-question-circle vd_red'></i> Question Mark beside filter to take a view layout tour guide");
                  }, 1500);
              });
        </script>
        <!-- Specific Page Scripts END -->

<script type="text/javascript">
		$(document).ready(function() {
			"use strict";
			
			$('#data-tables').dataTable({
                                    "language": {
                                        "lengthMenu": "Muestra _MENU_ registros por página",
                                        "zeroRecords": "Disculpe, no se encontraron registros para su búsqueda",
                                        "info": "Mostrando página _PAGE_ de _PAGES_",
                                        "infoEmpty": "No hay registros disponibles",
                                        "infoFiltered": "(filtered from _MAX_ total records)",
                                        "search": "Buscar:",
                                        "paginate": {
                                            "first":      "Primero",
                                            "last":       "Último",
                                            "next":       "Próx",
                                            "previous":   "Prev"
                                        },
                                    }
                                });
		} );
</script>

<!-- Specific Page Scripts END -->

<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script> 
        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
        <script>
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-XXXXX-X']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
        </script> 

    </body>
<?php }else{?>
    <body id="pages" class="full-layout no-nav-left no-nav-right  nav-top-fixed background-login     responsive remove-navbar login-layout   clearfix" data-active="pages "  data-smooth-scrolling="1">     
<div class="vd_body">
<!-- Header Start -->

<!-- Header Ends --> 
<div class="content">
  <div class="container"> 
    
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
            <div class="vd_content-section clearfix" style="padding: 0px;">
            <div class="vd_login-page">
                <div class="heading clearfix" style="margin-bottom: 0px;">
                <div class="logo">
                  <h2 class="mgbt-xs-5"><?php echo $this->Html->image('logo_verde.png'); ?></h2>
                </div>
              </div>
              <div class="panel widget">
                <div class="panel-body" style="text-align: center;">
                  <div class="fa fa-key fa-5x" style="padding-bottom: 35px;"></div>
                  <div class="alert alert-danger vd_hidden">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> Change a few things up and try submitting again. </div>
                    <div class="form-block">
                      <?php echo $this->Session->flash(); ?>
                      <?php echo $this->fetch('content'); ?>
                    </div>
              </div>
              <!-- Panel Widget -->
            </div>
            <!-- vd_login-page --> 
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 
  </div>
  <!-- .container --> 
</div>
<!-- .content -->

<!-- Footer Start -->
  <footer class="footer-2"  id="footer">      
    <div class="vd_bottom ">
        <div class="container">
            <div class="row">
              <div class=" col-xs-12">
                <div class="copyright text-center">
                  	Copyright &copy;2014 Mobile Media Networks. Todos los Derechos Reservados 
                </div>
              </div>
            </div><!-- row -->
        </div><!-- container -->
    </div>
  </footer>
<!-- Footer END -->

</div>

<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>
<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<!-- Specific Page Scripts Put Here -->
<script type="text/javascript">
$(document).ready(function() {
	
	"use strict";

    var form_register_2 = $('#login-form');
    var error_register_2 = $('.alert-danger', form_register_2);
    var success_register_2 = $('.alert-success', form_register_2);

    form_register_2.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
			
            email: {
                required: true,
                email: true
            },				
            password: {
                required: true,
				minlength: 6
            },
			
        },
		
		errorPlacement: function(error, element) {
			if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
				element.parent().append(error);
			} else if (element.parent().hasClass("vd_input-wrapper")){
				error.insertAfter(element.parent());
			}else {
				error.insertAfter(element);
			}
		}, 
		
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register_2.hide();
            error_register_2.show();
        },

        highlight: function (element) { // hightlight error inputs
	
			$(element).addClass('vd_bd-red');
			$(element).parent().siblings('.help-inline').removeClass('help-inline hidden');
			if ($(element).parent().hasClass("vd_checkbox") || $(element).parent().hasClass("vd_radio")) {
				$(element).siblings('.help-inline').removeClass('help-inline hidden');
			}
        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline hidden') // mark the current input as valid and display OK icon
            	.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
			$(element).removeClass('vd_bd-red');			
        },

        submitHandler: function (form) {
			$(form).find('#login-submit').prepend('<i class="fa fa-spinner fa-spin mgr-10"></i>')/*.addClass('disabled').attr('disabled')*/;					
            success_register_2.show();
            error_register_2.hide();				
			setTimeout(function(){window.location.href = "index.html"},2000)	 ; 				
        }
    });	
	
});
</script>
<!-- Specific Page Scripts END -->

<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>

</body>
<?php } ?>

</html>