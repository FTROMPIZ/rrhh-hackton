<div class="vd_head-section clearfix">
  <div class="vd_panel-header">
    <ul class="breadcrumb">
        <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>">Home</a> </li>
        <li class="active">Comedor</li>
    </ul>
    <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
      <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
    </div>
  </div>
</div>

<div class="vd_title-section clearfix">
  <div class="vd_panel-header">
      <h1>Comedor</h1>
  </div>
</div>
    
<?php 
  if(isset($module)){ 
?> 
    <div class="vd_content-section clearfix">
      <div class="panel widget" style="margin-bottom:0px;">
        <div class="panel-heading vd_bg-grey">
          <h3 class="panel-title"> <span class="menu-icon">  </span> Filtro </h3>
        </div>
        <div class="panel-body table-responsive">
          <div class="users form">
            <?php echo $this->Form->create('Diningroom'); ?>
            <fieldset>
              <div class="col-md-5">
                <div class="form-group">
                  <label class="control-label">Fecha</label>
                  <div class="controls">
                    <div class="input-group">
                      <input type="text" name ="date" placeholder="Date" id="datepicker-icon" readonly>
                      <span class="input-group-addon" id="datepicker-icon-trigger" data-datepicker="#datepicker-icon"><i class="fa fa-calendar"></i></span> 
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-5">
                <?php echo $this->Form->input('Branch_idBranch', array('label'=>'Sucursal','options' => $branches)); ?>
              </div>
              <div class="col-md-2" style="">
                <?php 
                  echo $this->Form->input("Buscar",array("label" => false,'div'=>'false', "class" => "btn btn-success start", "type" => "submit","style"=>"margin-top:30px;"));
                  echo $this->Form->end(); 
                ?>
              </div>
            </fieldset>
          </div>
        </div>
      </div>
   
      <div class="related">
		    <div class="row">
          <div class="col-md-12">
            <div class="panel widget">
              <div class="panel-heading vd_bg-grey">
                <h3 class="panel-title"> <span class="menu-icon">  </span> Listado de comedores </h3>
              </div>

              <div class="panel-body table-responsive">
                <table class="table table-striped" id="data-tables">
                  <thead>
                    <tr>
                      <th>Fecha</th>
                      <th>Sucursal</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
  	                <?php foreach ($diningrooms as $diningroom): ?>
                    	<tr>
                    		<td><span style='display: none;'><?php echo date('Y/m/d',strtotime($diningroom['Diningroom']['date'])); ?></span><?php echo date('d-m-Y',strtotime($diningroom['Diningroom']['date'])); ?></td>
                    		<td>
                          <?php $var=$thisDiningroom->Diningroom->query("SELECT name FROM branch WHERE idBranch=".$diningroom['Diningroom']['Branch_idBranch']); 
                                echo $var[0]['branch']['name'];
                          ?>&nbsp;
                        </td>
                                    
                        <td class="menu-action" valign="middle" style="vertical-align:middle;">
                          <a data-original-title="view" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green" href="<?php echo $this->Html->url(array( 'action' => 'view',$diningroom['Diningroom']['idDiningRoom'])) ?>"> <i class="fa fa-eye"></i> </a> 
                          <a data-original-title="edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow" href="<?php echo $this->Html->url(array( 'action' => 'edit',$diningroom['Diningroom']['idDiningRoom'])) ?>"> <i class="fa fa-pencil"></i> </a>
                          <form action="<?php echo $this->Html->url(array( 'action' => 'delete',$diningroom['Diningroom']['idDiningRoom'])) ?>" name="post_deleteRow<?php echo $diningroom['Diningroom']['idDiningRoom'] ?>" id="post_deleteRow<?php echo $diningroom['Diningroom']['idDiningRoom'] ?>" style="display:none;" method="post">
                            <input type="hidden" name="_method" value="POST">
                          </form>
                          <a href="#" onclick="if (confirm(&quot;¿Estás seguro que deseas borrar el menú del día: <?php echo date('d-m-Y',strtotime($diningroom['Diningroom']['date'])); ?> ?&quot;)) { document.post_deleteRow<?php echo $diningroom['Diningroom']['idDiningRoom'] ?>.submit(); } event.returnValue = false; return false;"  class="btn menu-icon vd_bd-red vd_red" href=""><i class="glyphicon glyphicon-trash"></i></a>
                        </td>
                    	</tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          <!-- Panel Widget --> 
          </div>
        <!-- col-md-12 --> 
        </div>
        <a href="<?php echo $this->Html->url(array('controller' => 'fileuploads', 'action' => 'addDiningroom')) ?>" class="btn btn-success start"> <i class="glyphicon glyphicon-plus"></i> <span>Agregar Men&uacute;</span> </a>
      </div> 
    </div>

<?php 
  }else{
?>
    <div class="alert alert-danger"> 
      <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
      <strong>Error! </strong>No tienes acceso a esta locación 
    </div>
<?php 
  } 
?>

<div class="vd_content-section clearfix" hidden="true">
  <div class="row" id="auto-complete-input">
    <div class="col-md-12">
      <div class="panel widget">
        <div class="panel-heading vd_bg-grey">
          <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Auto Complete Input </h3>
        </div>
        <div class="panel-body">
          <form class="form-horizontal" action="#" role="form">
            <div class="form-group">
              <div class="col-sm-7 controls">
                <input class="width-70" type="text" placeholder="Try typing 'a'" id="image-autocomplete">
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- Panel Widget --> 
    </div>
    <!-- col-md-12 --> 
  </div>
</div>