<div class="vd_head-section clearfix">
    <div class="vd_panel-header">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>">Home</a> </li>
            <li><a href="<?php echo $this->Html->url(array('controller' => 'diningrooms', 'action' => 'index')) ?>">Menú Diario</a> </li>
            <li class="active">Ver Menú</li>
        </ul>
        <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
            <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
            <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
            <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>

        </div>

    </div>
</div>

<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1>Menú del día <?php echo date('d-m-Y',strtotime($diningroom['Diningroom']['date'])); ?></h1>

    </div>
</div>

<div class="clinics view">
    <div class="vd_content-section clearfix">
<div class="related">
    <div class="row" style="background: white">
        <table class="table table-bordered table-striped" style="margin-bottom: 0px;">
                        <tbody>
                          <tr>
                            <th>Fecha:</th>
                            <td><?php echo date('d-m-Y',strtotime($diningroom['Diningroom']['date'])); ?></td>
                          </tr>
                          <tr>
                            <th>Sucursal:</th>
                            <td><?php $var=$thisDiningroom->Diningroom->query("SELECT name FROM branch WHERE idBranch=".$diningroom['Diningroom']['Branch_idBranch']); 
                                echo $var[0]['branch']['name'];?>
                            </td>
                          </tr>
                            <th>Imagen:</th>
                            <td><?php if($diningroom['Diningroom']['imgPath']!='0'){ echo '<img src="'.$diningroom['Diningroom']['imgPath'].'">'; }else{echo 'No posee imagen asociada';} ?></td>
                          </tr>
                        </tbody>
    </table>
          
          </div>
        </div>
    <a href="<?php echo $this->Html->url(array('controller' => 'diningrooms', 'action' => 'edit',$diningroom['Diningroom']['idDiningRoom'])) ?>" class="btn btn-success start"> <i class="glyphicon glyphicon-upload"></i> <span>Editar Menú</span> </a>
    <a href="<?php echo $this->Html->url(array('controller' => 'fileuploads', 'action' => 'editDiningroom',$diningroom['Diningroom']['idDiningRoom'])) ?>" class="btn btn-warning start"> <i class="glyphicon glyphicon-picture"></i> <span>Editar Imagen</span> </a>
    <form action="<?php echo $this->Html->url(array('controller' => 'diningrooms', 'action' => 'delete', $diningroom['Diningroom']['idDiningRoom'])) ?>" name="post_delete" id="post_delete" style="display:none;" method="post">
        <input type="hidden" name="_method" value="POST">
    </form>
    <a href="#" onclick="if (confirm(&quot;¿Estás seguro que deseas borrar el menú del día: <?php echo date('d-m-Y',strtotime($diningroom['Diningroom']['date'])); ?> ?&quot;)) { document.post_delete.submit(); } event.returnValue = false; return false;" class="btn btn-danger start"><i class="glyphicon glyphicon-trash"></i>Borrar</a>
    <br><br>
    <div class="row">
              <div class="col-md-12">
                <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon">  </span> Listado de Comidas </h3>
                  </div>
                  <div class="panel-body table-responsive">
    <table class="table table-hover">
    <?php if (!empty($thisDiningroom->Diningroom->query('SELECT distinct(menutype.description) FROM `menu`,menutype WHERE menu.`menuType`= menutype.idMenuType AND `menu`.DiningRoom_idDiningRoom='.$diningroom['Diningroom']['idDiningRoom']))): ?> 
        <?php foreach ($thisDiningroom->Diningroom->query('SELECT distinct(menutype.description) FROM `menu`,menutype WHERE menu.`menuType`= menutype.idMenuType AND `menu`.DiningRoom_idDiningRoom='.$diningroom['Diningroom']['idDiningRoom']) as $option): ?>
                    <thead class="vd_bg-black-10 vd_bd-black">
                        <tr>
                          <th><?php echo $option['menutype']['description']?></th>
                          <th valign="middle" style="vertical-align:middle;"></th>
                        </tr>
                      </thead>
                      <tbody>
                          
                        <?php if (!empty($thisDiningroom->Diningroom->query('SELECT * FROM `menu`,menutype WHERE menu.`menuType`= menutype.idMenuType AND `menu`.DiningRoom_idDiningRoom='.$diningroom['Diningroom']['idDiningRoom'].' AND menutype.description="'.$option['menutype']['description'].'"'))): ?> 
                            <?php foreach ($thisDiningroom->Diningroom->query('SELECT * FROM `menu`,menutype WHERE menu.`menuType`= menutype.idMenuType AND `menu`.DiningRoom_idDiningRoom='.$diningroom['Diningroom']['idDiningRoom'].' AND menutype.description="'.$option['menutype']['description'].'"') as $menu): ?>
                                <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $menu['menu']['description'] ?></td>
                                        <td class="menu-action" style="width:15%;">
                                              <a style="margin-left: 16px; padding-left: 16px;" ></a>
                                              <a data-original-title="edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow" href="<?php echo $this->Html->url(array('controller' => 'menus', 'action' => 'edit',$menu['menu']['idMenu'],$diningroom['Diningroom']['idDiningRoom'])) ?>"> <i class="fa fa-pencil"></i> </a>
                                              <form action="<?php echo $this->Html->url(array('controller' => 'menus', 'action' => 'delete',$menu['menu']['idMenu'],$diningroom['Diningroom']['idDiningRoom'])) ?>" name="post_deleteRow<?php echo $menu['menu']['idMenu'] ?>" id="post_deleteRow<?php echo $menu['menu']['idMenu'] ?>" style="display:none;" method="post">
                                                    <input type="hidden" name="_method" value="POST">
                                                </form>
                                                <a href="#" onclick="if (confirm(&quot;¿Estás seguro que deseas borrar el menú: <?php echo $menu['menu']['description'] ?> ?&quot;)) { document.post_deleteRow<?php echo $menu['menu']['idMenu'] ?>.submit(); } event.returnValue = false; return false;"  class="btn menu-icon vd_bd-red vd_red" href=""><i class="glyphicon glyphicon-trash"></i></a>
                                          </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
        <?php endforeach; ?>
                      
    <?php endif; ?>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Agregar nueva Comida</td>
                                    <td class="menu-action">
                                        <a  style=" margin-bottom: 5px;" data-original-title="edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green" href="<?php echo $this->Html->url(array('controller' => 'menus', 'action' => 'add', $diningroom['Diningroom']['idDiningRoom'])) ?>"> <i class="fa fa-plus"></i> </a>
                                    </td>
                                </tr>
                      </tbody>
    </table>
                          
                  </div>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-md-12 --> 
            </div>