<div class="vd_head-section clearfix">
    <div class="vd_panel-header">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>">Home</a> </li>
            <li><a href="<?php echo $this->Html->url(array('controller' => 'diningrooms', 'action' => 'index')) ?>">Comedor</a> </li>
            <li class="active">Editar Comedor</li>
        </ul>
        <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
            <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
            <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
            <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>

        </div>

    </div>
</div>

<div class="questionaires view">
<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1>Editar Comedor</h1>

    </div>
</div> 
    <?php if(isset($module)){ ?>   
              <div class="vd_content-section clearfix">
<div class="related">
<div class="users form">
<?php echo $this->Form->create('Diningroom'); ?>
	<fieldset>
	<div class="form-group">
                        <label class="control-label">Fecha</label>
                        <div class="controls">
                          <div class="input-group">
                            <input type="text" name ="date" value="<?php echo $dateEdit; ?>" placeholder="Date" id="datepicker-icon" readonly>
                            <span class="input-group-addon" id="datepicker-icon-trigger" data-datepicker="#datepicker-icon"><i class="fa fa-calendar"></i></span> </div>
                        </div>
                      </div>
            
                <?php echo $this->Form->input('Branch_idBranch', array('label'=>'Sucursal',
                    'options' => $branches
                )); ?>
	</fieldset><br>
<?php echo $this->Form->input("Editar",array("label" => false,'div'=>'false', "class" => "btn btn-success start", "type" => "submit"));
	echo $this->Form->end(); ?>
</div>
          </div>
          
        </div>
<?php }else{?>
    <div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>No tienes acceso a esta locación </div>
    <?php } ?>

    
<div class="vd_content-section clearfix" hidden="true">
            <div class="row" id="auto-complete-input">
              <div class="col-md-12">
                <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Auto Complete Input </h3>
                  </div>
                  <div class="panel-body">
                    <form class="form-horizontal" action="#" role="form">
                      <div class="form-group">
                        <div class="col-sm-7 controls">
                          <input class="width-70" type="text" placeholder="Try typing 'a'" id="image-autocomplete">
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-md-12 --> 
            </div>
          </div>