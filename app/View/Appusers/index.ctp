
<div class="vd_head-section clearfix">
    <div class="vd_panel-header">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>">Home</a> </li>
            <li class="active">Usuarios de Aplicación</li>
        </ul>
        <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
            <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
            <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
            <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>

        </div>

    </div>
</div>

<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1>Lista de Usuarios de Aplicación</h1>

    </div>
</div>
    <?php if(isset($module)){ ?>  
         <div class="vd_content-section clearfix">
<div class="related">
		<div class="row">
              <div class="col-md-12">
                <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon">  </span> Listado de Usuarios de Aplicación </h3>
                  </div>
                  <div class="panel-body table-responsive">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Usuario</th>
                          <th>Correo</th>
                          <th>Nombre</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
	<?php foreach ($appusers as $appuser): ?>
	<tr>
		<td><?php echo $appuser['Appuser']['username']; ?>&nbsp;</td>
		<td><?php echo $appuser['Appuser']['email']; ?>&nbsp;</td>
		<td><?php echo $appuser['Appuser']['name']; ?>&nbsp;</td>
		<td class="menu-action">
                      <a data-original-title="edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow" href="<?php echo $this->Html->url( array('action' => 'edit', $appuser['Appuser']['idAppUser'])); ?>"> <i class="fa fa-pencil"></i> </a>
                      <form action="<?php echo $this->Html->url(array('controller' => 'appusers', 'action' => 'delete',$appuser['Appuser']['idAppUser'])) ?>" name="post_deleteRow<?php echo $appuser['Appuser']['idAppUser'] ?>" id="post_deleteRow<?php echo $appuser['Appuser']['idAppUser'] ?>" style="display:none;" method="post">
                            <input type="hidden" name="_method" value="POST">
                        </form>
                        <a href="#" onclick="if (confirm(&quot;¿Estás seguro que deseas borrar el usuario: <?php echo $appuser['Appuser']['username'] ?> ?&quot;)) { document.post_deleteRow<?php echo $appuser['Appuser']['idAppUser'] ?>.submit(); } event.returnValue = false; return false;"  class="btn menu-icon vd_bd-red vd_red" href=""><i class="glyphicon glyphicon-trash"></i></a>
                  </td>
	</tr>
<?php endforeach; ?>
	</tbody>
                    </table>
                  </div>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-md-12 --> 
            </div>
    <a href="<?php echo $this->Html->url(array('controller' => 'appusers', 'action' => 'add')) ?>" class="btn btn-success start"> <i class="glyphicon glyphicon-plus"></i> <span>Agregar Usuario</span> </a>
</div> 
         </div>
<?php }else{?>
    <div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>No tienes acceso a esta locación </div>
    <?php } ?>