<div class="appusers view">
<h2><?php echo __('Appuser'); ?></h2>
	<dl>
		<dt><?php echo __('IdAppUser'); ?></dt>
		<dd>
			<?php echo $appuser['Appuser']['idAppUser']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo $appuser['Appuser']['username']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo $appuser['Appuser']['email']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo $appuser['Appuser']['password']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo $appuser['Appuser']['name']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Appuser'), array('action' => 'edit', $appuser['Appuser']['idAppUser'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Appuser'), array('action' => 'delete', $appuser['Appuser']['idAppUser']), array(), __('Are you sure you want to delete # %s?', $appuser['Appuser']['idAppUser'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Appusers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Appuser'), array('action' => 'add')); ?> </li>
	</ul>
</div>
