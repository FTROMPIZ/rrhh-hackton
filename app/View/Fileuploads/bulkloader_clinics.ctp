<div class="vd_head-section clearfix">
    <div class="vd_panel-header">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>">Home</a> </li>
            <li class="active">Carga de Clínicas</li>
        </ul>
        <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
            <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
            <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
            <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>

        </div>

    </div>
</div>

<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1>Cargar de Clínicas</h1>

    </div>
</div>

<div class="fileuploads form" style="margin: 15px; padding: 15px;">
<?php echo $this->Form->create('Fileupload',array('type'=>'file')); ?>
        <fieldset>
	<?php
		echo $this->Form->input('fileName', array('type' => 'hidden'));
	?>
	</fieldset>
        <fieldset>
	<?php
		echo $this->Form->input(
                                        'archivo', array(
                                                            'type' => 'file',
                                                            'accept' => '.csv',
                                                        )
                                        );
	?>
	</fieldset>
 <br>
    
<?php 
echo $this->Form->input("Cargar",array("label" => false,'div'=>'false', "class" => "btn btn-success start", "type" => "submit"));	
echo $this->Form->end(); ?>
</div>