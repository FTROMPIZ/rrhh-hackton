<div class="vd_head-section clearfix">
    <div class="vd_panel-header">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>">Home</a> </li>
            <li><a href="<?php echo $this->Html->url(array('controller' => 'fileuploads', 'action' => 'bulkloaderClinics')) ?>">Cargar Clínicas</a> </li>
            <li class="active">Log Clínicas</li>
        </ul>
        <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
            <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
            <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
            <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>

        </div>

    </div>
</div>

<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1>Log de Clínicas</h1>
    </div>
</div>
<div class="fileuploads view" style="margin: 15px; padding: 15px;">
    <textarea readonly name="comment" form="usrform" style=" height: 400px;"><?php
                $dir2 = new Folder("");
                $file = new File($dir2->pwd().'/LogClinicas.txt');
                $contents = $file->read();
                if($contents){
                    echo $contents;
                }else{
                    echo "La carga masiva de clínicas se ha realizado con exito.";
                }
                $file->close();
    ?></textarea>
</div>
