
<div class="vd_head-section clearfix">
    <div class="vd_panel-header">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>">Home</a> </li>
            <li><a href="<?php echo $this->Html->url(array('controller' => 'events', 'action' => 'index')) ?>">Eventos</a> </li>
            <li class="active">Ver Evento</li>
        </ul>
        <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
            <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
            <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
            <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>

        </div>

    </div>
</div>

<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1>Evento: <?php echo $event['Event']['name']; ?></h1>

    </div>
</div>

<div class="clinics view">
    <div class="vd_content-section clearfix">
<div class="related">
    <div class="row" style="background: white">
        <table class="table table-bordered table-striped" style="margin-bottom: 0px;">
                        <tbody>
                          <tr>
                            <th>Nombre:</th>
                            <td><?php echo $event['Event']['name']; ?></td>
                          </tr>
                          <tr>
                            <th>Descripción:</th>
                            <td><?php echo $event['Event']['description']; ?></td>
                          </tr>
                          <tr>
                            <th>Fecha:</th>
                            <td><?php echo date('d-m-Y g:i a',strtotime($event['Event']['date'])); ?></td>
                          </tr>
                          <tr>
                            <th>Lugar:</th>
                            <td><?php echo $event['Event']['place']; ?></td>
                          </tr>
                          <tr>
                            <th>Relevante:</th>
                            <td><?php if($event['Event']['outstanding']=='0'){echo 'No';}else{echo 'Si';} ?></td>
                          </tr>
                          <tr>
                            <th>Imagen:</th>
                            <td><?php if($event['Event']['imgPath']!='0'){ echo '<img src="'.$event['Event']['imgPath'].'">'; }else{echo 'No posee imagen asociada';} ?></td>
                          </tr>
                        </tbody>
    </table>
          
          </div>
        </div>
    <a href="<?php echo $this->Html->url(array('controller' => 'events', 'action' => 'edit',$event['Event']['idEvent'])) ?>" class="btn btn-success start"> <i class="glyphicon glyphicon-upload"></i> <span>Editar Evento</span> </a>
    <a href="<?php echo $this->Html->url(array('controller' => 'fileuploads', 'action' => 'editEvent',$event['Event']['idEvent'])) ?>" class="btn btn-warning start"> <i class="glyphicon glyphicon-picture"></i> <span>Editar Imagen</span> </a>
    <form action="<?php echo $this->Html->url(array('controller' => 'events', 'action' => 'delete', $event['Event']['idEvent'])) ?>" name="post_delete" id="post_delete" style="display:none;" method="post">
        <input type="hidden" name="_method" value="POST">
    </form>
    <a href="#" onclick="if (confirm(&quot;¿Estás seguro que deseas borrar la clínica: <?php echo $event['Event']['name']; ?> ?&quot;)) { document.post_delete.submit(); } event.returnValue = false; return false;" class="btn btn-danger start"><i class="glyphicon glyphicon-trash"></i>Borrar</a>