<div class="vd_head-section clearfix">
    <div class="vd_panel-header">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>">Home</a> </li>
            <li class="active">Talleres</li>
        </ul>
        <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
            <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
            <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
            <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>

        </div>
    </div>
</div>

<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1>Talleres</h1>
    </div>
</div>

<?php if(isset($module)){ ?>  
  <div class="vd_content-section clearfix">
    <div class="related">
		  <div class="row">
          <div class="col-md-12">
              <div class="panel widget">
                <div class="panel-heading vd_bg-grey">
                  <h3 class="panel-title"> <span class="menu-icon">  </span> Listado de Talleres </h3>
                </div>
                <div class="panel-body table-responsive">
                  <table class="table table-striped" id="data-tables">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Teléfono</th>
                        <th>Dirección</th>
                        <th>Ciudad</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($repairshops as $repairshop): ?>
                        <tr>
                          <td><?php echo $repairshop['Repairshop']['name']; ?>&nbsp;</td>
                          <td><?php echo $repairshop['Repairshop']['phone']; ?>&nbsp;</td>
                          <td><?php echo $repairshop['Repairshop']['address']; ?>&nbsp;</td>
                          <td><?php echo $repairshop['Repairshop']['city']; ?>&nbsp;</td>
                          <td><?php echo $repairshop['Repairshop']['state']; ?>&nbsp;</td>
                          <td class="menu-action" valign="middle" style="vertical-align:middle;">
                            <a data-original-title="view" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green" href="<?php echo $this->Html->url(array( 'action' => 'view',$repairshop['Repairshop']['idRepairShop'])) ?>"> <i class="fa fa-eye"></i> </a> 
                            <a data-original-title="edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow" href="<?php echo $this->Html->url(array( 'action' => 'edit',$repairshop['Repairshop']['idRepairShop'])) ?>"> <i class="fa fa-pencil"></i> </a>
                            <form action="<?php echo $this->Html->url(array( 'action' => 'delete',$repairshop['Repairshop']['idRepairShop'])) ?>" name="post_deleteRow<?php echo $repairshop['Repairshop']['idRepairShop'] ?>" id="post_deleteRow<?php echo $repairshop['Repairshop']['idRepairShop'] ?>" style="display:none;" method="post">
                              <input type="hidden" name="_method" value="POST">
                            </form>
                           <a href="#" onclick="if (confirm(&quot;¿Estás seguro que deseas borrar el taller: <?php echo $repairshop['Repairshop']['name'] ?> ?&quot;)) { document.post_deleteRow<?php echo $repairshop['Repairshop']['idRepairShop'] ?>.submit(); } event.returnValue = false; return false;"  class="btn menu-icon vd_bd-red vd_red" href=""><i class="glyphicon glyphicon-trash"></i></a>
                          </td>
                        </tr>
                      <?php endforeach; ?>
	                  </tbody>
                  </table>
                </div>
              </div>
            <!-- Panel Widget --> 
            </div>
          <!-- col-md-12 --> 
          </div>
          
          <a href="<?php echo $this->Html->url(array('controller' => 'repairshops', 'action' => 'add')) ?>" class="btn btn-success start"> <i class="glyphicon glyphicon-plus"></i> <span>Agregar Taller</span> </a>

          <?php echo $this->Html->link('Borrar Data', '/repairshops/delete_data', array('class' => 'delete_data btn btn-success start', 'onclick' => 'confirmacion()')); ?>
        </div> 
      </div>
<?php }else{?>
  <div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
    <strong>Error! </strong>No tienes acceso a esta locación 
  </div>
<?php } ?>

<script type="text/javascript"> 
  function confirmacion(e){
    var retVal = confirm("Desea continuar?");
    if( retVal == true ){
      return true;
    }
    else{
     event.preventDefault();
    }
  }
</script>