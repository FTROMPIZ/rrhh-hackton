<?php
    $cakeDescription = __d('cake_dev', 'Banesco');
    $cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version());

    if(preg_match('/Chrome/i',$_SERVER['HTTP_USER_AGENT'])) {
        header("Location: ".$this->webroot."users/login");
        exit;
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $cakeDescription ?>
        </title>

        <?php
            echo $this->Html->meta(
                'favicon.ico',
                '/img/favicon.png',
                array('type' => 'icon')
                );

            echo $this->fetch('meta');
            echo $this->fetch('css');
            echo $this->fetch('script');

            //CSS

            //Bootstrap & FontAwesome & Entypo CSS
            echo $this->Html->css('bootstrap/css/bootstrap.min');
            echo $this->Html->css('backend/css/font-awesome.min');
            echo $this->Html->css('backend/css/font-entypo');

            //Fonts CSS
            echo $this->Html->css('backend/css/fonts');

            // Specific CSS
            ?>
            <?php

            // JQUERY-UI
            echo $this->Html->css('jquery-ui/jquery-ui');
            echo $this->Html->css('jquery-ui/jquery-ui.structure');
            echo $this->Html->css('jquery-ui/jquery-ui.theme');

            //Theme CSS
            echo $this->Html->css('backend/css/theme.min');
            echo $this->Html->css('backend/css/chrome'); //chrome only css -->

            // Responsive CSS -->
            echo $this->Html->css('backend/css/theme-responsive.min');

            // Custom CSS -->
            echo $this->Html->css('backend/custom/custom');


            //Head SCRIPTS -->

            // JQUERY
            echo $this->Html->script('jquery.js');

            // JQUERY-UI
            echo $this->Html->script('jquery-ui/jquery-ui.js');

            echo $this->Html->script('bootstrap/bootstrap.min.js');
        ?>
    </head>

    <body id="pages" class="full-layout no-nav-left no-nav-right  nav-top-fixed background-login     responsive remove-navbar login-layout   clearfix" data-active="pages "  data-smooth-scrolling="1">
        <div class="vd_body">
            <div class="content">
                <div class="container">
                    <div class="vd_content-wrapper">
                        <div class="vd_container">
                            <div class="vd_content clearfix">
                                <div class="vd_content-section clearfix" style="padding: 0;">
                                    <div class="vd_login-page">
                                    	<div class="heading clearfix">
                                    		<div class="logo">
                                    			<h2 class="mgbt-xs-5">
                                    				<img src="<?php echo $this->webroot; ?>img/logo_verde.png" width="200px" alt="logo">
                                    			</h2>
                                    		</div>
                                    		<br />
                                    		<p>
                                                Este navegador no es compatible con la aplicación de <strong>Banesco</strong>. Te invitamos a descargar el navegador Google Chrome para poder usar este portal.
                                            </p>
                                    		<p>
                                                <a href="https://www.google.com/chrome/browser/desktop/index.html" class="btn btn-nw" target="_blank">Descargar Chorme</a>
                                            </p>
										</div>
                                    </div>
                                </div> <!-- .vd_content-section -->
                            </div> <!-- .vd_content -->
                        </div> <!-- .vd_container -->
                    </div> <!-- .vd_content-wrapper -->
                </div> <!-- .container -->
            </div> <!-- .content -->
        </div> <!-- .vd_body END  -->
    </body>
</html>