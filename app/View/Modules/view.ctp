<div class="modules view">
<h2><?php echo __('Module'); ?></h2>
	<dl>
		<dt><?php echo __('IdModule'); ?></dt>
		<dd>
			<?php echo $module['Module']['idModule']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ModuleName'); ?></dt>
		<dd>
			<?php echo $module['Module']['moduleName']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Module'), array('action' => 'edit', $module['Module']['idModule'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Module'), array('action' => 'delete', $module['Module']['idModule']), array(), __('Are you sure you want to delete # %s?', $module['Module']['idModule'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Modules'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Module'), array('action' => 'add')); ?> </li>
	</ul>
</div>
