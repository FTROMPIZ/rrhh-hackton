
<div class="vd_head-section clearfix">
    <div class="vd_panel-header">
        <ul class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')) ?>">Home</a> </li>
            <li class="active">Sucursales</li>
        </ul>
        <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
            <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
            <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
            <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>

        </div>

    </div>
</div>

<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1>Lista de Sucursales</h1>

    </div>
</div>
    <?php if(isset($module)){ ?> 
         <div class="vd_content-section clearfix">
            <div class="row">
              <div class="col-md-12">
                <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>  </h3>
                  </div>
                  <div class="panel-body table-responsive">
                    <table class="table table-striped" id="data-tables">
                      <thead>
                        <tr>
                          <th>Nombre</th>
                          <th>Estado</th>
                          <th>Ciudad</th>
                          <th>Dirección</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
	<?php foreach ($branches as $branch): ?>
	<tr>
		<td><?php echo $branch['Branch']['name']; ?>&nbsp;</td>
		<td><?php echo $branch['Branch']['state']; ?>&nbsp;</td>
		<td><?php echo $branch['Branch']['city']; ?>&nbsp;</td>
		<td><?php echo $branch['Branch']['address']; ?>&nbsp;</td>
		<td class="menu-action">
                    <!--<a data-original-title="view" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green" href="<?php //echo $this->Html->url(array('controller' => 'branches', 'action' => 'view',$branch['Branch']['idBranch'])) ?>"> <i class="fa fa-eye"></i> </a> -->
                      <a data-original-title="edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow" href="<?php echo $this->Html->url( array('action' => 'edit', $branch['Branch']['idBranch'])); ?>"> <i class="fa fa-pencil"></i> </a>
                      <form action="<?php echo $this->Html->url(array('controller' => 'branches', 'action' => 'delete',$branch['Branch']['idBranch'])) ?>" name="post_deleteRow<?php echo $branch['Branch']['idBranch'] ?>" id="post_deleteRow<?php echo $branch['Branch']['idBranch'] ?>" style="display:none;" method="post">
                            <input type="hidden" name="_method" value="POST">
                        </form>
                        <a href="#" onclick="if (confirm(&quot;¿Estás seguro que deseas borrar la sucursal: <?php echo $branch['Branch']['name'] ?> ?&quot;)) { document.post_deleteRow<?php echo $branch['Branch']['idBranch'] ?>.submit(); } event.returnValue = false; return false;"  class="btn menu-icon vd_bd-red vd_red" href=""><i class="glyphicon glyphicon-trash"></i></a>
                  </td>
	</tr>
<?php endforeach; ?>
	</tbody>
                    </table>
                   </table>
                  </div>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-md-12 --> 
            </div>
            <!-- row --> 
            
    <a href="<?php echo $this->Html->url(array('controller' => 'branches', 'action' => 'add')) ?>" class="btn btn-success start"> <i class="glyphicon glyphicon-plus"></i> <span>Agregar Sucursal</span> </a>
</div> 
<?php }else{?>
    <div class="alert alert-danger"> <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Error! </strong>No tienes acceso a esta locación </div>
    <?php } ?>