<h4 class="text-center font-semibold vd_grey">Ingresa tu correo para restablecer la contraseña</h4>     
	<div id="forgetPasswordForm">
		<?php
			echo $this->Session->flash('auth'); // This will show the authentication error message
			echo $this->Form->create('User', array("controller" => "users", "action" => "forgetPassword", "method" => "post"))
                                ?>
                <div class="vd_input-wrapper" id="email-input-wrapper"> <span class="menu-icon"> <i class="fa fa-envelope"></i> </span>
                <?php
			echo $this->Form->input('User.username', array("label" => false,"placeholder"=>"Correo"));
                        ?>
                </div><br>
                <div class="col-md-12 text-center mgbt-xs-5" style="padding: 0px;">
                <?php
			echo $this->Form->input("Enviar Correo de Recuperación!",array("label" => false,'div'=>'false', "class" => "btn vd_bg-green vd_white width-100", "type" => "submit"));	
                        echo $this->Form->end();
		?>
                </div>
	</div>
</div>