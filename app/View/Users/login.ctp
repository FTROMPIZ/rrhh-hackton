<h4 class="text-center font-semibold vd_grey">INGRESA CON TU CUENTA</h4>
<div id="loginForm">
	<?php
		echo $this->Session->flash('auth'); // This will show the authentication error message
		echo $this->Form->create('User', array("controller" => "users", "action" => "login", "method" => "post"))
  ?>
  <div class="vd_input-wrapper" id="email-input-wrapper"> <span class="menu-icon"> <i class="fa fa-user"></i> </span>
    <?php
      echo $this->Form->input('User.username', array("label" => false,"placeholder"=>"Usuario"));
    ?>
  </div>
          
  <div class="vd_input-wrapper" id="password-input-wrapper" > <span class="menu-icon"> <i class="fa fa-lock"></i> </span>
    <?php
      echo $this->Form->input('User.password', array("label" => false,"placeholder"=>"Contraseña"));
    ?>
  </div>
          
  <div class="col-md-12 text-center mgbt-xs-5" style="padding: 0px;">
    <?php
      echo $this->Form->input("Entrar",array("label" => false,'div'=>'false', "class" => "btn vd_bg-green vd_white width-100", "type" => "submit"));	
      echo $this->Form->end();
	  ?>
  </div>
</div>

<div class="alert alert-success vd_hidden">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
  <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Bien Hecho!</strong>. 
</div>                  

<div id="vd_login-error" class="alert alert-danger hidden"><i class="fa fa-exclamation-circle fa-fw"></i> Please fill the necessary field </div>
  <div class="form-group">
    <div class="col-md-12">
      <div class="row">
        <div class="col-xs-6" style="padding: 0px;">
          <div class="vd_checkbox">
            <input type="checkbox" id="checkbox-1" value="1">
            <label for="checkbox-1"> Recuerdame</label>
          </div>
        </div>
      
        <div class="col-xs-6 text-right" style="padding: 0px;">
          <div class=""> <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'forgetPassword')) ?>">Recuperar Contraseña </a> </div>
        </div>
      </div>
    </div>
  </div>
</div>