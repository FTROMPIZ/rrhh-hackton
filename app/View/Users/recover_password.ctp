<h4 class="text-center font-semibold vd_grey">Restablecer Contraseña</h4>
<div class="vd_content-section clearfix">
  <div class="related">
    <div class="users form">
      <?php echo $this->Form->create('User'); ?>
      <fieldset>
        <div class="vd_input-wrapper" id="password-input-wrapper" >
          <span class="menu-icon"> <i class="fa fa-lock"></i> </span>
          <?php
            echo $this->Form->input('idUser',array('label'=>false,'hidden'=>'true'));
            echo $this->Form->input('password',array('value'=>'',"placeholder"=>"Contraseña",
                                    'label'=>false, 
                                    'pattern' => '(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%]).{8,}', 
                                    'title' => "Debe contener al menos 8 caractéres, una letra mayúscula, un numero y alguno de estos caracteres !@#$%", 
                                    'maxlength' => 45));
          ?>
        </div>
      </fieldset>
      <br>

      <div class="col-md-12 text-center mgbt-xs-5" style="padding: 0px;">
        <?php
          echo $this->Form->input("Restablecer!",array("label" => false,'div'=>'false', 
                                  "class" => "btn vd_bg-green vd_white width-100",
                                  "type" => "submit"));
          echo $this->Form->end();
        ?>
      </div>
    </div>
  </div>
</div>
