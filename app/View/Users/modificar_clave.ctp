<!DOCTYPE html>
<!--[if IE 8]>      <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>      <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  
<html><!--<![endif]-->

    <!-- Specific Page Data -->

    <!-- End of Data -->

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>RRHH Banesco</title>
    <meta name="keywords" content="HTML5 Template, CSS3, All Purpose Admin Template, " />
    <meta name="description" content="Responsive Admin Template for multipurpose use">
    <meta name="author" content="Venmond">

    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    


    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/pple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
    <!--<link rel="shortcut icon" href="img/ico/favicon.png">-->
    <?php echo $this->Html->meta('img/favicon.ico','img/favicon.ico',array('type' => 'icon')); ?>
    <!-- CSS -->


    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="../custom/custom.css" />
    
    <!-- Bootstrap & FontAwesome & Entypo CSS -->
    <?php echo $this->Html->css('bootstrap.min.css'); ?>
    <?php echo $this->Html->css('font-awesome.min.css'); ?>
    <!--[if IE 7]><link type="text/css" rel="stylesheet" href="css/font-awesome-ie7.min.css"><![endif]-->
    <?php echo $this->Html->css('font-entypo.css'); ?> 

    <!-- Fonts CSS -->
    <?php echo $this->Html->css('fonts.css'); ?>

    <?php echo $this->Html->css('stylesheets.css'); ?>

    <!-- Plugin CSS -->
    
    <?php echo $this->Html->css('../plugins/jquery-ui/jquery-ui.custom.min.css'); ?>
    <?php echo $this->Html->css('../plugins/prettyPhoto-plugin/css/prettyPhoto.css'); ?>
    <?php echo $this->Html->css('../plugins/isotope/css/isotope.css'); ?>
    <?php echo $this->Html->css('../plugins/pnotify/css/jquery.pnotify.css'); ?>   
    <?php echo $this->Html->css('../plugins/google-code-prettify/prettify.css'); ?>
    
    <?php echo $this->Html->css('../plugins/mCustomScrollbar/jquery.mCustomScrollbar.css'); ?>
    <?php echo $this->Html->css('../plugins/tagsInput/jquery.tagsinput.css'); ?>
    <?php echo $this->Html->css('../plugins/bootstrap-switch/bootstrap-switch.css'); ?>
    <?php echo $this->Html->css('../plugins/daterangepicker/daterangepicker-bs3.css'); ?>
    <?php echo $this->Html->css('../plugins/bootstrap-timepicker/bootstrap-timepicker.min.css'); ?>
    <?php echo $this->Html->css('../plugins/colorpicker/css/colorpicker.css'); ?>       
    
    <?php echo $this->Html->css('../plugins/jquery-file-upload/css/jquery.fileupload.css'); ?>     
    <?php echo $this->Html->css('../plugins/jquery-file-upload/css/jquery.fileupload-ui.css'); ?>     
    <?php echo $this->Html->css('../plugins/bootstrap-wysiwyg/css/bootstrap-wysihtml5-0.0.2.css'); ?>        
 
    <!-- Specific CSS -->
    <?php echo $this->Html->css('../plugins/fullcalendar/fullcalendar.css'); ?>
    <?php echo $this->Html->css('../plugins/fullcalendar/fullcalendar.print.css'); ?>
    <?php echo $this->Html->css('../plugins/introjs/css/introjs.min.css'); ?>
    <?php echo $this->Html->css("../plugins/dataTables/css/jquery.dataTables..css"); ?>
    <?php echo $this->Html->css("../plugins/dataTables/css/dataTables.bootstrap.css"); ?> 

    <!-- Theme CSS -->
    <?php echo $this->Html->css('theme.min.css'); ?>
    <!--[if IE]> <link href="css/ie.css" rel="stylesheet" > <![endif]-->
    <?php echo $this->Html->css('chrome.css'); ?><!-- chrome only css -->    

    <!-- Responsive CSS -->
    <?php echo $this->Html->css('theme-responsive.min.css'); ?>

    <!-- Head SCRIPTS -->
    <?php echo $this->Html->script('modernizr.js'); ?>
    <?php echo $this->Html->script('mobile-detect.min.js'); ?>
    <?php echo $this->Html->script('mobile-detect-modernizr.js'); ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type="text/javascript" src="js/html5shiv.js"></script>
      <script type="text/javascript" src="js/respond.min.js"></script>     
    <![endif]-->
  </head> 

  <body id="pages" class="full-layout no-nav-left no-nav-right  nav-top-fixed background-login     responsive remove-navbar login-layout   clearfix" data-active="pages "  data-smooth-scrolling="1">     
    <div class="vd_body">
      <div class="content">
        <div class="container">   
          <div class="vd_content-wrapper">
            <div class="vd_container">
              <div class="vd_content clearfix">
                <div class="vd_content-section clearfix" style="padding: 0px;">
                  <div class="vd_login-page">
                      <div class="heading clearfix" style="margin-bottom: 0px;">
                      <div class="logo">
                        <h2 class="mgbt-xs-5"><img src="/rrhh-backend-banesco/img/logo_verde.png" alt="" /></h2>
                      </div>
                    </div>
                    <div class="panel widget">
                      <div class="panel-body">
                        <div class="login-icon entypo-icon"> <i class="icon-key"></i> </div>
                        <div class="alert alert-danger vd_hidden">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                          <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> Change a few things up and try submitting again. 
                        </div>

                        <div class="form-block">
                          <div style="color: red; margin-bottom: 10px;">
                            Su contraseña ha expirado. Debe cambiarla para continuar
                            <?php echo $this->Session->flash(); ?>
                          </div>
                          <h4 class="text-center font-semibold vd_grey">Modificar Contraseña</h4>
                          <div id="loginForm">
                            <?php if(isset($userRole)){ ?>
                              <?php
                                //echo $this->Session->flash('auth'); // This will show the authentication error message
                                echo $this->Form->create('User', array("controller" => "users", "action" => "modificar_clave", "method" => "post"))
                              ?>
                                               
                              <div class="vd_input-wrapper" id="password-input-wrapper" > <span class="menu-icon"> <i class="fa fa-lock"></i> </span>
                                <?php
                                  echo $this->Form->input('password', array('value'=>'',"placeholder"=>"Contraseña",
                                                  'label'=>false, 
                                                  'pattern' => '(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%]).{8,}', 
                                                  'title' => "Debe contener al menos 8 caractéres, una letra mayúscula, un numero y alguno de estos caracteres !@#$%", 
                                                  'maxlength' => 45));
                                ?>
                              </div>
                                                
                              <div class="col-md-12 text-center mgbt-xs-5" style="padding: 0px;">
                                <?php
                                  echo $this->Form->input("Modificar!",array("label" => false,'div'=>'false', 
                                                          "class" => "btn vd_bg-green vd_white", "type" => "submit"));
                                  echo $this->Form->end(); 
                                ?>
                              </div>
                            <?php }else{ ?>
                              <div class="alert alert-danger"> 
                                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                                <strong>Error! </strong>No tienes acceso a esta locación 
                              </div>
                            <?php } ?>  
                          </div>
                        </div>              
                      </div>                      
                    </div>
                  </div>
                </div>
              </div> 
            </div> 
          </div> 
        </div>   
      </div> 
    </div>
  </body>
</html>
