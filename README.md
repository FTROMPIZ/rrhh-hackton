# README #

### What is this repository for? ###

Este repositorio contiene el proyecto de Banesco RRHH y usa los siguientes lenguajes y/o frameworks.

- PHP 5.5.27
- Cake 2.6.1
- MySQL 5.6.25


### Version ###

- Versión: 1.0


### How do I get set up?  * How to run tests  ###

DESARROLLO:

Para utilizar la aplicación localmente necesitamos iniciar los servicios de apache y mysql, esto se realiza colocando en la consola el siguiente comando:
	sudo /opt/lampp/lampp start

Al tener iniciado dichos servicios se puede ingresar al sistema a través de la url: http://localhost/rrhh-backend-banesco/, la cual nos llevará ala página de login del sistema. Los datos de prueba para poder ingresar son:

	Usuario: admin
	Contraseña: Se*****6

Para acceder a la base de datos local no se necesita clave y la url es: http://localhost/phpmyadmin, la base de datos se llama "rrhh".


PRODUCCIÓN:

La url para ingresar al sistema es: http://rrhh-banesco.cloudapp.net/

Para ingresar a la base de datos a través de phpmyadmin es a través de la url: http://rrhh-banesco.cloudapp.net/phpmyadmin

Los datos de acceso al mismo se encuentran en confluence.


### Database configuration ###
El sql del sistema se encuentra en la raiz del proyecto comprimido con el nombre de rrhh_banesco.rar.

Para importar el sql a la base de datos, se debe ingresar a phpmyadmin, crear la bd con el nombre de "rrhh" e ir a importar y adjuntar el archivo.


### Contribution guidelines ###

Writing tests:

Este sistema sirve de servicio web para las aplicaciones móviles tales como android.


### Who do I talk to? ###

Other community or team contact:

Cindy De Almada - cdealmada@mobmedianet.com